-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 31, 2018 at 03:08 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_spi`
--

-- --------------------------------------------------------

--
-- Table structure for table `aspek_audit`
--

CREATE TABLE `aspek_audit` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_aspek` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `aspek_audit`
--

INSERT INTO `aspek_audit` (`id`, `nama_aspek`, `created_at`, `updated_at`) VALUES
(1, 'Keuangan/Akuntansi', NULL, NULL),
(2, 'SDM', NULL, NULL),
(3, 'Ketatalaksanaan Organisasi', NULL, NULL),
(4, 'BMN/Aset', NULL, NULL),
(5, 'Hukum', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `auditee`
--

CREATE TABLE `auditee` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `unit_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pimpinan_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `masa_kerja` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auditee`
--

INSERT INTO `auditee` (`id`, `id_user`, `unit_kerja`, `pimpinan_kerja`, `nip`, `masa_kerja`, `keterangan`, `token`, `created_at`, `updated_at`) VALUES
(1, 18, 'IT', 'Dedi', '3456789', '1998-MASIH AKTIF', 'ya', '44476', '2018-03-25 05:41:28', '2018-03-25 05:41:28');

-- --------------------------------------------------------

--
-- Table structure for table `auditor`
--

CREATE TABLE `auditor` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_jurusan` int(10) UNSIGNED NOT NULL,
  `id_golongan` int(10) UNSIGNED NOT NULL,
  `nip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `auditor`
--

INSERT INTO `auditor` (`id`, `id_user`, `id_jurusan`, `id_golongan`, `nip`, `token`, `created_at`, `updated_at`) VALUES
(7, 16, 1, 1, '123456', '35458', '2018-03-18 20:48:55', '2018-03-18 20:48:55'),
(8, 17, 1, 1, '2345678', '35374', '2018-03-25 05:40:50', '2018-03-25 05:40:50'),
(9, 19, 1, 1, '67890112', '15247', '2018-03-25 06:35:16', '2018-03-25 06:35:16'),
(10, 22, 1, 1, '10101010', '98123', '2018-03-28 00:24:21', '2018-03-28 00:24:21');

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_fakultas` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id`, `nama_fakultas`, `created_at`, `updated_at`) VALUES
(1, 'MIPA', NULL, NULL),
(2, 'Teknik', NULL, NULL),
(3, 'KIP', NULL, NULL),
(4, 'Ekonomi', NULL, NULL),
(5, 'Hukum', NULL, NULL),
(6, 'Pertanian', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'III/a', NULL, NULL),
(2, 'III/b', NULL, NULL),
(3, 'III/c', NULL, NULL),
(4, 'III/d', NULL, NULL),
(5, 'IV/a', NULL, NULL),
(6, 'IV/b', NULL, NULL),
(7, 'IV/c', NULL, NULL),
(8, 'IV/d', NULL, NULL),
(9, 'IV/e', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan_auditor`
--

CREATE TABLE `jabatan_auditor` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jabatan_auditor`
--

INSERT INTO `jabatan_auditor` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Ketua Audit', NULL, NULL),
(2, 'Sekretaris', NULL, NULL),
(3, 'Anggota Tim', NULL, NULL),
(4, 'Anggota Penunjang', NULL, NULL),
(5, 'Staff Audit', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_audit`
--

CREATE TABLE `jadwal_audit` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_auditee` int(10) UNSIGNED NOT NULL,
  `id_tipeaudit` int(10) UNSIGNED NOT NULL,
  `nomorsurat` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `attachment_surattugas` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `keterangan` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jadwal_audit`
--

INSERT INTO `jadwal_audit` (`id`, `id_auditee`, `id_tipeaudit`, `nomorsurat`, `tanggal_mulai`, `tanggal_selesai`, `attachment_surattugas`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 2, '1836/UN26/KP/2017', '2018-03-28', '2018-03-29', '374-137-Article Text-220-1-10-20170309.pdf', 'SPI2018', '2018-03-25 05:46:01', '2018-03-25 05:46:01'),
(4, 1, 2, '1838/UN26/KP/2017', '2018-04-19', '2018-04-20', '573-sample.pdf', 'SPI2018', '2018-04-18 23:43:35', '2018-04-18 23:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_fakultas` int(10) UNSIGNED NOT NULL,
  `nama_jurusan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id`, `id_fakultas`, `nama_jurusan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ilkom', '2018-03-18 06:37:17', '2018-03-18 06:37:17');

-- --------------------------------------------------------

--
-- Table structure for table `laporanaudit`
--

CREATE TABLE `laporanaudit` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jadwalAudit` int(10) UNSIGNED NOT NULL,
  `nomor_urut` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_dokumen` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kode_temuan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi_temuan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kriteria` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `akibat` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sebab` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kesimpulan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rekomendasi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `rencana_perbaikan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tindak_lanjut` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jadwal_penyelesaian` date NOT NULL,
  `status` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filedokumen` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `laporanaudit`
--

INSERT INTO `laporanaudit` (`id`, `id_jadwalAudit`, `nomor_urut`, `kode_dokumen`, `kode_temuan`, `deskripsi_temuan`, `kriteria`, `akibat`, `sebab`, `kesimpulan`, `rekomendasi`, `rencana_perbaikan`, `tindak_lanjut`, `jadwal_penyelesaian`, `status`, `filedokumen`, `created_at`, `updated_at`) VALUES
(1, 1, '2', 'ya', '1', 'baik', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', 'ya', '2018-03-29', 'Tervalidasi', '', '2018-03-25 12:49:41', '2018-03-29 05:29:57');

--
-- Triggers `laporanaudit`
--
DELIMITER $$
CREATE TRIGGER `trigger_insert_tanggapan` AFTER INSERT ON `laporanaudit` FOR EACH ROW BEGIN
                INSERT INTO tanggapanaudit(id_laporanaudit) VALUES(NEW.id);
            END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_00_00_000001_create_fakultas_table', 1),
(2, '2016_00_00_000002_create_jurusan_table', 1),
(3, '2016_00_00_000003_create_golongan_table', 1),
(4, '2016_00_00_000004_create_tipe_audit_table', 1),
(5, '2016_00_00_000005_create_aspek_audit_table', 1),
(6, '2016_00_00_000006_create_roles_table', 1),
(7, '2016_00_00_000007_create_users_table', 1),
(8, '2016_00_00_000008_create_jabatan_auditor_table', 1),
(9, '2016_00_00_000009_create_auditee_table', 1),
(10, '2016_00_00_000010_create_auditor_table', 1),
(11, '2016_00_00_000011_create_jadwal_audit_table', 1),
(12, '2016_00_00_000012_create_laporanaudit_table', 1),
(13, '2016_00_00_000013_create_tanggapan_audit_table', 1),
(14, '2017_10_04_075943_create_password_resets_table', 1),
(15, '2017_12_21_014523_create_p_auditoraudit_table', 1),
(16, '2017_12_21_014540_create_p_jadwalaspekaudit_table', 1),
(17, '2018_01_05_025528_create_trigger_tanggapan_audit', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `p_auditoraudit`
--

CREATE TABLE `p_auditoraudit` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jadwalAudit` int(10) UNSIGNED NOT NULL,
  `id_auditor` int(10) UNSIGNED NOT NULL,
  `id_jabatan_audit` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `p_auditoraudit`
--

INSERT INTO `p_auditoraudit` (`id`, `id_jadwalAudit`, `id_auditor`, `id_jabatan_audit`, `created_at`, `updated_at`) VALUES
(26, 1, 7, 1, '2018-03-25 07:07:51', '2018-03-25 07:07:51'),
(27, 1, 8, 2, '2018-03-25 07:07:51', '2018-03-25 07:07:51'),
(28, 1, 7, 3, '2018-03-25 07:07:51', '2018-03-25 07:07:51'),
(29, 1, 7, 4, '2018-03-25 07:07:51', '2018-03-25 07:07:51'),
(30, 1, 7, 5, '2018-03-25 07:07:51', '2018-03-25 07:07:51'),
(31, 4, 8, 1, '2018-04-18 23:43:35', '2018-04-18 23:43:35'),
(32, 4, 7, 2, '2018-04-18 23:43:35', '2018-04-18 23:43:35'),
(33, 4, 9, 3, '2018-04-18 23:43:35', '2018-04-18 23:43:35'),
(34, 4, 10, 4, '2018-04-18 23:43:35', '2018-04-18 23:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `p_jadwalaspekaudit`
--

CREATE TABLE `p_jadwalaspekaudit` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_jadwalAudit` int(10) UNSIGNED NOT NULL,
  `id_aspekaudit` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `p_jadwalaspekaudit`
--

INSERT INTO `p_jadwalaspekaudit` (`id`, `id_jadwalAudit`, `id_aspekaudit`, `created_at`, `updated_at`) VALUES
(8, 1, 1, '2018-03-25 07:07:51', '2018-03-25 07:07:51'),
(9, 4, 1, '2018-04-18 23:43:35', '2018-04-18 23:43:35');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `nama`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ketua SPI', NULL, NULL, NULL, NULL),
(2, 'Operator', NULL, NULL, NULL, NULL),
(3, 'Auditor', NULL, NULL, NULL, NULL),
(4, 'Audity', NULL, NULL, NULL, NULL),
(5, 'Koordinator Bidang', NULL, NULL, NULL, NULL),
(6, 'Rektor', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tanggapanaudit`
--

CREATE TABLE `tanggapanaudit` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_laporanaudit` int(10) UNSIGNED NOT NULL,
  `tanggapan_auditee` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `tanggapan_auditor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `tanggapan_rektor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `keterangan` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '-',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tanggapanaudit`
--

INSERT INTO `tanggapanaudit` (`id`, `id_laporanaudit`, `tanggapan_auditee`, `tanggapan_auditor`, `tanggapan_rektor`, `keterangan`, `created_at`, `updated_at`) VALUES
(1, 1, 'ya', '-', 'baik', '-', NULL, '2018-05-09 20:51:33');

-- --------------------------------------------------------

--
-- Table structure for table `tipe_audit`
--

CREATE TABLE `tipe_audit` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama_tipe` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tipe_audit`
--

INSERT INTO `tipe_audit` (`id`, `nama_tipe`, `created_at`, `updated_at`) VALUES
(1, 'Reguler', NULL, NULL),
(2, 'PDTT', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_role` tinyint(3) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `email`, `password`, `id_role`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ketua SPI', 'ketua_spi', 'fcummerata@von.info', '$2y$10$tt3sd/W57.wJo4JQ3bNp/.9rDhMnQvLajSLlnbikWyVfCYAa6fnly', 1, 'RnggTy9DZk921D2SjbZgfc1MeY6YVrF0p7VRtFS9HCwGHO7gOo4BjhZ3aeJG', '2018-02-26 05:43:00', '2018-02-26 05:43:00', NULL),
(2, 'Operator', 'operator', 'schmitt.loraine@hotmail.com', '$2y$10$x2M7kWHEl5eMeGEyvukTUek8Bu7xJXVWLHQ07OpsTt6rw6HQsRGtW', 2, '4MxgVtDY0eOdLNY4aZhhsehc30VT9FWhBYENKPpVOX9IV6hSUnDmONSzPcJU', '2018-02-26 05:43:01', '2018-02-26 05:43:01', NULL),
(3, 'Auditor', 'auditor', 'jermain12@hotmail.com', '$2y$10$7FsyzjqfxSqj.JsSVRFvc.5szEBtiw7N1M3QG1bA7.dFqBMs8289W', 3, 'hXOislOTYOS2NREIZ1V6mxgZTreynur4FT7pZwof9QHR9GxDKTd31IUSolJI', '2018-02-26 05:43:01', '2018-02-26 05:43:01', NULL),
(4, 'Auditi', 'auditi', 'damon29@gmail.com', '$2y$10$cBhOs7VQcCDO9F3mZ50Sv.otaYvd2w/vElcZEXxPRZuS77dUZ.So2', 4, 't8sgNB5TdZyWRvmMFECQxCNnOK1yej2ImWIYBB9N6119zQWMivxTxNxQcA5k', '2018-02-26 05:43:01', '2018-02-26 05:43:01', NULL),
(5, 'Koordinator Bidang', 'koor_bidang', 'turcotte.stuart@jenkins.com', '$2y$10$xYtWioDEKFIxqXnpTQ/fWe9heqlI8TJnswCqs6DrMjCEm67TCcMCy', 5, 'OxcusztpDrXGI3NwCYhqCu5mPYS1LBTqYSWe74lJAF6Sk6zFql9Zs0FAcqdL', '2018-02-26 05:43:01', '2018-02-26 05:43:01', NULL),
(6, 'Rektor', 'rektor', 'merlin.leffler@yahoo.com', '$2y$10$p1USZSIikxGcJawElqyM5OwtO2/w14GPLwbOKeRMaadgVuIi1Uuny', 6, 'TWCFRbJDrLRxTtOTR3jCeGyIYrP8VEWDoaClFOTw95IqpZV67zJeLHLAjjuB', '2018-02-26 05:43:01', '2018-02-26 05:43:01', NULL),
(16, 'Indra', '1P3ZZO', 'torp.ulices@purdy.com', '$2y$10$8uA70YuNxdr0R1oBznrwGemSqHZ7xl2KylLBSAc3CL7a0peBqPaaS', 3, 'tR7rOaZSzZgc7SV2CBe79bZQnd9OlP6jSUwzAYV3ioFbVTDV8N70a6m9MT8n', '2018-03-18 20:48:55', '2018-03-25 05:46:56', NULL),
(17, 'suwarno', 'jq6xsf', 'phyllis28@yahoo.com', '$2y$10$12hq28huznl9aBH7/Llny.yXB49RiUvNpDa9MYJnDiFDk2Kec.u5y', 3, 'xB4Eg3Af8mAyxIzZO58ny1BnHjAipuggnO2eyccxFXCo5HgC8odAIjaFxZFA', '2018-03-25 05:40:50', '2018-03-25 05:46:45', NULL),
(18, 'IT', 'QDqHdk', 'alison.koch@hotmail.com', '$2y$10$l0as8B6IWeTpqtX4owiLGuudSpVZ4w2cMVmW4TwP7oi/nAqAITySu', 4, 'GxGK5Hg6lF2fnTLIxYryNWzFXvdNe7rIuYvchqYndoSoJajyoukWsyWorNns', '2018-03-25 05:41:28', '2018-03-25 05:47:07', NULL),
(19, 'iin', 'zMGFXX', 'hweber@hauck.net', '$2y$10$XOoiZNkEFQUwaPz0ZCHP1e2mawZJ7UDw5tov/uxHGDZmfck1hs.oy', 3, 'Kj1fV2TB3sFOumWnXIGELQ2Xe7xcHSM4bG0JaQSWJFtrKBFCG9df6VI0Ozj7', '2018-03-25 06:35:16', '2018-03-25 06:37:26', NULL),
(22, 'vina', 'Eui0U7', 'billie21@kovacek.com', '$2y$10$qea1je2mNP4SeC58wbkcZOk.lYsaAYvi25BvSw0Qiig8BbmUqBMJS', 3, NULL, '2018-03-28 00:24:21', '2018-03-28 20:50:25', NULL),
(23, 'Keuangan', 'zjWbm0', 'olin.weimann@schultz.com', '$2y$10$CYIpkWPmeW6nUuYUJDpOjeycyc6t8qwfEuDwaod0gdX2yQL6pcZ9e', 4, NULL, '2018-04-19 00:23:17', '2018-04-19 00:23:17', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aspek_audit`
--
ALTER TABLE `aspek_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auditee`
--
ALTER TABLE `auditee`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD KEY `auditee_id_user_index` (`id_user`);

--
-- Indexes for table `auditor`
--
ALTER TABLE `auditor`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nip` (`nip`),
  ADD KEY `auditor_id_user_index` (`id_user`),
  ADD KEY `auditor_id_jurusan_index` (`id_jurusan`),
  ADD KEY `auditor_id_golongan_index` (`id_golongan`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan_auditor`
--
ALTER TABLE `jabatan_auditor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_audit`
--
ALTER TABLE `jadwal_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jadwal_audit_id_auditee_index` (`id_auditee`),
  ADD KEY `jadwal_audit_id_tipeaudit_index` (`id_tipeaudit`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jurusan_id_fakultas_index` (`id_fakultas`);

--
-- Indexes for table `laporanaudit`
--
ALTER TABLE `laporanaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `laporanaudit_id_jadwalaudit_index` (`id_jadwalAudit`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `p_auditoraudit`
--
ALTER TABLE `p_auditoraudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_auditoraudit_id_jadwalaudit_index` (`id_jadwalAudit`),
  ADD KEY `p_auditoraudit_id_auditor_index` (`id_auditor`),
  ADD KEY `p_auditoraudit_id_jabatan_audit_index` (`id_jabatan_audit`);

--
-- Indexes for table `p_jadwalaspekaudit`
--
ALTER TABLE `p_jadwalaspekaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `p_jadwalaspekaudit_id_jadwalaudit_index` (`id_jadwalAudit`),
  ADD KEY `p_jadwalaspekaudit_id_aspekaudit_index` (`id_aspekaudit`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tanggapanaudit`
--
ALTER TABLE `tanggapanaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tanggapanaudit_id_laporanaudit_index` (`id_laporanaudit`);

--
-- Indexes for table `tipe_audit`
--
ALTER TABLE `tipe_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_role_index` (`id_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aspek_audit`
--
ALTER TABLE `aspek_audit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `auditee`
--
ALTER TABLE `auditee`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `auditor`
--
ALTER TABLE `auditor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `jabatan_auditor`
--
ALTER TABLE `jabatan_auditor`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `jadwal_audit`
--
ALTER TABLE `jadwal_audit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `laporanaudit`
--
ALTER TABLE `laporanaudit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `p_auditoraudit`
--
ALTER TABLE `p_auditoraudit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `p_jadwalaspekaudit`
--
ALTER TABLE `p_jadwalaspekaudit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tanggapanaudit`
--
ALTER TABLE `tanggapanaudit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tipe_audit`
--
ALTER TABLE `tipe_audit`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auditee`
--
ALTER TABLE `auditee`
  ADD CONSTRAINT `auditee_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditor`
--
ALTER TABLE `auditor`
  ADD CONSTRAINT `auditor_id_golongan_foreign` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auditor_id_jurusan_foreign` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auditor_id_user_foreign` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jadwal_audit`
--
ALTER TABLE `jadwal_audit`
  ADD CONSTRAINT `jadwal_audit_id_auditee_foreign` FOREIGN KEY (`id_auditee`) REFERENCES `auditee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_audit_id_tipeaudit_foreign` FOREIGN KEY (`id_tipeaudit`) REFERENCES `tipe_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `jurusan_id_fakultas_foreign` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `laporanaudit`
--
ALTER TABLE `laporanaudit`
  ADD CONSTRAINT `laporanaudit_id_jadwalaudit_foreign` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_auditoraudit`
--
ALTER TABLE `p_auditoraudit`
  ADD CONSTRAINT `p_auditoraudit_id_auditor_foreign` FOREIGN KEY (`id_auditor`) REFERENCES `auditor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `p_auditoraudit_id_jabatan_audit_foreign` FOREIGN KEY (`id_jabatan_audit`) REFERENCES `jabatan_auditor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `p_auditoraudit_id_jadwalaudit_foreign` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_jadwalaspekaudit`
--
ALTER TABLE `p_jadwalaspekaudit`
  ADD CONSTRAINT `p_jadwalaspekaudit_id_aspekaudit_foreign` FOREIGN KEY (`id_aspekaudit`) REFERENCES `aspek_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `p_jadwalaspekaudit_id_jadwalaudit_foreign` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tanggapanaudit`
--
ALTER TABLE `tanggapanaudit`
  ADD CONSTRAINT `tanggapanaudit_id_laporanaudit_foreign` FOREIGN KEY (`id_laporanaudit`) REFERENCES `laporanaudit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_role_foreign` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
