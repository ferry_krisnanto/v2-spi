<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JenisAudit extends Model
{
    protected $table = 'jenisaudit';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {
            $data->jadwalAudit()->delete();
        });
    }

    //RELATION table
  	public function tipeAudit() {
  		return $this->belongsTo('App\Model\TipeAudit', 'id_tipeAudit');
  	}
}
