<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Auditee extends Model
{
    protected $table = 'auditee';

	protected $fillable = [
        'id_user', 'unit_kerja', 'pimpinan_kerja', 'nip', 'masa_kerja', 'keterangan', 'token',
    ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table
  	public function user() {
  		return $this->belongsTo('App\Model\User', 'id_user');
  	}
}
