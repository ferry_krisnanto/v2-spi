<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Auditor extends Model
{
    protected $table = 'auditor';

	protected $fillable = [
        'id_user', 'id_jurusan', 'nip', 'id_golongan', 'token',
    ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table
  	public function user() {
  		return $this->belongsTo('App\Model\User', 'id_user');
  	}

	public function jurusan() {
  		return $this->belongsTo('App\Model\Jurusan', 'id_jurusan');
  	}

	public function golongan() {
  		return $this->belongsTo('App\Model\Golongan', 'id_golongan');
  	}

	public function auditorAudit() {
  		return $this->hasMany('App\Model\AuditorAudit', 'id_jadwalAudit');
  	}
}
