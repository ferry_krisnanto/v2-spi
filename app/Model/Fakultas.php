<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Fakultas extends Model
{
    protected $table = 'fakultas';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table

	public function jurusan() {
  		return $this->hasMany('App\Model\Jurusan', 'id_fakultas');
  	}
}
