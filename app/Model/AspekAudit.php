<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AspekAudit extends Model
{
    protected $table = 'aspek_audit';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {
            $data->jenisAudit()->forceDelete();
        });
    }

    //RELATION table
  	public function jenisAudit() {
  		return $this->hasMany('App\Model\JenisAudit', 'id_tipeAudit');
  	}
}
