<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JadwalAspekAudit extends Model
{
    protected $table = 'p_jadwalaspekaudit';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table
  	public function jadwalAudit() {
  		return $this->belongsTo('App\Model\JadwalAudit', 'id_jadwalAudit');
  	}

	public function aspekAudit() {
  		return $this->belongsTo('App\Model\AspekAudit', 'id_aspekaudit');
  	}
}
