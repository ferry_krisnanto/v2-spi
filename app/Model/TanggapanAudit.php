<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TanggapanAudit extends Model
{
    protected $table = 'tanggapanaudit';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table
  	public function laporanAudit() {
  		return $this->belongsTo('App\Model\LaporanAudit', 'id_laporanaudit');
  	}
}
