<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JabatanAuditor extends Model
{
    protected $table = 'jabatan_auditor';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table

	public function auditorAudit() {
  		return $this->hasMany('App\Model\AuditorAudit', 'id_jabatan_audit');
  	}
}
