<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Jurusan extends Model
{
    protected $table = 'jurusan';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table

	public function auditor() {
  		return $this->hasMany('App\Model\Auditor', 'id_jurusan');
  	}

	public function fakultas() {
  		return $this->belongsTo('App\Model\Fakultas', 'id_fakultas');
  	}
}
