<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AuditorAudit extends Model
{
    protected $table = 'p_auditoraudit';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table

	public function jadwalAudit() {
  		return $this->belongsTo('App\Model\JadwalAudit', 'id_jadwalAudit');
  	}

	public function auditor() {
  		return $this->belongsTo('App\Model\Auditor', 'id_auditor');
  	}

	public function jabatanAuditor() {
  		return $this->belongsTo('App\Model\JabatanAuditor', 'id_jabatan_audit');
  	}
}
