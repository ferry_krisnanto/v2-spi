<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'roles';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {
            $data->users()->forceDelete();
        });
    }

    //RELATION table
  	public function users() {
  		return $this->hasMany('App\Model\User', 'id_role');
  	}
}
