<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TipeAudit extends Model
{
    protected $table = 'tipe_audit';

  	protected $fillable = [
          'nama_tipe',
      ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {
            $data->jenisAudit()->forceDelete();
        });
    }

    //RELATION table
  	public function jenisAudit() {
  		return $this->hasMany('App\Model\JenisAudit', 'id_jenisAudit');
  	}
}
