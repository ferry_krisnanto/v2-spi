<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LaporanAudit extends Model
{
    protected $table = 'laporanaudit';

  	protected $fillable = [
          'id_jadwalAudit', 'kode_dokumen', 'status',
      ];

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table
  	public function jadwalAudit() {
  		return $this->belongsTo('App\Model\JadwalAudit', 'id_jadwalAudit');
  	}
    public function tanggapanAudit() {
  		return $this->hasOne('App\Model\TanggapanAudit', 'id_laporanaudit');
  	}

    // MUTATOR
	public function getTerbitAttribute($value) {
        setlocale(LC_ALL, 'IND');
        return date('d F Y H:i:s', strtotime($this->jadwal_penyelesaian));
    }
}
