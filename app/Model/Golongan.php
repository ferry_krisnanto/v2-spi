<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Golongan extends Model
{
    protected $table = 'golongan';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {
			
        });
    }

    //RELATION table

	public function auditor() {
  		return $this->hasMany('App\Model\Auditor', 'id_golongan');
  	}
}
