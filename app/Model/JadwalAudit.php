<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class JadwalAudit extends Model
{
    protected $table = 'jadwal_audit';

    protected static function boot() {
        parent::boot();
        static::deleting(function($data) {

        });
    }

    //RELATION table
  	public function tipeAudit() {
  		return $this->belongsTo('App\Model\TipeAudit', 'id_tipeaudit');
  	}

	public function auditee() {
  		return $this->belongsTo('App\Model\Auditee', 'id_auditee');
  	}

	public function jadwalAspek(){
		return $this->hasMany('App\Model\JadwalAspekAudit', 'id_jadwalAudit');
	}

	public function auditorAudit(){
		return $this->hasMany('App\Model\AuditorAudit', 'id_jadwalAudit');
	}
}
