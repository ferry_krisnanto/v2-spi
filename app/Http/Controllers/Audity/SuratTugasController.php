<?php

namespace App\Http\Controllers\Audity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class SuratTugasController extends Controller
{
    public function index(){
        $user=Auth::user();
        $jadwal = DB::table('jadwal_audit')
        ->join('auditee','jadwal_audit.id_auditee','=','auditee.id')
        ->select('jadwal_audit.id','jadwal_audit.nomorsurat','jadwal_audit.attachment_surattugas','jadwal_audit.tanggal_mulai','jadwal_audit.tanggal_selesai',
        'jadwal_audit.created_at')
        ->where('auditee.id_user','=',$user->id)
        ->get();

        $auditor = DB::table('jadwal_audit')
        ->join('auditee','jadwal_audit.id_auditee','=','auditee.id')
        ->join('p_auditoraudit', 'jadwal_audit.id', '=', 'p_auditoraudit.id_jadwalAudit')
        ->join('auditor','p_auditoraudit.id_auditor','=','auditor.id')
        ->join('users','auditor.id_user','=','users.id')
        ->select('jadwal_audit.nomorsurat','p_auditoraudit.id_jabatan_audit','jadwal_audit.attachment_surattugas','jadwal_audit.tanggal_mulai','jadwal_audit.tanggal_selesai',
        'jadwal_audit.created_at','users.nama')
        ->where('auditee.id_user','=',$user->id, 'and')
        ->where(  'p_auditoraudit.id_jabatan_audit','=',1 )
        ->get();
        return view('audity.surat_tugas')->with([
			'auditor'			=> $auditor,
			'jadwal'		=> $jadwal,
		]);
    }
    public function daftar_auditor (){
        $auditor = DB::table('jadwal_audit')
        ->join('auditee','jadwal_audit.id_auditee','=','auditee.id')
        ->join('p_auditoraudit', 'jadwal_audit.id', '=', 'p_auditoraudit.id_jadwalAudit')
        ->join('auditor','p_auditoraudit.id_auditor','=','auditor.id')
        ->join('users','auditor.id_user','=','users.id')
        ->select('jadwal_audit.nomorsurat','p_auditoraudit.id_jabatan_audit','jadwal_audit.attachment_surattugas','jadwal_audit.tanggal_mulai','jadwal_audit.tanggal_selesai',
        'jadwal_audit.created_at','users.nama')
        ->where('auditee.id_user','=',$user->id, 'and')
        ->where(  'p_auditoraudit.id_jabatan_audit','=',1 )
        ->get();
        dd($auditor);
        return view('audity.surat_tugas')->with([
			'auditor'			=> $auditor,
			'jadwal'		=> $jadwal,
		]);
    }
}
