<?php

namespace App\Http\Controllers\Audity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;


class LHPController extends Controller
{
	public function index(){
		$user=Auth::user();		
		$LaporanAudit=DB::table('laporanaudit')
		->leftjoin('jadwal_audit', 'laporanaudit.id_jadwalAudit', '=', 'jadwal_audit.id')
		->leftjoin('tipe_audit', 'jadwal_audit.id_tipeaudit', '=', 'tipe_audit.id')
		->leftjoin('auditee', 'jadwal_audit.id_auditee', '=', 'auditee.id')
		->select(	'laporanaudit.id', 'laporanaudit.kode_dokumen', 'laporanaudit.created_at', 'laporanaudit.status',
							'jadwal_audit.tanggal_mulai', 'jadwal_audit.tanggal_selesai', 'tipe_audit.nama_tipe', 'laporanaudit.status')
		->orderBy('laporanaudit.id', "DESC")
		->where ('auditee.id_user','=',$user->id)
		->get();
		return view('audity.data-lhp.index', compact('LaporanAudit'));
	}
	public function tanggap(Request $req){
		$id=$req->id;
		$tanggapan=$req->tanggapan;
		DB::table('tanggapanaudit')
		->where('id', $id)
		->update(['tanggapan_auditee' => $tanggapan]);
	}
}
