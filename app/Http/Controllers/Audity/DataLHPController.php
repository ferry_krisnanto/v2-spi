<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

class DataLHPController extends Controller
{
	public function index(){
		$LaporanAudit=DB::table('laporanaudit')
		->leftjoin('jadwal_audit', 'laporanaudit.id_jadwalAudit', '=', 'jadwal_audit.id')
		->leftjoin('tipe_audit', 'jadwal_audit.id_tipeaudit', '=', 'tipe_audit.id')
		->select(	'laporanaudit.id', 'laporanaudit.kode_dokumen', 'laporanaudit.created_at', 'laporanaudit.status',
							'jadwal_audit.tanggal_mulai', 'jadwal_audit.tanggal_selesai', 'tipe_audit.nama_tipe')
		->orderBy('laporanaudit.id', "DESC")->get();
		return view('auditor.data-lhp.index', compact('LaporanAudit'));
	}

	public function indexTambah(){
		$NomorSurat=DB::table('jadwal_audit')->select('id', 'nomorsurat')->get();
		return view('auditor.data-lhp.add', compact('NomorSurat'));
	}

	public function postTambah(Request $request){
		date_default_timezone_set("Asia/Jakarta");
		// $JadwalAudit=DB::table('jadwal_audit')->where('nomorsurat', $request->input('nomorsurat'))->value('id');
		// if (empty($JadwalAudit)) {
		// 	return back()->with('error', "Tidak ditemukan Nomor Surat Tugas" );
		// }

		DB::table('laporanaudit')->insert([
			'id_jadwalAudit'			=> $request->input('nomorsurat'),
			'nomor_urut'					=> $request->input('nouruttemuan'),
			'kode_dokumen'				=> $request->input('kodedokumen'),
			'kode_temuan'					=> $request->input('kodetemuan'),
			'deskripsi_temuan'		=> $request->input('deskripsitemuan'),
			'kriteria'						=> $request->input('kriteria'),
			'akibat'							=> $request->input('akibat'),
			'sebab'								=> $request->input('sebab'),
			'kesimpulan'					=> $request->input('simpulan'),
			'rekomendasi'					=> $request->input('rekomendasi'),
			'rencana_perbaikan'		=> $request->input('rencana'),
			'tindak_lanjut'				=> $request->input('tindaklanjut'),
			'jadwal_penyelesaian'	=> $request->input('jadwalpenyelesaian'),
			'created_at'					=> date("Y-m-d H:i:s"),
			'updated_at'					=> date("Y-m-d H:i:s"),
			'status'							=> "Proses"
			]);
			return redirect('/auditor/data-lhp')->with('success', "Data Berhasil Disimpan");
	}

	public function download($id){

	}

	public function hapus($id){

	}
}
