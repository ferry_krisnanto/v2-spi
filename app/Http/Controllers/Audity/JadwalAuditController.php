<?php

namespace App\Http\Controllers\Audity;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class JadwalAuditController extends Controller
{
    public function index(){
        $user=Auth::user();
        $auditor = DB::table('jadwal_audit')
        ->join('auditee','jadwal_audit.id_auditee','=','auditee.id')        
        ->join('p_auditoraudit', 'jadwal_audit.id', '=', 'p_auditoraudit.id_jadwalAudit')
        ->join('auditor','p_auditoraudit.id_auditor','=','auditor.id')
        ->join('users','auditor.id_user','=','users.id')
        ->select('jadwal_audit.id','jadwal_audit.nomorsurat')
        ->where('auditee.id_user','=',$user->id, 'and')
        ->where('p_auditoraudit.id_jabatan_audit','=',1 )        
        ->get();
        return view('audity.jadwalaudit')->with([
			'auditor'			=> $auditor,    
		]);
    }

    public function detail(Request $a){
        $user=Auth::user();        
        $id_jadwal = $a->jadwal_audit;
        $nomor_surat= DB::table('jadwal_audit') 
        ->select('nomorsurat')
        ->where ('id' ,'=', $id_jadwal)
        ->value('nomor_surat');

        $ketuaAuditor = DB::table('jadwal_audit')
        ->join('auditee','jadwal_audit.id_auditee','=','auditee.id')        
        ->join('p_auditoraudit', 'jadwal_audit.id', '=', 'p_auditoraudit.id_jadwalAudit')
        ->join('auditor','p_auditoraudit.id_auditor','=','auditor.id')
        ->join('users','auditor.id_user','=','users.id')
        ->select('users.nama')
        ->where('jadwal_audit.id','=',$id_jadwal)
        ->where('p_auditoraudit.id_jabatan_audit','=',1 )        
        ->value('ketuaAuditor');

        $unitKerja = DB::table('jadwal_audit')
        ->join('auditee','jadwal_audit.id_auditee','=','auditee.id')        
        ->join('p_auditoraudit', 'jadwal_audit.id', '=', 'p_auditoraudit.id_jadwalAudit')
        ->join('auditor','p_auditoraudit.id_auditor','=','auditor.id')
        ->join('users','auditor.id_user','=','users.id')
        ->select('users.nama')
        ->where('jadwal_audit.id','=',$id_jadwal)
        ->where('p_auditoraudit.id_jabatan_audit','!=',1 )        
        ->get();

        $tipe = DB::table('jadwal_audit')
        ->join('tipe_audit','jadwal_audit.id_tipeaudit','=','tipe_audit.id')
        ->select('nama_tipe')
        ->where('jadwal_audit.id','=',$id_jadwal)       
        ->value('tipe');
        
        $aspek_audit = DB::table('p_jadwalaspekaudit')
        ->join('jadwal_audit','p_jadwalaspekaudit.id_jadwalAudit','=','jadwal_audit.id')
        ->join('aspek_audit', 'p_jadwalaspekaudit.id_aspekaudit', '=', 'aspek_audit.id')        
        ->select('nama_aspek')
        ->where('jadwal_audit.id','=',$id_jadwal)        
        ->get();
            
        $tanggal_mulai= DB::table('jadwal_audit') 
        ->select('tanggal_mulai')
        ->where ('id' ,'=', $id_jadwal)
        ->value('tanggal_mulai');

        $tanggal_selesai= DB::table('jadwal_audit') 
        ->select('tanggal_selesai')
        ->where ('id' ,'=', $id_jadwal)
        ->value('tanggal_selesai');

        $dokumen= DB::table('jadwal_audit') 
        ->select('attachment_surattugas')
        ->where ('id' ,'=', $id_jadwal)
        ->value('dokumen');

        $keterangan= DB::table('jadwal_audit') 
        ->select('keterangan')
        ->where ('id' ,'=', $id_jadwal)
        ->value('keterangan');
        return view('audity.detjadwalaudit')->with([
            'nomor_surat'			=> $nomor_surat,
            'ketuaAuditor'          => $ketuaAuditor,
            'unitKerja'             => $unitKerja,
            'tipe'                  => $tipe,
            'aspek_audit'           => $aspek_audit,
            'tanggal_mulai'         => $tanggal_mulai,
            'tanggal_selesai'       => $tanggal_selesai,
            'dokumen'               => $dokumen,
            'keterangan'            => $keterangan,
		]);;
    }
}
