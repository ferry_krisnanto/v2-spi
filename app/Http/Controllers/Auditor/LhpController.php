<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\LaporanAudit;
use App\Model\TipeAudit;
use App\Model\JadwalAudit;
use App\Model\TanggapanAudit;
use DB;

class LhpController extends Controller
{

	public function index(){
		$lap = LaporanAudit::with(['jadwalAudit', 'tanggapanAudit'])->get();
		return view('auditor.data-lhp.index')->with([
			'lap'				=> $lap,
		]);
	}

	public function postEdit(Request $req){
		// dd($req->idnya);
		// return "coba";

		$modify = DB::table('tanggapanaudit')
			->where('id', $req->idnya)
			->update([
				'tanggapan_auditor' => $req->tanggapan_auditor
				]);

		if ($modify) {
			return back()->with('success', "Berhasil mengubah data Tanggapan Auditor");
		}
		else{
			return back()->with('danger', "Gagal mengubah data Tanggapan Auditor");
		}

	}

	public function search($id){

		$modify = TanggapanAudit::where('id_laporanaudit', $id)->first();
		$data['kode_dokumen'] = $modify->laporanAudit->kode_dokumen;
		$data['id'] = $modify->id;
		$data['tanggapan_auditor'] = $modify->tanggapan_auditor;

		return response()->json($data);
	}

	public function hapus($id){
		$modify = TanggapanAudit::where('id_laporanaudit', $id)->first();
		$modify->tanggapan_rektor = '-';
		$modify->save();

		return back()->with('success', "Berhasil menghapus tanggapan rektor");
	}


}
