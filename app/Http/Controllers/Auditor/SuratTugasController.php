<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use DB;

class SuratTugasController extends Controller
{
	public function index(){
		$SuratTugas=DB::table('p_auditoraudit')
			->leftjoin('auditor', 'p_auditoraudit.id_auditor', '=', 'auditor.id')
			->leftjoin('jadwal_audit', 'p_auditoraudit.id_jadwalAudit', '=', 'jadwal_audit.id')
			->leftjoin('auditee', 'jadwal_audit.id_auditee', '=', 'auditee.id')
			->select('auditor.id_user', 'jadwal_audit.id', 'jadwal_audit.nomorsurat', 'jadwal_audit.tanggal_mulai', 'jadwal_audit.tanggal_selesai', 'jadwal_audit.attachment_surattugas', 'jadwal_audit.created_at', 'auditee.unit_kerja')
			->where('auditor.id_user', Auth::user()->id)
			->orderBy('jadwal_audit.id', 'DESC')
			->distinct()->get();
		return view('auditor.surat-tugas.index', compact('SuratTugas'));
	}

	public function view($id){
		$pdf=DB::table('jadwal_audit')->where('id', $id)->value('attachment_surattugas');
		return view('auditor.surat-tugas.view', compact('pdf'));
	}
}
