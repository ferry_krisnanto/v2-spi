<?php

namespace App\Http\Controllers\Auditor;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;

class DataLHPController extends Controller
{
	public function index(){
		$LaporanAudit=DB::table('laporanaudit')
		->leftjoin('jadwal_audit', 'laporanaudit.id_jadwalAudit', '=', 'jadwal_audit.id')
		->leftjoin('tipe_audit', 'jadwal_audit.id_tipeaudit', '=', 'tipe_audit.id')
		->select(	'laporanaudit.id', 'laporanaudit.kode_dokumen', 'laporanaudit.created_at', 'laporanaudit.status',
					'jadwal_audit.tanggal_mulai', 'jadwal_audit.tanggal_selesai', 'tipe_audit.nama_tipe')
		->orderBy('laporanaudit.id', "DESC")->get();
		return view('auditor.data-lhp.index', compact('LaporanAudit'));
	}

	public function indexTambah(){
		$NomorNOT=DB::table('laporanaudit')
			->leftjoin('jadwal_audit', 'laporanaudit.id_jadwalAudit', '=', 'jadwal_audit.id')
			->select('jadwal_audit.nomorsurat')
			->pluck('nomorsurat');
			//dd($NomorNOT);
		$NomorSurat=DB::table('p_auditoraudit')
			->leftjoin('auditor', 'p_auditoraudit.id_auditor', '=', 'auditor.id')
			->leftjoin('jadwal_audit', 'p_auditoraudit.id_jadwalAudit', '=', 'jadwal_audit.id')
			->select('auditor.id_user', 'jadwal_audit.id', 'jadwal_audit.nomorsurat')
			->where('auditor.id_user', Auth::user()->id)
			->whereNotIn('jadwal_audit.nomorsurat', $NomorNOT)
			->orderBy('jadwal_audit.id', 'DESC')
			->distinct()->get();
		return view('auditor.data-lhp.add', compact('NomorSurat', 'NomorNOT'));
	}

	public function postTambah(Request $request){
		date_default_timezone_set("Asia/Jakarta");

		DB::table('laporanaudit')->insert([
			'id_jadwalAudit'			=> $request->input('nomorsurat'),
			'nomor_urut'				=> $request->input('nouruttemuan'),
			'kode_dokumen'				=> $request->input('kodedokumen'),
			'kode_temuan'				=> $request->input('kodetemuan'),
			'deskripsi_temuan'			=> $request->input('deskripsitemuan'),
			'kriteria'					=> $request->input('kriteria'),
			'akibat'					=> $request->input('akibat'),
			'sebab'						=> $request->input('sebab'),
			'kesimpulan'				=> $request->input('simpulan'),
			'rekomendasi'				=> $request->input('rekomendasi'),
			'rencana_perbaikan'			=> $request->input('rencana'),
			'tindak_lanjut'				=> $request->input('tindaklanjut'),
			'jadwal_penyelesaian'		=> $request->input('jadwalpenyelesaian'),
			'tanggapan_auditor'			=> $request->input('tanggapanauditor'),
			'created_at'				=> date("Y-m-d H:i:s"),
			'updated_at'				=> date("Y-m-d H:i:s"),
			'status'					=> "Proses"
			]);
			return redirect('/auditor/data-lhp')->with('success', "Data Berhasil Disimpan");
	}

	public function edit($id){
		$Data=DB::table('laporanaudit')
		->where('laporanaudit.id', $id)
		->leftjoin('jadwal_audit', 'laporanaudit.id_jadwalAudit', '=', 'jadwal_audit.id')
		->select('laporanaudit.*', 'jadwal_audit.nomorsurat')
		->first();
		return view('auditor.data-lhp.edit', compact('Data'));
	}

	public function editPost(Request $request){
		DB::table('laporanaudit')
			->leftjoin('jadwal_audit', 'laporanaudit.id_jadwalAudit', '=', 'jadwal_audit.id')
			->select('laporanaudit.*', 'jadwal_audit.nomorsurat','tanggapanaudit.tanggapan_auditor')
		->where('jadwal_audit.nomorsurat', $request->input('nomorsurat'))
		->update([
			'nomor_urut'				=> $request->input('nouruttemuan'),
			'kode_dokumen'				=> $request->input('kodedokumen'),
			'kode_temuan'				=> $request->input('kodetemuan'),
			'deskripsi_temuan'			=> $request->input('deskripsitemuan'),
			'kriteria'					=> $request->input('kriteria'),
			'akibat'					=> $request->input('akibat'),
			'sebab'						=> $request->input('sebab'),
			'kesimpulan'				=> $request->input('simpulan'),
			'rekomendasi'				=> $request->input('rekomendasi'),
			'rencana_perbaikan'			=> $request->input('rencana'),
			'tindak_lanjut'				=> $request->input('tindaklanjut'),
			'jadwal_penyelesaian'		=> $request->input('jadwalpenyelesaian'),
			'tanggapan_auditor'			=> $request->input('tanggapanauditor'),
			'laporanaudit.updated_at'	=> date("Y-m-d H:i:s"),
		]);
		return redirect('/auditor/data-lhp')->with('success', "Data Berhasil Disimpan");
	}

	public function download($id){

	}
	

	public function hapus($id){
		DB::table('laporanaudit')->where('id', $id)->delete();
		return back()->with('success', "Data Berhasil Dihapus");
	}
}
