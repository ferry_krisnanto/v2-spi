<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->id_role == 1) {
        	return redirect()->route('ketuaspi');
        }
		elseif(Auth::user()->id_role == 2){
			return redirect()->route('operator');
		}
		elseif(Auth::user()->id_role == 3){
			return redirect()->route('auditor');
		}
		elseif(Auth::user()->id_role == 4){
			return redirect()->route('audity');
		}
		elseif(Auth::user()->id_role == 5){
			return redirect()->route('koorbidang');
		}
		elseif(Auth::user()->id_role == 6){
			return redirect()->route('rektor');
		}
    }
}
