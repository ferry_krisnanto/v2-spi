<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\AspekAudit;
class AspekAuditController extends Controller
{
	public function index(){
		$aspek = AspekAudit::all();
		return view('operator.aspek-audit.index', compact('aspek'));
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'nama_aspek'		=> 'required|string',
		]);

		$insert 			= new AspekAudit;
		$insert->nama_aspek	= $req->nama_aspek;
		$insert->save();

		return back()->with('success', 'Berhasil menambah Aspek Audit');
	}

	public function search($id){
		$aspek = AspekAudit::find($id);

		return response()->json($aspek);
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'id'			=> 'required',
			'nama_aspek'	=> 'required|string',
		]);

		$insert 			= AspekAudit::find($req->id);
		$insert->nama_aspek	= $req->nama_aspek;
		$insert->save();

		return back()->with('success', 'Berhasil mengubah Aspek Audit');
	}

	public function hapus($id){
		$aspek = AspekAudit::find($id);
		$aspek->forceDelete();

		return back()->with('success', 'Berhasil menghapus Aspek Audit');
	}
}
