<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\TipeAudit;
use App\Model\AspekAudit;
use App\Model\JadwalAudit;
use App\Model\Auditor;
use App\Model\Auditee;
use App\Model\JadwalAspekAudit;
use App\Model\AuditorAudit;
use Storage;
use Carbon\Carbon;
use Excel;

class JadwalAuditController extends Controller
{
	public function pertahun(){
		$date			= Carbon::today();
		$tahun 			= $date->year;
		$jadwal 		= JadwalAudit::where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())->get();

		return view('operator.jadwal-audit.pertahun', compact(['jadwal', 'tahun']));
	}

	public function semua(){
		$jadwal = JadwalAudit::all();
		return view('operator.jadwal-audit.semua', compact('jadwal'));
	}

	public function download($tahun){
		$date			= Carbon::today();
		$tahun 			= $tahun;
		$jadwal 		= JadwalAudit::where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())->get();

		$excel = Excel::create('rekap-jadwal-tahun-'.$tahun, function($excel) use ($jadwal) {

		    $excel->sheet('Sheet1', function($sheet) use ($jadwal) {
				$sheet->mergeCells('A1:A2');
				$sheet->mergeCells('B1:B2');
				$sheet->mergeCells('C1:N1');
				$sheet->setAllBorders('solid');
		        $sheet->loadView('excel.jadwal')->with([
					'jadwal'	=> $jadwal,
				]);
		    });

		})->download('csv');
	}

	public function searchAuditor($id){

	}

	public function search(Request $req)
    {
		$date			= Carbon::create($req->tahun);
		$tahun 			= $date->year;
		$jadwal 		= JadwalAudit::where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())->get();
		return view('operator.jadwal-audit.pertahun', compact(['jadwal', 'tahun']));
    }

	public function getTambah(){
		$tipeAudit 	= TipeAudit::all();
		$aspekAudit = AspekAudit::all();
		$auditor	= Auditor::all();
		$auditee	= Auditee::all();
		return view('operator.jadwal-audit.tambah')->with([
			'tipeAudit'			=> $tipeAudit,
			'aspekAudit'		=> $aspekAudit,
			'auditor'			=> $auditor,
			'auditee'			=> $auditee
		]);
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'nomorsurat' 		=> 'required|max:100|unique:jadwal_audit|regex:/\//',
			'ketua_audit' 		=> 'required',
			'sekretaris' 		=> 'required',
			'anggota_tim' 		=> 'required',
			'auditee' 			=> 'required',
			'id_tipeaudit' 		=> 'required',
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_selesai' 	=> 'required|date',
			'surat_tugas' 		=> 'required|mimes:pdf',
			'keterangan'		=> 'max:255|alpha_num'
		]);

		$file		= $req->surat_tugas;
		$nameRaw	= $file->getClientOriginalName();
		$filename	= mt_rand(100, 999).'-'.$nameRaw;
		Storage::disk('surat-tugas')->put($filename, file_get_contents($file));

		$jadwal 						= new JadwalAudit();
		$jadwal->id_auditee 			= $req->auditee;
		$jadwal->id_tipeaudit 			= $req->id_tipeaudit;
		$jadwal->nomorsurat 			= $req->nomorsurat;
		$jadwal->tanggal_mulai 			= date('Y-m-d', strtotime($req->tanggal_mulai));
		$jadwal->tanggal_selesai 		= date('Y-m-d', strtotime($req->tanggal_selesai));
		$jadwal->attachment_surattugas 	= $filename;
		$jadwal->keterangan 			= $req->keterangan;
		$jadwal->save();

		$idJadwal = $jadwal->id;
		if ($req->id_tipeaudit == 1) {
				$aspek = AspekAudit::all();
				foreach ($aspek as $key => $value) {
					$aspekAudit 					= new JadwalAspekAudit();
					$aspekAudit->id_jadwalAudit 	= $idJadwal;
					$aspekAudit->id_aspekaudit 		= $value->id;
					$aspekAudit->save();
				}

		}
		else{
			foreach ($req->aspek_audit as $key => $value) {
				$aspekAudit 					= new JadwalAspekAudit();
				$aspekAudit->id_jadwalAudit 	= $idJadwal;
				$aspekAudit->id_aspekaudit 		= $value;
				$aspekAudit->save();
			}
		}

		$ketuaAudit 					= new AuditorAudit();
		$ketuaAudit->id_jadwalAudit 	= $idJadwal;
		$ketuaAudit->id_auditor 		= $req->ketua_audit;
		$ketuaAudit->id_jabatan_audit 	= 1;
		$ketuaAudit->save();

		$sekretaris 					= new AuditorAudit();
		$sekretaris->id_jadwalAudit 	= $idJadwal;
		$sekretaris->id_auditor 		= $req->sekretaris;
		$sekretaris->id_jabatan_audit 	= 2;
		$sekretaris->save();

		foreach ($req->anggota_tim as $key => $value) {
			$anggotaTim 					= new AuditorAudit();
			$anggotaTim->id_jadwalAudit 	= $idJadwal;
			$anggotaTim->id_auditor 		= $value;
			$anggotaTim->id_jabatan_audit 	= 3;
			$anggotaTim->save();
		}

		if (!empty($req->anggota_penunjang)) {
			foreach ($req->anggota_penunjang as $key => $value) {
				$anggotaTim 					= new AuditorAudit();
				$anggotaTim->id_jadwalAudit 	= $idJadwal;
				$anggotaTim->id_auditor 		= $value;
				$anggotaTim->id_jabatan_audit 	= 4;
				$anggotaTim->save();
			}
		}
		if (!empty($req->staff_audit)) {
			foreach ($req->staff_audit as $key => $value) {
				$anggotaTim 					= new AuditorAudit();
				$anggotaTim->id_jadwalAudit 	= $idJadwal;
				$anggotaTim->id_auditor 		= $value;
				$anggotaTim->id_jabatan_audit 	= 5;
				$anggotaTim->save();
			}
		}

		return redirect()->route('operator.jadwal-audit.semua')->with('success', "Berhasil menambah jadwal audit");
	}

	public function getEdit($id){
		$jadwal 	= JadwalAudit::find($id);
		$ketua 		= AuditorAudit::select('id_auditor')->where('id_jadwalAudit', $jadwal->id)->where('id_jabatan_audit', 1)->first();
		$sekretaris = AuditorAudit::select('id_auditor')->where('id_jadwalAudit', $jadwal->id)->where('id_jabatan_audit', 2)->first();
		$tim 		= AuditorAudit::select('id_auditor')->where('id_jadwalAudit', $jadwal->id)->where('id_jabatan_audit', 3)->get()->pluck('id_auditor')->toArray();
		$penunjang 	= AuditorAudit::select('id_auditor')->where('id_jadwalAudit', $jadwal->id)->where('id_jabatan_audit', 4)->get()->pluck('id_auditor')->toArray();
		$staff 		= AuditorAudit::select('id_auditor')->where('id_jadwalAudit', $jadwal->id)->where('id_jabatan_audit', 5)->get()->pluck('id_auditor')->toArray();
		$aspek 		= JadwalAspekAudit::select('id_aspekaudit')->where('id_jadwalAudit', $jadwal->id)->get()->pluck('id_aspekaudit')->toArray();
		$tipeAudit 	= TipeAudit::all();
		$aspekAudit = AspekAudit::all();
		$auditor	= Auditor::all();
		$auditee	= Auditee::all();
		return view('operator.jadwal-audit.edit', [
			'jadwal' 		=> $jadwal,
			'ketua' 		=> $ketua,
			'sekretaris' 	=> $sekretaris,
			'tim' 			=> $tim,
			'staff' 		=> $staff,
			'penunjang' 	=> $penunjang,
			'tipeAudit' 	=> $tipeAudit,
			'aspekAudit' 	=> $aspekAudit,
			'auditor' 		=> $auditor,
			'auditee' 		=> $auditee,
			'aspek'			=> $aspek,
		]);
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'id'				=> 'required',
			'nomorsurat' 		=> 'required|max:100|regex:/\//',
			'ketua_audit' 		=> 'required',
			'sekretaris' 		=> 'required',
			'anggota_tim' 		=> 'required',
			'auditee' 			=> 'required',
			'id_tipeaudit' 		=> 'required',
			'tanggal_mulai' 	=> 'required|date',
			'tanggal_selesai' 	=> 'required|date',
			'surat_tugas' 		=> 'mimes:pdf',
			'keterangan'		=> 'max:255|alpha_num',
			'nama_surat'		=> 'required'
		]);

		if ($req->hasFile('surat_tugas')) {
			Storage::disk('surat-tugas')->delete($req->nama_surat);
			$file		= $req->surat_tugas;
			$nameRaw	= $file->getClientOriginalName();
			$filename	= mt_rand(100, 999).'-'.$nameRaw;
			Storage::disk('surat-tugas')->put($filename, file_get_contents($file));
		}
		else{
			$filename = $req->nama_surat;
		}
		$jadwal 						= JadwalAudit::find($req->id);
		$jadwal->id_auditee 			= $req->auditee;
		$jadwal->id_tipeaudit 			= $req->id_tipeaudit;
		$jadwal->nomorsurat 			= $req->nomorsurat;
		$jadwal->tanggal_mulai 			= date('Y-m-d', strtotime($req->tanggal_mulai));
		$jadwal->tanggal_selesai 		= date('Y-m-d', strtotime($req->tanggal_selesai));
		$jadwal->attachment_surattugas 	= $filename;
		$jadwal->keterangan 			= $req->keterangan;
		$jadwal->save();

		$idJadwal = $req->id;
		JadwalAspekAudit::where('id_jadwalAudit', $idJadwal)->forceDelete();
		AuditorAudit::where('id_jadwalAudit', $idJadwal)->forceDelete();

		if ($req->id_tipeaudit == 1) {
				$aspek = AspekAudit::all();
				foreach ($aspek as $key => $value) {
					$aspekAudit 					= new JadwalAspekAudit();
					$aspekAudit->id_jadwalAudit 	= $idJadwal;
					$aspekAudit->id_aspekaudit 		= $value->id;
					$aspekAudit->save();
				}

		}

		else{
			foreach ($req->aspek_audit as $key => $value) {
				$aspekAudit 					= new JadwalAspekAudit();
				$aspekAudit->id_jadwalAudit 	= $idJadwal;
				$aspekAudit->id_aspekaudit 		= $value;
				$aspekAudit->save();
			}
		}

		$ketuaAudit 					= new AuditorAudit();
		$ketuaAudit->id_jadwalAudit 	= $idJadwal;
		$ketuaAudit->id_auditor 		= $req->ketua_audit;
		$ketuaAudit->id_jabatan_audit 	= 1;
		$ketuaAudit->save();

		$sekretaris 					= new AuditorAudit();
		$sekretaris->id_jadwalAudit 	= $idJadwal;
		$sekretaris->id_auditor 		= $req->sekretaris;
		$sekretaris->id_jabatan_audit 	= 2;
		$sekretaris->save();

		foreach ($req->anggota_tim as $key => $value) {
			$anggotaTim 					= new AuditorAudit();
			$anggotaTim->id_jadwalAudit 	= $idJadwal;
			$anggotaTim->id_auditor 		= $value;
			$anggotaTim->id_jabatan_audit 	= 3;
			$anggotaTim->save();
		}

		if (!empty($req->anggota_penunjang)) {
			foreach ($req->anggota_penunjang as $key => $value) {
				$anggotaTim 					= new AuditorAudit();
				$anggotaTim->id_jadwalAudit 	= $idJadwal;
				$anggotaTim->id_auditor 		= $value;
				$anggotaTim->id_jabatan_audit 	= 4;
				$anggotaTim->save();
			}
		}
		if (!empty($req->staff_audit)) {
			foreach ($req->staff_audit as $key => $value) {
				$anggotaTim 					= new AuditorAudit();
				$anggotaTim->id_jadwalAudit 	= $idJadwal;
				$anggotaTim->id_auditor 		= $value;
				$anggotaTim->id_jabatan_audit 	= 5;
				$anggotaTim->save();
			}
		}

		return redirect()->route('operator.jadwal-audit.semua')->with('success', "Berhasil mengubah jadwal audit");
	}

	public function hapus($id){
		$jadwal = JadwalAudit::find($id);
		Storage::disk('surat-tugas')->delete($jadwal->attachment_surattugas);
		$jadwal->forceDelete();

		return redirect()->route('operator.jadwal-audit.semua')->with('success', "Berhasil menghapus jadwal audit");
	}
}
