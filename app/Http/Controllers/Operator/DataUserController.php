<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\User;
use DB;

class DataUserController extends Controller
{
	public function index(){
		$user = User::all();
		$jabatans = DB::table('roles')->get();
		return view('operator.data-user.index', compact('user', 'jabatans'));

	}

	public function postTambah(Request $req){
		$this->validate($req, [
			

		]);

		
	}
	public function postEdit(Request $req){

	}

	public function resetPassword(Request $req){
		$this->validate($req, [
			'idUser'	=> 'required',
			'password' 	=> 'required|string|min:6|confirmed',
		]);

		$user 			= User::find($req->idUser);
		$user->username = $req->username;
		$user->password	= bcrypt($req->password);
		if ($user->save()) {
			return back()->with('success', "Berhasil me-reset password");
		}
		else{
			return back()->with('danger', "Berhasil me-reset password");
		}
	}

	public function search($id){
		$user = User::find($id);
		
		return response()->json($user);
	}

	public function hapus($id){
		$user = User::find($id);
		$user->forceDelete();

		return back()->with('success', "Berhasil menghapus User");
	}
}
