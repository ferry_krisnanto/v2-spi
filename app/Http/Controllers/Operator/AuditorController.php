<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fakultas;
use App\Model\Jurusan;
use App\Model\Golongan;
use App\Model\Auditor;
use Faker\Factory as Faker;
use App\Model\User;
use DB;

class AuditorController extends Controller
{
	public function index(){
		$fak = Fakultas::all();
		$jur = Jurusan::all();
		$gol = Golongan::all();
		$auditor = Auditor::all();
		return view('operator.auditor.index')->with([
			'fak'		=> $fak,
			'gol'		=> $gol,
			'jur'		=> $jur,
			'auditor'	=> $auditor,
		]);
	}

	public function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'nama'			=> 'required|string|max:50',
			'id_jurusan'	=> 'required',
			'nip'			=> 'required|numeric',
			'id_golongan'	=> 'required',
		]);
		$rand = substr(uniqid('', true), -5);
		$faker = Faker::create();

		$auditor = Auditor::select('nip')->where('nip', $req->nip)->first();
		if (!empty($auditor)) {
			return back()->with('danger', "NIP Sudah Ada.");
		}

		$createUser = new User();
		$createUser->nama 		= $req->nama;
		$createUser->username 	= $this->generateRandomString(6);
		$createUser->email		= $faker->email;
		$createUser->password 	= bcrypt($rand);
		$createUser->id_role	= 3;
		$createUser->save();

		$create = new Auditor();
		$create->id_user		= $createUser->id;
		$create->id_jurusan	= $req->id_jurusan;
		$create->nip			= $req->nip;
		$create->id_golongan	= $req->id_golongan;
		$create->token			= $rand;
		$create->save();

		if ($create && $createUser) {
			return back()->with('success', "Berhasil menambah Auditor");
		}
		else{
			return back()->with('danger', "Gagal menambah Auditor");
		}

	}

	public function search($id){
		$auditor = Auditor::find($id);
		$data['id']				= $id;
		$data['nama']			= $auditor->user->nama;
		$data['id_fakultas']	= $auditor->jurusan->id_fakultas;
		$data['id_jurusan']		= $auditor->id_jurusan;
		$data['id_golongan']	= $auditor->id_golongan;

		return response()->json($data);
	}

	public function getJurusan($id){
		$jur = Jurusan::where('id_fakultas', $id)->get();

		return response()->json($jur);
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'idAuditor'		=> 'required',
			'nama'			=> 'required|string|max:50',
			'id_jurusan'	=> 'required',
			'id_golongan'	=> 'required',
		]);

		$modify = Auditor::find($req->idAuditor);
		$idUser = $modify->id_user;

		$modify->id_jurusan		= $req->id_jurusan;
		$modify->id_golongan	= $req->id_golongan;
		$modify->save();

		$modifyUser = User::find($idUser);
		$modifyUser->nama 		= $req->nama;
		$modifyUser->save();

		if ($modify && $modifyUser) {
			return back()->with('success', "Berhasil mengubah data Auditor");
		}
		else{
			return back()->with('danger', "Gagal mengubah data Auditor");
		}

	}

	public function hapus($id){
		$auditor = Auditor::find($id);
		$user	 = User::find($auditor->id_user);
		$user->forceDelete();

		return back()->with('success', "Berhasil menghapus Auditor");
	}
}
