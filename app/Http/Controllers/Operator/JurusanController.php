<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Jurusan;

class JurusanController extends Controller
{
	public function postEdit(Request $req){
		$this->validate($req, [
			'nama_jurusan'	=> 'required|string'
		]);

		$input=Jurusan::find($req->id);
		$input->id_fakultas = $req->id_fakultas;
		$input->nama_jurusan = $req->nama_jurusan;
		$input->save();

		return back()->with('success', "Jurusan berhasil diubah");
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'nama_jurusan'	=> 'required|string',
			'id_fakultas'	=> 'required'
		]);

		$input=new Jurusan;
		$input->id_fakultas = $req->id_fakultas;
		$input->nama_jurusan = $req->nama_jurusan;
		$input->save();

		return back()->with('success', "Jurusan berhasil ditambah");
	}

	public function hapus($id){
		$fak=Jurusan::find($id);
		$fak->forceDelete();
		return back()->with('success', "Jurusan berhasil dihapus");
	}

	public function search($id){
		$search = Jurusan::find($id);
		$data['id']						= $id;
		$data['id_fakultas']			= $search->id_fakultas;
		$data['nama_jurusan']			= $search->nama_jurusan;

		return response()->json($data);
	}
}
