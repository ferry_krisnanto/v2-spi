<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Auditee;
use App\Model\User;
use Faker\Factory as Faker;

class AuditiController extends Controller
{
    public function index(){
		$auditee = Auditee::all();
		return view('operator.auditi.index', compact('auditee'));
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'unit_kerja'		=> 'required|string|max:50',
			'pimpinan_kerja'	=> 'required|string|max:50',
			'nip'				=> 'required|numeric',
			'tahun_mulai'		=> 'required|numeric',
			'tahun_selesai'		=> 'required',
			'keterangan'		=> 'required|string|max:100',
		]);

		$masa_kerja = $req->tahun_mulai.'-'.$req->tahun_selesai;

		$rand 			= substr(uniqid('', true), -5);
		$faker 			= Faker::create();
		
		//cek untuk nip audity
		$auditee = Auditee::select('nip')->where('nip', $req->nip)->first();
		if (!empty($auditee)) {
			return back()->with('danger', "NIP Sudah Ada.");
		}

		DB::beginTransaction();
		try {
			$createUser 	= User::create([
				'nama'			=> $req->unit_kerja,
				'username'		=> $this->generateRandomString(6),
				'password'		=> bcrypt($rand),
				'email'			=> $faker->email,
				'id_role'		=> 4,
			]);

			$input = Auditee::create([
				'id_user'			=> $createUser->id,
				'unit_kerja'		=> $req->unit_kerja,
				'pimpinan_kerja'	=> $req->pimpinan_kerja,
				'nip'				=> $req->nip,
				'masa_kerja'		=> $masa_kerja,
				'keterangan'		=> $req->keterangan,
				'token'				=> $rand,
			]);
			DB::commit();
			return back()->with('success', "Berhasil menambah Auditee");
		} catch (Exception $e) {
			DB::rollBack();
			return back()->with('danger', "Gagal menambah Auditee");
		}

		// if ($input && $createUser) {
		// 	return back()->with('success', "Berhasil menambah Auditee");
		// }
		
		// else{
		// 	return back()->with('danger', "Gagal menambah Auditee");
		// }
	}

	public function generateRandomString($length) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++) {
	        $randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'idAuditee'			=> 'required',
			'unit_kerja'		=> 'required|string|max:50',
			'pimpinan_kerja'	=> 'required|string|max:50',
			'tahun_mulai'		=> 'required|numeric',
			'tahun_selesai'		=> 'required',
			'keterangan'		=> 'required|string|max:100',
		]);

		$masa_kerja = $req->tahun_mulai.'-'.$req->tahun_selesai;

		$auditee = Auditee::find($req->idAuditee);
		$auditee->unit_kerja		= $req->unit_kerja;
		$auditee->pimpinan_kerja	= $req->pimpinan_kerja;
		$auditee->masa_kerja		= $masa_kerja;
		$auditee->keterangan		= $req->keterangan;

		if ($auditee->save()) {
			return back()->with('success', "Berhasil mengubah data Auditee");
		}
		else{
			return back()->with('danger', "Gagal mengubah data Auditee");
		}
	}

	public function search($id){
		$auditi = Auditee::find($id);

		$tahun = explode('-', $auditi->masa_kerja);

		$auditee['id']				= $auditi->id;
		$auditee['unit_kerja']		= $auditi->unit_kerja;
		$auditee['pimpinan_kerja']	= $auditi->pimpinan_kerja;
		$auditee['keterangan']		= $auditi->keterangan;
		$auditee['tahun_mulai']		= $tahun[0];
		$auditee['tahun_selesai']	= $tahun[1];

		return response()->json($auditee);
	}

	// public function search($id){

	// 	$auditee = Auditee::find($id);
	// 	return json_encode($auditee);

	// 	$tahun = explode('-', $auditee->masa_kerja);

	// 	$data['id']				= $id;
	// 	$id1					= 1000;
	// 	$data['nama']			= $auditee->user->nama;
	// 	$data['id_fakultas']	= $auditee->jurusan->id_fakultas;
	// 	$data['id_jurusan']		= $auditee->id_jurusan;
	// 	$data['id_golongan']	= $auditee->id_golongan;

	// 	return response()->json($auditee);
	// }
	
	public function hapus($id){
		$auditee = Auditee::find($id);
		$user	 = User::find($auditee->id_user);
		$user->forceDelete();

		return back()->with('success', "Berhasil menghapus Auditee");
	}
}
