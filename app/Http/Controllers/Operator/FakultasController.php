<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Fakultas;
use App\Model\Jurusan;

class FakultasController extends Controller
{
    public function index(){
		$fakultas = Fakultas::all();
		$jurusan  = Jurusan::all();

		return view('operator.fakultas.index', compact(['fakultas', 'jurusan']));
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'nama_fakultas'	=> 'required|string'
		]);

		$input=Fakultas::find($req->id);
		$input->nama_fakultas = $req->nama_fakultas;
		$input->save();

		return back()->with('success', "Fakultas berhasil diubah");
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'nama_fakultas'	=> 'required|string'
		]);

		$input=new Fakultas;
		$input->nama_fakultas = $req->nama_fakultas;
		$input->save();

		return back()->with('success', "Fakultas berhasil ditambah");
	}

	public function hapus($id){
		$fak=Fakultas::find($id);
		$fak->forceDelete();
		return back()->with('success', "Fakultas berhasil dihapus");
	}

	public function search($id){
		$search = Fakultas::find($id);
		
		$data['id']						= $id;
		$data['nama_fakultas']			= $search->nama_fakultas;

		return response()->json($data);
	}
}
