<?php

namespace App\Http\Controllers\Operator;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Golongan;
class GolonganController extends Controller
{
	public function index(){
		$golongan = Golongan::all();
		return view('operator.golongan.index', compact('golongan'));
	}

	public function postTambah(Request $req){
		$this->validate($req, [
			'nama'		=> 'required|string',
		]);

		$insert 			= new Golongan;
		$insert->nama		= $req->nama;
		$insert->save();

		return back()->with('success', 'Berhasil menambah Aspek Audit');
	}

	public function search($id){
		$golongan = Golongan::find($id);

		return response()->json($golongan);
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'id'			=> 'required',
			'nama'			=> 'required|string',
		]);

		$insert 			= Golongan::find($req->id);
		$insert->nama		= $req->nama;
		$insert->save();

		return back()->with('success', 'Berhasil mengubah Aspek Audit');
	}

	public function hapus($id){
		$golongan = Golongan::find($id);
		$golongan->forceDelete();

		return back()->with('success', 'Berhasil menghapus Aspek Audit');
	}
}
