<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Model\JadwalAudit;
use DB;
use Excel;

class GuestController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$date			= Carbon::today();
		$tahun 			= $date->year;
		$jadwal 		= JadwalAudit::where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())->get();
		$jadwalJson 	= DB::table('jadwal_audit')
		->join('auditee', 'jadwal_audit.id_auditee', '=', 'auditee.id')
		->select(['auditee.unit_kerja as title', 'tanggal_mulai as start', 'tanggal_selesai as end'])
		->where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())
		->get();

		// dd($jadwalJson);
		return view('welcome', compact(['jadwal', 'tahun', 'jadwalJson']));
    }

	public function cetakJadwal($tahun){
		$date			= Carbon::today();
		$tahun 			= $tahun;
		$jadwal 		= JadwalAudit::where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())->get();

		$excel = Excel::create('rekap-jadwal-tahun-'.$tahun, function($excel) use ($jadwal) {

		    $excel->sheet('Sheet1', function($sheet) use ($jadwal) {
				$sheet->mergeCells('A1:A2');
				$sheet->mergeCells('B1:B2');
				$sheet->mergeCells('C1:N1');
				$sheet->setAllBorders('solid');
		        $sheet->loadView('excel.jadwal')->with([
					'jadwal'	=> $jadwal,
				]);
		    });

		})->download('csv');
	}

	public function search(Request $req)
    {
		$date			= Carbon::create($req->tahun);
		$tahun 			= $date->year;
		$jadwal 		= JadwalAudit::where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())->get();
		$jadwalJson 	= DB::table('jadwal_audit')
		->join('auditee', 'jadwal_audit.id_auditee', '=', 'auditee.id')
		->select(['auditee.unit_kerja as title', 'tanggal_mulai as start', 'tanggal_selesai as end'])
		->where('tanggal_mulai', '>=', $date->startOfYear()->toDateString())->where('tanggal_selesai', '<=', $date->endOfYear()->toDateString())
		->get();
		return view('welcome', compact(['jadwal', 'tahun', 'jadwalJson']));
    }
}
