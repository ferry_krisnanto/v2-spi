<?php

namespace App\Http\Controllers\KoorBidang;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\LaporanAudit;
use App\Model\Auditee;
use App\Model\TipeAudit;
use App\Model\JadwalAudit;
use App\Model\TanggapanAudit;
use Faker\Factory as Faker;
use App\Model\User;
use PDF;
use Storage;
use Response;
use DB;
use Carbon\Carbon;

class DataLaporanController extends Controller
{
	public function index(){
		$lap = LaporanAudit::with(['jadwalAudit', 'tanggapanAudit'])->where('status', '!=', "Tervalidasi")->get();
		return view('koor-bidang.data-laporan.index')->with([
			'lap'				=> $lap,
		]);
	}

	public function validasiLaporan($id){
		$laporan = LaporanAudit::find($id);
		$laporan->status = "Tervalidasi";
		$laporan->save();
		return back()->with('success', "Laporan telah tervalidasi");
	}

	public function validated(){
		$lap = LaporanAudit::with(['jadwalAudit', 'tanggapanAudit'])->where('status', "Tervalidasi")->get();
		return view('koor-bidang.data-laporan.validated')->with([
			'lap'				=> $lap,
		]);
	}

	public function searchTanggapan($id){
		$laporan = DB::table('laporanaudit')
		->join('tanggapanaudit', 'laporanaudit.id', '=', 'tanggapanaudit.id_laporanaudit')
		->select('tanggapanaudit.id as id_tanggapan', 'laporanaudit.kode_dokumen as kode_dokumen', 'tanggapanaudit.tanggapan_auditor as tanggapan_auditor')
		->where('laporanaudit.id', $id)
		->first();

		$data['id_tanggapan'] = $laporan->id_tanggapan;
		$data['kode_dokumen'] = $laporan->kode_dokumen;
		$data['tanggapan_auditor'] = $laporan->tanggapan_auditor;

		return response()->json($data);
	}

	public function viewdocument($id){
		$laporan = LaporanAudit::find($id);
		return view('koor-bidang.data-laporan.viewdocument')->with([
			'id'		=> $id,
			'laporan'	=> $laporan,
		]);
	}

	public function postEdit(Request $req){
		$this->validate($req, [
			'id_tanggapan'		=> 'required',
			'tanggapanauditor'	=> 'required',
		]);

		$lap = TanggapanAudit::find($req->id_tanggapan);

		$lap->tanggapan_auditor	= $req->tanggapanauditor;
		if ($lap->save()) {
			return back()->with('success', "Berhasil mengubah data Tanggapan Auditor");
		}
		else{
			return back()->with('danger', "Gagal mengubah data Tanggapan Auditor");
		}

	}

	public function streamdocument($id){
		$laporan	= LaporanAudit::where('id', $id)->first();
		$nama_ketua = '';
		$nip_ketua = '';
		foreach($laporan->jadwalAudit->auditorAudit as $dataAuditor){
			if($dataAuditor->id_jabatan_audit == 1){
				$nama_ketua = $dataAuditor->auditor->user->nama;
				$nip_ketua = $dataAuditor->auditor->nip;
			}
		}
		$pdf = PDF::loadView('dokumen.temuan', compact(['laporan', 'nama_ketua', 'nip_ketua']))->setPaper('legal', 'potrait');
		$filename = Carbon::now();
		return $pdf->stream($filename.'.pdf');
	}


}
