<?php

namespace App\Http\Controllers\KetuaSpi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\LaporanAudit;
use App\Model\Auditee;
use App\Model\TipeAudit;
use App\Model\JadwalAudit;
use App\Model\TanggapanAudit;
use Faker\Factory as Faker;
use App\Model\User;
use PDF;
use Storage;
use Response;
use DB;
use Carbon\Carbon;

class DataLaporanController extends Controller
{
	public function index(){
		$jad = LaporanAudit::with(['jadwalAudit', 'tanggapanAudit'])->where('status', '!=', "Tervalidasi")->get();
		return view('ketua-spi.data-laporan.index')->with([
			'jad'				=> $jad,
		]);
	}

	public function validasiLaporan($id){
		$laporan = LaporanAudit::find($id);
		$laporan->status = "Tervalidasi";
		$laporan->save();
		return back()->with('success', "Laporan telah tervalidasi");
	}

	public function validated(){
		$jad = LaporanAudit::with(['jadwalAudit', 'tanggapanAudit'])->where('status', "Tervalidasi")->get();
		return view('ketua-spi.data-laporan.validated')->with([
			'jad'				=> $jad,
		]);
	}

	public function viewdocument($id){
		$laporan = LaporanAudit::find($id);
		return view('ketua-spi.data-laporan.viewdocument')->with([
			'id'		=> $id,
			'laporan'	=> $laporan,
		]);
	}

	public function streamdocument($id){
		$laporan	= LaporanAudit::where('id', $id)->first();
		$nama_ketua = '';
		$nip_ketua = '';
		foreach($laporan->jadwalAudit->auditorAudit as $dataAuditor){
			if($dataAuditor->id_jabatan_audit == 1){
				$nama_ketua = $dataAuditor->auditor->user->nama;
				$nip_ketua = $dataAuditor->auditor->nip;
			}
		}
		$pdf = PDF::loadView('dokumen.temuan', compact(['laporan', 'nama_ketua', 'nip_ketua']))->setPaper('legal', 'potrait');
		$filename = Carbon::now();
		return $pdf->stream($filename.'.pdf');
	}

}
