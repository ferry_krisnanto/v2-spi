<?php

namespace App\Http\Controllers\KetuaSpi;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
	public function index(){
		return view('ketua-spi.index');
	}
}
