<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WordTestController extends Controller
{
    public function createWordDocx()
    {
      $wordTest = new \PhpOffice\PhpWord\PhpWord();

      $newSection = $wordTest->addSection();
      //Jangan diapus om...punya ryudav ini
      $desc1 = "Shakespeare's stories can sometimes be very difficult to understand. Have you tried clicking on the 'CC'
      button on the video so that you can see the words and watch the story at the same time? You can also click on the
      'Print the story' button under the video too and see the words. ";

      $desc2 = "The first game under the video helps you to understand who all the different characters are in the story.
      Some of them have very difficult names! You could also try printing out the activity sheet to help you with some of
      the other words in the story. Maybe a teacher or parent or friend can help you to understand better?  ";

      $desc3 = "There are lots of other songs and stories and games on our site I hope you will find something else that
      you enjoy. The important thing is to have fun while you learn and don't worry! If you keep practising, soon you will
      be able to understand everything! ";

      $newSection->addText($desc1);
      $newSection->addText($desc2);
      $newSection->addText($desc3, array('name' => 'Tahoma', 'size' =>15));

      $objectWriter = \PhpOffice\PhpWord\IOFactory::createWriter($wordTest, 'HTML');
      try {
      $objectWriter->save(storage_path('TestWordFile.html'));
      }catch(Exception $e)
      {}
      return response()->download(storage_path('TestWordFile.html'));
      }

}
