<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',      			 	'GuestController@index');
Route::get('/jadwal-audit/{id}', 	'GuestController@detailJadwal');
Route::get('/cetak-jadwal/{tahun}', 'GuestController@cetakJadwal')->name('cetakJadwal');
Route::post('/search',      	 	'GuestController@search')->name('searchTahun');

// Auth::routes();

// Authentication Routes...
Route::get('login',      'Auth\LoginController@showLoginForm')->name('login');
Route::get('login-admin','Auth\LoginController@showLoginAdmin')->name('login-admin');
Route::post('login',     'Auth\LoginController@login');
Route::post('logout',    'Auth\LoginController@logout')->name('logout');

// Registration Routes...
// Route::get('register',  'User\RegistrasiController@getDaftar')->name('register');
// Route::post('register', 'User\RegistrasiController@postDaftar')->name('daftar');

// Password Reset Routes...
// Route::get('password/reset',            'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
// Route::post('password/email',           'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
// Route::get('password/reset/{token}',    'Auth\ResetPasswordController@showResetForm')->name('password.reset');
// Route::post('password/reset',           'Auth\ResetPasswordController@reset');

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'ketuaspi', 'prefix' => 'ketuaspi'], function(){
	// DAVID ABROR
	Route::get('/', 'KetuaSpi\HomeController@index')->name('ketuaspi');

	Route::prefix('data-laporan')->group(function () {
        Route::get('/',             	    		'KetuaSpi\DataLaporanController@index')->name('ketuaspi.data-laporan.index');
        Route::get('/validated',          		'KetuaSpi\DataLaporanController@validated')->name('ketuaspi.data-laporan.validated');
        Route::get('/validate/{id}',    			'KetuaSpi\DataLaporanController@getValidate')->name('ketuaspi.data-laporan.validate');
        Route::post('/validate/post',   			'KetuaSpi\DataLaporanController@postValidate')->name('ketuaspi.data-laporan.validate.post');
		Route::get('/viewdocument/{id}',	    'KetuaSpi\DataLaporanController@viewdocument')->name('ketuaspi.data-laporan.viewdocument');
        Route::get('/ViewDocumentLHP/{id}',	  'KetuaSpi\DataLaporanController@streamdocument')->name('ketuaspi.data-laporan.streamdocument');
		Route::get('/validasi-laporan/{id}',	'KetuaSpi\DataLaporanController@validasiLaporan')->name('ketuaspi.data-laporan.validate');
    });
});

Route::group(['middleware' => 'koorbidang', 'prefix' => 'koorbidang'], function(){
	// DAVID ABROR
	Route::get('/', 'KoorBidang\HomeController@index')->name('koorbidang');

  Route::prefix('data-laporan')->group(function () {
        Route::get('/',             	    		'KoorBidang\DataLaporanController@index')->name('koorbidang.data-laporan.index');
        Route::get('/validated',          		'KoorBidang\DataLaporanController@validated')->name('koorbidang.data-laporan.validated');
        Route::get('/validate/{id}',    			'KoorBidang\DataLaporanController@getValidate')->name('koorbidang.data-laporan.validate');
        Route::post('/validate/post',   			'KoorBidang\DataLaporanController@postValidate')->name('koorbidang.data-laporan.validate.post');
        Route::get('/edit/{id}',    	    		'KoorBidang\DataLaporanController@getEdit')->name('koorbidang.jadwal-audit.edit');
        Route::post('/edit/post',   	    		'KoorBidang\DataLaporanController@postEdit')->name('koorbidang.jadwal-audit.edit.post');
        Route::get('/viewdocument/{id}',	    'KoorBidang\DataLaporanController@viewdocument')->name('koorbidang.data-laporan.viewdocument');
		Route::get('/search-tanggapan/{id}',	'KoorBidang\DataLaporanController@searchTanggapan')->name('koorbidang.data-laporan.search');
        Route::get('/ViewDocumentLHP/{id}',	  'KoorBidang\DataLaporanController@streamdocument')->name('koorbidang.data-laporan.streamdocument');
		Route::get('/validasi-laporan/{id}',	'KoorBidang\DataLaporanController@validasiLaporan')->name('koorbidang.data-laporan.validate');
    });
});

Route::group(['middleware' => 'operator', 'prefix' => 'operator'], function(){
	// FAIZ AZMI REKATAMA
	Route::get('/', 'Operator\HomeController@index')->name('operator');

	Route::prefix('jadwal-audit')->group(function () {
        Route::get('/pertahun',       		'Operator\JadwalAuditController@pertahun')->name('operator.jadwal-audit.pertahun');
		Route::get('/semua',              	'Operator\JadwalAuditController@semua')->name('operator.jadwal-audit.semua');
		Route::get('/tambah',       	    'Operator\JadwalAuditController@getTambah')->name('operator.jadwal-audit.tambah');
        Route::post('/tambah/post', 		'Operator\JadwalAuditController@postTambah')->name('operator.jadwal-audit.tambah.post');
        Route::get('/edit/{id}',    		'Operator\JadwalAuditController@getEdit')->name('operator.jadwal-audit.edit');
        Route::post('/edit/post',   		'Operator\JadwalAuditController@postEdit')->name('operator.jadwal-audit.edit.post');
        Route::get('/hapus/{id}',   		'Operator\JadwalAuditController@hapus')->name('operator.jadwal-audit.hapus');
		Route::get('/download/{tahun}',		'Operator\JadwalAuditController@download')->name('operator.jadwal-audit.download');
		Route::get('/search-auditor/{id}',	'Operator\JadwalAuditController@searchAuditor')->name('operator.jadwal-audit.search-auditor');
		Route::post('/search',      	 	'Operator\JadwalAuditController@search')->name('operator.jadwal-audit.search');
    });

	Route::prefix('auditi')->group(function () {
        Route::get('/',             'Operator\AuditiController@index')->name('operator.auditi.index');
        Route::post('/tambah/post', 'Operator\AuditiController@postTambah')->name('operator.auditi.tambah.post');
        Route::post('/edit/post',   'Operator\AuditiController@postEdit')->name('operator.auditi.edit.post');
        Route::get('/hapus/{id}',   'Operator\AuditiController@hapus')->name('operator.auditi.hapus');
		Route::get('/search/{id}',   'Operator\AuditiController@search')->name('operator.auditi.edit.search');
    });

	Route::prefix('auditor')->group(function () {
        Route::get('/',             'Operator\AuditorController@index')->name('operator.auditor.index');
        Route::post('/tambah/post', 'Operator\AuditorController@postTambah')->name('operator.auditor.tambah.post');
        Route::post('/edit/post',   'Operator\AuditorController@postEdit')->name('operator.auditor.edit.post');
        Route::get('/hapus/{id}',   'Operator\AuditorController@hapus')->name('operator.auditor.hapus');
		Route::get('/jurusan/{id}', 'Operator\AuditorController@getJurusan')->name('operator.auditor.jurusan');
		Route::get('/search/{id}',   'Operator\AuditorController@search')->name('operator.auditor.edit.search');
    });

	// Route::prefix('jenis-audit')->group(function () {
	// 	Route::get('/',             'Operator\JenisAuditController@index')->name('operator.jenis-audit.index');
	// 	Route::post('/tambah/post', 'Operator\JenisAuditController@postTambah')->name('operator.jenis-audit.tambah.post');
	// 	Route::post('/edit/post',   'Operator\JenisAuditController@postEdit')->name('operator.jenis-audit.edit.post');
	// 	Route::get('/hapus/{id}',   'Operator\JenisAuditController@hapus')->name('operator.jenis-audit.hapus');
	// });

	Route::prefix('aspek-audit')->group(function () {
		Route::get('/',             'Operator\AspekAuditController@index')->name('operator.aspek-audit.index');
		Route::post('/tambah/post', 'Operator\AspekAuditController@postTambah')->name('operator.aspek-audit.tambah.post');
		Route::post('/edit/post',   'Operator\AspekAuditController@postEdit')->name('operator.aspek-audit.edit.post');
		Route::get('/hapus/{id}',   'Operator\AspekAuditController@hapus')->name('operator.aspek-audit.hapus');
		Route::get('/search/{id}',  'Operator\AspekAuditController@search')->name('operator.aspek-audit.search');
	});

	Route::prefix('golongan')->group(function () {
		Route::get('/',             'Operator\GolonganController@index')->name('operator.golongan.index');
		Route::post('/tambah/post', 'Operator\GolonganController@postTambah')->name('operator.golongan.tambah.post');
		Route::post('/edit/post',   'Operator\GolonganController@postEdit')->name('operator.golongan.edit.post');
		Route::get('/hapus/{id}',   'Operator\GolonganController@hapus')->name('operator.golongan.hapus');
		Route::get('/search/{id}',  'Operator\GolonganController@search')->name('operator.golongan.search');
	});

	Route::prefix('fakultas')->group(function () {
		Route::get('/',             'Operator\FakultasController@index')->name('operator.fakultas.index');
		Route::post('/tambah/post', 'Operator\FakultasController@postTambah')->name('operator.fakultas.tambah.post');
		Route::post('/edit/post',   'Operator\FakultasController@postEdit')->name('operator.fakultas.edit.post');
		Route::get('/hapus/{id}',   'Operator\FakultasController@hapus')->name('operator.fakultas.hapus');
		Route::get('/search/{id}',   'Operator\FakultasController@search')->name('operator.fakultas.search');

		Route::prefix('jurusan')->group(function () {
			Route::post('/tambah/post', 'Operator\JurusanController@postTambah')->name('operator.jurusan.tambah.post');
			Route::post('/edit/post',   'Operator\JurusanController@postEdit')->name('operator.jurusan.edit.post');
			Route::get('/hapus/{id}',   'Operator\JurusanController@hapus')->name('operator.jurusan.hapus');
			Route::get('/search/{id}',   'Operator\JurusanController@search')->name('operator.jurusan.search');
		});
	});

	Route::prefix('golongan')->group(function () {
		Route::get('/',             'Operator\GolonganController@index')->name('operator.golongan.index');
		Route::post('/tambah/post', 'Operator\GolonganController@postTambah')->name('operator.golongan.tambah.post');
		Route::post('/edit/post',   'Operator\GolonganController@postEdit')->name('operator.golongan.edit.post');
		Route::get('/hapus/{id}',   'Operator\GolonganController@hapus')->name('operator.golongan.hapus');
		Route::get('/search/{id}',   'Operator\GolonganController@search')->name('operator.golongan.search');
	});

	Route::prefix('data-user')->group(function () {
		Route::get('/',             			'Operator\DataUserController@index')->name('operator.data-user.index');
		Route::post('/tambah/post', 			'Operator\DataUserController@postTambah')->name('operator.data-user.tambah.post');
		Route::post('/edit/post',   			'Operator\DataUserController@postEdit')->name('operator.data-user.edit.post');
		Route::post('/reset-password/post',   	'Operator\DataUserController@resetPassword')->name('operator.data-user.reset.password');
		Route::get('/search/{id}',  			'Operator\DataUserController@search')->name('operator.data-user.search');
		Route::get('/hapus/{id}',   			'Operator\DataUserController@hapus')->name('operator.data-user.hapus');
	});

	// Route::prefix('surat-tugas')->group(function () {
	// 	Route::get('/',             	'Operator\SuratTugasController@index')->name('operator.surat-tugas.index');
	// 	Route::post('/kirim/post', 		'Operator\SuratTugasController@postTambah')->name('operator.surat-tugas.tambah.post');
	// 	Route::get('/hapus/{id}',   	'Operator\SuratTugasController@hapus')->name('operator.surat-tugas.hapus');
	// 	Route::get('/download/{id}',   	'Operator\SuratTugasController@hapus')->name('operator.surat-tugas.download');
	// });
});

Route::group(['middleware' => 'auditor', 'prefix' => 'auditor'], function(){
	// Ichwan Almaza
	Route::get('/', 'Auditor\HomeController@index')->name('auditor');

	Route::prefix('surat-tugas')->group(function () {
		Route::get('/',             	     'Auditor\SuratTugasController@index')->name('auditor.surat-tugas.index');
		Route::get('/view/{id}',           'Auditor\SuratTugasController@view')->name('auditor.surat-tugas.view');
	});

	Route::prefix('data-lhp')->group(function () {
		Route::get('/',             	     'Auditor\DataLHPController@index')->name('auditor.data-lhp.index');
    	Route::get('/add',             	  	 'Auditor\DataLHPController@indexTambah')->name('auditor.data-lhp.tambah');
    	Route::get('/search/{id}',   		 'Auditor\LhpController@search')->name('auditor.data-lhp.edit.search');
    	Route::post('/edit/post/tanggapan',  'Auditor\LhpController@postEdit')->name('auditor.data-lhp.edit.post.tanggapan');
		Route::post('/kirim/post', 	    	 'Auditor\DataLHPController@postTambah')->name('auditor.data-lhp.tambah.post');
		Route::get('/edit/{id}',   	     	 'Auditor\DataLHPController@edit')->name('auditor.data-lhp.edit');
		Route::post('/edit/post',   	     'Auditor\DataLHPController@editPost')->name('auditor.data-lhp.edit.post');
		Route::get('/hapus/{id}',   	     'Auditor\DataLHPController@hapus')->name('auditor.data-lhp.hapus');
		//Route::get('/download/{id}',   	   'Auditor\DataLHPController@hapus')->name('auditor.data-lhp.download');
	});

});

Route::group(['middleware' => 'audity', 'prefix' => 'audity'], function(){
	//Deddy Pratama
	Route::get('/', 'Audity\HomeController@index')->name('audity');
	Route::get('/surat_tugas', 'Audity\SuratTugasController@index')->name('surat_tugas');
	Route::get('/jadwal_audit', 'Audity\JadwalAuditController@index')->name('jadwal_audit');
	Route::get('/lhp', 'Audity\LHPController@index')->name('LHP');
	Route::post('/jadwal_audit/detail','Audity\JadwalAuditController@detail')->name('detjadwalaudit');
	Route::post('/lhp/tanggap','Audity\LHPController@tanggap')->name('tanggap');
});

Route::group(['middleware' => 'rektor', 'prefix' => 'rektor'], function(){
	// ANLEY WIVER
	Route::get('/', 'Rektor\HomeController@index')->name('rektor');

  Route::prefix('laporan-hasil-pemeriksaan')->group(function () {
        Route::get('/',             	    'Rektor\LhpController@index')->name('rektor.lhp.index');
        Route::post('/edit/post',   'Rektor\LhpController@postEdit')->name('rektor.lhp.edit.post');
        Route::get('/search/{id}',   'Rektor\LhpController@search')->name('rektor.lhp.edit.search');
        Route::get('/hapus/{id}',   'Rektor\LhpController@hapus')->name('rektor.lhp.hapus');
    });
});
