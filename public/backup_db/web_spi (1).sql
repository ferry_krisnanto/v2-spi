-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 15, 2017 at 05:33 AM
-- Server version: 10.1.22-MariaDB
-- PHP Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `web_spi`
--

-- --------------------------------------------------------

--
-- Table structure for table `aspek_audit`
--

CREATE TABLE `aspek_audit` (
  `id` int(10) NOT NULL,
  `nama_aspek` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aspek_audit`
--

INSERT INTO `aspek_audit` (`id`, `nama_aspek`, `created_at`, `updated_at`) VALUES
(1, 'OTK', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Keuangan', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'SDM', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Hukum', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'BNN', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `auditee`
--

CREATE TABLE `auditee` (
  `id` int(10) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `unit_kerja` varchar(100) NOT NULL,
  `pimpinan_kerja` varchar(100) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `masa_kerja` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `token` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auditee`
--

INSERT INTO `auditee` (`id`, `id_user`, `unit_kerja`, `pimpinan_kerja`, `nip`, `masa_kerja`, `keterangan`, `token`, `created_at`, `updated_at`) VALUES
(1, 9, 'FMIPA', 'Prof. Dr. Warsito', '123123123123', '4 tahun', 'Fakultas Matematika dan Pengetahuan Alam', '20388', '2017-11-14 12:05:37', '2017-11-14 13:18:26');

-- --------------------------------------------------------

--
-- Table structure for table `auditor`
--

CREATE TABLE `auditor` (
  `id` int(10) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `id_jurusan` int(10) NOT NULL,
  `nip` varchar(100) NOT NULL,
  `id_golongan` int(10) NOT NULL,
  `token` varchar(20) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auditor`
--

INSERT INTO `auditor` (`id`, `id_user`, `id_jurusan`, `nip`, `id_golongan`, `token`, `created_at`, `updated_at`) VALUES
(1, 13, 1, '1417051050', 5, '13703', '2017-11-14 14:58:12', '2017-11-14 15:29:00'),
(2, 14, 1, '1417051032', 8, '88052', '2017-11-14 17:29:24', '2017-11-14 17:29:24'),
(3, 15, 1, '1417051000', 3, '31960', '2017-11-14 17:29:38', '2017-11-14 17:29:38');

-- --------------------------------------------------------

--
-- Table structure for table `daftar_auditee`
--

CREATE TABLE `daftar_auditee` (
  `id` int(10) NOT NULL,
  `nama_auditee` varchar(100) NOT NULL,
  `keterangan_auditee` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `fakultas`
--

CREATE TABLE `fakultas` (
  `id` int(10) NOT NULL,
  `nama_fakultas` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fakultas`
--

INSERT INTO `fakultas` (`id`, `nama_fakultas`, `created_at`, `update_at`) VALUES
(1, 'FMIPA', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id` int(10) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `golongan`
--

INSERT INTO `golongan` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(2, 'III/a', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(3, 'III/b', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(4, 'III/c', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(5, 'III/d', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(6, 'IV/a', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(7, 'IV/b', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(8, 'IV/c', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(9, 'IV/d', '2017-11-14 20:46:57', '2017-11-14 20:46:57'),
(10, 'IV/e', '2017-11-14 20:46:57', '2017-11-14 20:46:57');

-- --------------------------------------------------------

--
-- Table structure for table `jabatan_auditor`
--

CREATE TABLE `jabatan_auditor` (
  `id` int(10) NOT NULL,
  `nama` varchar(190) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan_auditor`
--

INSERT INTO `jabatan_auditor` (`id`, `nama`, `created_at`, `updated_at`) VALUES
(1, 'Ketua Audit', '2017-11-15 00:24:09', '2017-11-15 00:24:09'),
(2, 'Sekretaris', '2017-11-15 00:24:09', '2017-11-15 00:24:09'),
(3, 'Anggota Tim', '2017-11-15 00:24:09', '2017-11-15 00:24:09'),
(4, 'Anggota Penunjang', '2017-11-15 00:24:09', '2017-11-15 00:24:09'),
(5, 'Staff Audit', '2017-11-15 00:24:09', '2017-11-15 00:24:09');

-- --------------------------------------------------------

--
-- Table structure for table `jadwal_audit`
--

CREATE TABLE `jadwal_audit` (
  `id` int(10) NOT NULL,
  `id_auditee` int(10) NOT NULL,
  `id_tipeaudit` int(10) NOT NULL,
  `nomorsurat` varchar(100) NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `attachment_surattugas` varchar(100) NOT NULL,
  `keterangan` varchar(100) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jadwal_audit`
--

INSERT INTO `jadwal_audit` (`id`, `id_auditee`, `id_tipeaudit`, `nomorsurat`, `tanggal_mulai`, `tanggal_selesai`, `attachment_surattugas`, `keterangan`, `created_at`, `updated_at`) VALUES
(5, 1, 2, '1821/ASDS/122/12', '2017-11-15', '2017-11-16', '608-Belakang.jpg', 'Kerja Nyata', '2017-11-14 19:06:50', '2017-11-14 19:06:50');

-- --------------------------------------------------------

--
-- Table structure for table `jenisaudit`
--

CREATE TABLE `jenisaudit` (
  `id` int(10) NOT NULL,
  `id_tipeAudit` int(10) NOT NULL,
  `dokumenAudit` varchar(100) NOT NULL,
  `keterangan` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id` int(10) NOT NULL,
  `id_fakultas` int(10) NOT NULL,
  `nama_jurusan` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id`, `id_fakultas`, `nama_jurusan`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ilmu Komputer', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `laporanaudit`
--

CREATE TABLE `laporanaudit` (
  `id` int(10) NOT NULL,
  `id_jadwalAudit` int(10) NOT NULL,
  `nomor_urut` varchar(100) NOT NULL,
  `kode_dokumen` varchar(100) NOT NULL,
  `kode_temuan` varchar(100) NOT NULL,
  `deskripsi_temuan` varchar(100) NOT NULL,
  `kriteria` varchar(100) NOT NULL,
  `akibat` varchar(100) NOT NULL,
  `sebab` varchar(100) NOT NULL,
  `kesimpulan` text NOT NULL,
  `rekomendasi` text NOT NULL,
  `rencana_perbaikan` varchar(100) NOT NULL,
  `tindak_lanjut` varchar(100) NOT NULL,
  `jadwal_penyelesaian` date NOT NULL,
  `status` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2017_10_04_075941_create_roles_table', 1),
(2, '2017_10_04_075942_create_users_table', 1),
(3, '2017_10_04_075943_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `p_auditoraudit`
--

CREATE TABLE `p_auditoraudit` (
  `id` int(10) NOT NULL,
  `id_jadwalAudit` int(10) NOT NULL,
  `id_auditor` int(10) NOT NULL,
  `id_jabatan_audit` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_auditoraudit`
--

INSERT INTO `p_auditoraudit` (`id`, `id_jadwalAudit`, `id_auditor`, `id_jabatan_audit`, `created_at`, `updated_at`) VALUES
(10, 5, 2, 1, '2017-11-14 19:06:53', '2017-11-14 19:06:53'),
(11, 5, 3, 2, '2017-11-14 19:06:53', '2017-11-14 19:06:53'),
(12, 5, 1, 3, '2017-11-14 19:06:53', '2017-11-14 19:06:53'),
(13, 5, 2, 3, '2017-11-14 19:06:53', '2017-11-14 19:06:53'),
(14, 5, 3, 4, '2017-11-14 19:06:53', '2017-11-14 19:06:53'),
(15, 5, 2, 5, '2017-11-14 19:06:53', '2017-11-14 19:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `p_jadwalaspekaudit`
--

CREATE TABLE `p_jadwalaspekaudit` (
  `id` int(10) NOT NULL,
  `id_jadwalAudit` int(10) NOT NULL,
  `id_aspekaudit` int(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `p_jadwalaspekaudit`
--

INSERT INTO `p_jadwalaspekaudit` (`id`, `id_jadwalAudit`, `id_aspekaudit`, `created_at`, `updated_at`) VALUES
(10, 5, 2, '2017-11-14 19:06:52', '2017-11-14 19:06:52'),
(11, 5, 3, '2017-11-14 19:06:52', '2017-11-14 19:06:52'),
(12, 5, 4, '2017-11-14 19:06:53', '2017-11-14 19:06:53'),
(13, 5, 5, '2017-11-14 19:06:53', '2017-11-14 19:06:53');

-- --------------------------------------------------------

--
-- Table structure for table `p_userjadwal`
--

CREATE TABLE `p_userjadwal` (
  `id_jadwalAudit` int(10) NOT NULL,
  `id_user` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` tinyint(3) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deskripsi` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `nama`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ketua SPI', NULL, NULL, NULL, NULL),
(2, 'Operator', NULL, NULL, NULL, NULL),
(3, 'Auditor', NULL, NULL, NULL, NULL),
(4, 'Audity', NULL, NULL, NULL, NULL),
(5, 'Koordinator Bidang', NULL, NULL, NULL, NULL),
(6, 'Rektor', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tanggapanaudit`
--

CREATE TABLE `tanggapanaudit` (
  `id` int(10) NOT NULL,
  `id_laporanaudit` int(10) NOT NULL,
  `tanggapan_auditee` varchar(100) NOT NULL,
  `tanggapan_auditor` varchar(100) NOT NULL,
  `tanggapan_rektor` varchar(100) NOT NULL,
  `keterangan` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tipe_audit`
--

CREATE TABLE `tipe_audit` (
  `id` int(10) NOT NULL,
  `nama_tipe` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `update_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipe_audit`
--

INSERT INTO `tipe_audit` (`id`, `nama_tipe`, `created_at`, `update_at`) VALUES
(1, 'Reguler', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'PDTT', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `unit_kerja`
--

CREATE TABLE `unit_kerja` (
  `id` int(10) NOT NULL,
  `nama_unitkerja` varchar(100) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `nama` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_role` tinyint(3) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `nama`, `username`, `email`, `password`, `id_role`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ketua SPI', 'ketua_spi', 'bailey.vena@hartmann.biz', '$2y$10$xFVXUul0p925xI8MPar38uBBQKQ336OckAyd4wMAlSr4eosL4Zd52', 1, 'vUjDkit8AXApB6gziYi2fbm6GuCDiF0sBqSu3F8aGBJv49Uoskd4MsslNdER', '2017-10-05 09:49:35', '2017-11-14 08:57:46', NULL),
(2, 'Operator', 'operator', 'tamara.veum@yahoo.com', '$2y$10$.oqAGHUNtWGtnDvRWka5o.bvP8qWFed.WDnWQoRU/mLdCJe9vljue', 2, 'nPeyjLybiuBzk3CrGQ7ROldzht4uaHLbMueBh1SQ5kKFIhCHoCNxTHfoLVty', '2017-10-05 09:49:35', '2017-10-05 09:49:35', NULL),
(3, 'Auditor', 'auditor', 'pdicki@considine.com', '$2y$10$pWISeu7QeN3K1I.aMy5D5.s/f5PbwyJwTkzV9VNZJW4vEKPJovpa6', 3, 'aLw1nS0iEIWJT7LtanuJ0EzuJz6ZOPkD7zQXWbZvSlDoc07LN7qwe8zTJdG9', '2017-10-05 09:49:35', '2017-10-05 09:49:35', NULL),
(4, 'Audity', 'audity', 'arely70@yahoo.com', '$2y$10$KFmYsrPrWjoPbw/OprE9Kum75tEzN8XYs5xE9.ZBlShWi4CbeQeVO', 4, 'tICzxq56I3yLwiEARChGCv7iwsZZKgu2ckqN1USH3nOWvcCXw0MuietDX15P', '2017-10-05 09:49:36', '2017-10-05 09:49:36', NULL),
(5, 'Koordinator Bidang', 'koor_bidang', 'athena.hermiston@dubuque.com', '$2y$10$../WF6opZKO41NDRXNdYrO/iaCfE/Bhm1TN7RDu2EcwDLyL2MRdni', 5, NULL, '2017-10-05 09:49:36', '2017-10-05 09:49:36', NULL),
(6, 'Rektor', 'rektor', 'douglas32@gmail.com', '$2y$10$oyYWpGsKUuvJHFPn24MLiemwsNkHtZWZEam7wwGxv0GkOl9glujXi', 6, '73XM8R2saNGtsnNVnSgBr7HXLYBoiFx9AkKr37aOcuMHj8RxQqBmOlYtZJXE', '2017-10-05 09:49:36', '2017-10-05 09:49:36', NULL),
(9, 'FMIPA', '123123123123', 'dgreen@roberts.com', '$2y$10$7SO2ICJXSThjt1DL1Tkj6ujZ7IYvs4JjrTlg0uQ2.gx1PvQHizHGi', 4, 'Ss2E56pwSbKDuTXyGSzLpfXKYHm3NisxCQmZX6P6hzvYUWlchcieA7MERWB3', '2017-11-14 05:05:37', '2017-11-14 05:05:37', NULL),
(13, 'Faiz Azmi Rekatama', '1417051050', 'freichert@okeefe.com', '$2y$10$6V2oaF.VXdL2HN21GZrYKOWMjuwajeMQ2E3b1CWYoC6TDQfsuliqu', 3, NULL, '2017-11-14 07:58:12', '2017-11-14 08:29:00', NULL),
(14, 'David Abror', '1417051032', 'ethan.shields@howell.biz', '$2y$10$wl9mzV/jVyiHURUqMFUUlO1loFnL99EZHqgmNbt0412HK6H3fZNzC', 3, NULL, '2017-11-14 10:29:24', '2017-11-14 10:29:24', NULL),
(15, 'Ichwan Almaza', '1417051000', 'ymurphy@leannon.biz', '$2y$10$eSeaU2jWDk7jGE7GcKk00OglLslZJHtYOnMo5AGbduTZnA3HEV0ay', 3, NULL, '2017-11-14 10:29:38', '2017-11-14 10:29:38', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aspek_audit`
--
ALTER TABLE `aspek_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auditee`
--
ALTER TABLE `auditee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`);

--
-- Indexes for table `auditor`
--
ALTER TABLE `auditor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_user` (`id_user`,`id_jurusan`),
  ADD KEY `auditor_jurusan` (`id_jurusan`),
  ADD KEY `id_golongan` (`id_golongan`);

--
-- Indexes for table `daftar_auditee`
--
ALTER TABLE `daftar_auditee`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fakultas`
--
ALTER TABLE `fakultas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan_auditor`
--
ALTER TABLE `jabatan_auditor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jadwal_audit`
--
ALTER TABLE `jadwal_audit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_auditee` (`id_auditee`,`id_tipeaudit`),
  ADD KEY `jadwal_tipe` (`id_tipeaudit`);

--
-- Indexes for table `jenisaudit`
--
ALTER TABLE `jenisaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_tipeAudit` (`id_tipeAudit`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_fakultas` (`id_fakultas`);

--
-- Indexes for table `laporanaudit`
--
ALTER TABLE `laporanaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jadwalAudit` (`id_jadwalAudit`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `p_auditoraudit`
--
ALTER TABLE `p_auditoraudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jadwalAudit` (`id_jadwalAudit`,`id_auditor`),
  ADD KEY `AAAuditor` (`id_auditor`),
  ADD KEY `id_jabatan_audit` (`id_jabatan_audit`);

--
-- Indexes for table `p_jadwalaspekaudit`
--
ALTER TABLE `p_jadwalaspekaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_jadwalAudit` (`id_jadwalAudit`,`id_aspekaudit`),
  ADD KEY `aspek_aspek` (`id_aspekaudit`);

--
-- Indexes for table `p_userjadwal`
--
ALTER TABLE `p_userjadwal`
  ADD KEY `id_jadwalAudit` (`id_jadwalAudit`,`id_user`),
  ADD KEY `user_user` (`id_user`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tanggapanaudit`
--
ALTER TABLE `tanggapanaudit`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_laporanaudit` (`id_laporanaudit`);

--
-- Indexes for table `tipe_audit`
--
ALTER TABLE `tipe_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_username_unique` (`username`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_id_role_index` (`id_role`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aspek_audit`
--
ALTER TABLE `aspek_audit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `auditee`
--
ALTER TABLE `auditee`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `auditor`
--
ALTER TABLE `auditor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `daftar_auditee`
--
ALTER TABLE `daftar_auditee`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fakultas`
--
ALTER TABLE `fakultas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `jabatan_auditor`
--
ALTER TABLE `jabatan_auditor`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jadwal_audit`
--
ALTER TABLE `jadwal_audit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jenisaudit`
--
ALTER TABLE `jenisaudit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jurusan`
--
ALTER TABLE `jurusan`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `laporanaudit`
--
ALTER TABLE `laporanaudit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `p_auditoraudit`
--
ALTER TABLE `p_auditoraudit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `p_jadwalaspekaudit`
--
ALTER TABLE `p_jadwalaspekaudit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` tinyint(3) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tanggapanaudit`
--
ALTER TABLE `tanggapanaudit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tipe_audit`
--
ALTER TABLE `tipe_audit`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `unit_kerja`
--
ALTER TABLE `unit_kerja`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `auditee`
--
ALTER TABLE `auditee`
  ADD CONSTRAINT `auditee_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `auditor`
--
ALTER TABLE `auditor`
  ADD CONSTRAINT `auditor_jurusan` FOREIGN KEY (`id_jurusan`) REFERENCES `jurusan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `auditor_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `golongan` FOREIGN KEY (`id_golongan`) REFERENCES `golongan` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jadwal_audit`
--
ALTER TABLE `jadwal_audit`
  ADD CONSTRAINT `jadwal_auditee` FOREIGN KEY (`id_auditee`) REFERENCES `auditee` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jadwal_tipe` FOREIGN KEY (`id_tipeaudit`) REFERENCES `tipe_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jenisaudit`
--
ALTER TABLE `jenisaudit`
  ADD CONSTRAINT `jenis_tipe` FOREIGN KEY (`id_tipeAudit`) REFERENCES `tipe_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD CONSTRAINT `fak` FOREIGN KEY (`id_fakultas`) REFERENCES `fakultas` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `laporanaudit`
--
ALTER TABLE `laporanaudit`
  ADD CONSTRAINT `jadwal_lap` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_auditoraudit`
--
ALTER TABLE `p_auditoraudit`
  ADD CONSTRAINT `AAAuditor` FOREIGN KEY (`id_auditor`) REFERENCES `auditor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AAJabatan` FOREIGN KEY (`id_jabatan_audit`) REFERENCES `jabatan_auditor` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `AAJadwal` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_jadwalaspekaudit`
--
ALTER TABLE `p_jadwalaspekaudit`
  ADD CONSTRAINT `aspek_aspek` FOREIGN KEY (`id_aspekaudit`) REFERENCES `aspek_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `aspek_jadwal` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `p_userjadwal`
--
ALTER TABLE `p_userjadwal`
  ADD CONSTRAINT `user_jadwal` FOREIGN KEY (`id_jadwalAudit`) REFERENCES `jadwal_audit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tanggapanaudit`
--
ALTER TABLE `tanggapanaudit`
  ADD CONSTRAINT `lap_audit` FOREIGN KEY (`id_laporanaudit`) REFERENCES `laporanaudit` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_id_role_foreign` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
