<!doctype html>
<html lang="en">
@include('partials.htmlheader')
<body>
  <div class="wrapper wrapper-full-page">
    <div class="full-page login-page" data-color="blue" data-image="{{ asset('assets/img/full-screen-image-2.jpg') }}">

    {{-- you can change the color of the filter page using: data-color="blue | azure | green | orange | red | purple" --}}
        <div class="content">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3">
                        <form role="form" method="POST" action="{{ route('password.email') }}">
                          {{ csrf_field() }}
                            <div class="card card-hidden">
                                <div class="header text-center">Reset Password</div>
                                <div class="content">
                                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                        <label>Alamat Email</label>
                                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>
                                        <small>Gunakan email yang anda isi ketika mendaftar.</small>
                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="footer text-center">
                                    <button type="submit" class="btn btn-fill btn-warning btn-wd">Kirim Link Reset Password</button>
                                    <br>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    	<footer class="footer footer-transparent">
            <div class="container">
                <p class="copyright pull-right">
                    &copy;2017 <a href="#">SaN</a> Developed By: <a target="_blank" href="#">Capung Technology</a>.
                </p>
            </div>
        </footer>
    </div>
  </div>
</body>

<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->
<script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/jquery.mask.js') }}"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>
<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
@if ($message=Session::get('status'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@endif
<script type="text/javascript">
    $('.kode').mask('000-00-00000', {placeholder: "___-__-_____"});
    $().ready(function(){
        lbd.checkFullPageBackgroundImage();

        setTimeout(function(){
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700)
    });
</script>

</html>
