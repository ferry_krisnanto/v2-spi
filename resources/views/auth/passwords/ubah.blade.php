@extends('layouts.apps')
@section('nav_title')
  Ubah Password
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
              <form id="eventValidation" action="{{ route('password.ubah.post') }}" method="post">
                {{ csrf_field() }}
                  <div class="header">Ubah Password</div>
                  <div class="content">

                      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                          <label class="control-label">Password <star>*</star></label>
                          <input id="password" type="password" class="form-control" name="password" required>

                          @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                      </div>

                      <div class="form-group">
                          <label class="control-label">Ulangi Password <star>*</star></label>
                          <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                      </div>

                      <div class="category"><star>*</star> Harus diisi</div>
                  </div>

                  <div class="footer">
                      <button type="submit" class="btn btn-info btn-fill pull-right">Submit</button>
                      <div class="clearfix"></div>
                  </div>
              </form>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif
<script type="text/javascript">
  $().ready(function(){
      $('#eventValidation').validate();
      demo.initFormExtendedDatetimepickers();
  });
</script>
@endsection
