@extends('layouts.apps')
@section('nav_title')
Jadwal Audit
@endsection
@section('style')
<style media="screen">
.form-horizontal .control-label{
/* text-align:right; */
text-align:left;
}
</style>
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
		        <h5 class="title">Form Tambah Data LHP</h5>
		    </div>
            <div class="content">
				<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ route('auditor.data-lhp.tambah.post') }}">
					{{ csrf_field() }}
                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Nomor Surat Tugas</b><star>*</star></label>
                        <div class="col-md-6">
                            <select name="nomorsurat" class="selectpicker" data-title="--PILIH NOMOR SURAT--" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
              								@foreach($NomorSurat as $data)
              										<option value="{{ $data->id }}">{{ $data->nomorsurat }}</option>
              								@endforeach
              							</select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Kode Dokumen</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="kodedokumen" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Nomor Urut Temuan</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" valau="" maxlength="2" value="" onkeypress="return isNumberKey(event)" name="nouruttemuan" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Kode Temuan</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value=" " maxlength="3" onkeypress="return isNumberKey(event)" name="kodetemuan" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Deskripsi Temuan/Kondisi</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="deskripsitemuan" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Kriteria/Persyaratan</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="kriteria" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Akibat</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="akibat" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Sebab</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="sebab" class="form-control" required>
                        </div>
                    </div>

										<div class="form-group">
												<label class="col-md-3 control-label label"><b>Rencana Perbaikan</b><star>*</star></label>
												<div class="col-md-6">
														<input type="text" value="" name="rencana" class="form-control" required>
												</div>
										</div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Simpulan/Temuan</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="simpulan" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Rekomendasi</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="rekomendasi" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Tindak Lanjut</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="tindaklanjut" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Jadwal Penyelesaian</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="date" value="" name="jadwalpenyelesaian" class="form-control" required>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Tanggapan Auditor</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" value="" name="tanggapanauditor" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                      <label class="col-md-3"></label>
                      <div class="col-md-6">
          							<button type="submit" class="btn btn-fill btn-info btn-flat">SIMPAN</button>
          							<a href="{{ route('auditor.data-lhp.index') }}" class="btn btn-fill btn-danger btn-flat">BATAL</a>
                      </div>
                    </div>
                </form>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif
@endsection
<script>
    function isNumberKey(evt){
        var charCode=(evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false; 
    return true;
    }
</script>
