@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Data Auditor
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
        <a href="{{ route('auditor.data-lhp.tambah') }}" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Tambah</a>
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Dokumen</th>
                                <th>Tipe Audit</th>
								                <th>Tanggal Mulai Audit</th>
                								<th>Tanggal Selesai Au
                								<th>Tanggal Entri</th>
                                <th>Status</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          @foreach($LaporanAudit as $data)
							              <tr>
                                <td>{{$no}}</td>
                                <td>{{$data->kode_dokumen}}</td>
                                <td>{{$data->nama_tipe}}</td>
                								<td>{{$data->tanggal_mulai}}</td>
                								<td>{{$data->tanggal_selesai}}</td>
                								<td>{{$data->created_at}}</td>
                                <td>{{$data->status}}</td>
                                <td class="text-right">
                                    <a href="{{ route('auditor.data-lhp.edit', $data->id) }}" class="btn btn-simple btn-warning btn-icon edit"><i class="fa fa-edit"></i></a>

                                     <!-- <button data-toggle="modal" value="{{ $data->id }}" data-target="#editTanggapanLHP" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-pencil"></i></button> -->

                                     <button data-toggle="modal" value="{{ $data->id }}" data-target="#editTanggapanLHP" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-pencil"></i></button>    

                                    <a href="{{ route('auditor.data-lhp.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            <?php $no++; ?>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Edit Rektor -->
                <div class="modal fade" id="editTanggapanLHP" tabindex="-1" role="dialog" aria-labelledby="editTanggapanLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Form Edit Tanggapan LHP</h4>
                      </div>
                      <div class="modal-body">
                        <form action="{{route('auditor.data-lhp.edit.post.tanggapan')}}" method="post">
                          {{ csrf_field() }}

                              <label>Id Tanggapan</label>
                          <input type="text" name="idnya" id="idnya" value="{{ old('id') }}" class="form-control" required="true" readonly="">
                          <div class="form-group">
                              <label>Tanggapan Auditor</label>
                              <input name="tanggapan_auditor" id="auditornya" value="{{ old('tanggapan_auditor') }}" class="form-control" required="true"></input>
                          </div>
                      </div>
                      <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
<!-- Modal Edit Rektor -->

<!-- <div class="modal fade" id="editTanggapanLHP" tabindex="-1" role="dialog" aria-labelledby="editTanggapanLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Form Edit Tanggapan LHP</h4>
                      </div>
                      <div class="modal-body">
                        <form action="{{route('auditor.data-lhp.edit.post')}}" method="post">
                          {{csrf_field()}}
                          <input type="text" name="idnya" id="idnya" value="{{ old('id') }}">
                          <div class="form-group">
                              <label>Tanggapan Auditor</label>
                              <input name="tanggapan_auditor" id="auditornya" value="{{ old('tanggapan_auditor') }}" class="form-control" required="true"></input>
                          </div>
                      </div>
                      <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div> -->
<!-- Modal Tambah Auditee -->
<div class="modal fade" id="tambahAuditor" tabindex="-1" role="dialog" aria-labelledby="tambahAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Data LHP</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label>...</label>
              <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" required="true">
          </div>
          <div class="form-group">
              <label>...</label>
              <input type="text" name="nip" value="{{ old('nip') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>...</label>
              <input type="text" name="jabatan" value="{{ old('jabatan') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Resource",
        },

    });
    var table = $('#datatables').DataTable();

    $('.edit').click(function(){
      var userId = +$(this).val();
        $.get('{{ URL::to("auditor/data-lhp/search") }}/' + userId, function (data) {
        $('#idnya').val(data.id);
        $('#kodedokumennya').val(data.kode_dokumen);
        $('#auditornya').val(data.tanggapan_auditor);
        });

      event.stopPropagation();
      $('#editTanggapanLHP').modal('show');
    });
});

</script>
@endsection
