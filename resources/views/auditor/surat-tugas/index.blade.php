@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Data Auditor
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nomor Surat</th>
                                <th>Unit Kerja</th>
                				<th>Tanggal Diterima</th>
                                <th>Tanggal Mulai Audit</th>
                                <th>Tanggal Selesai Audit</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          @foreach($SuratTugas as $ST)
                             <tr>
                                <td>{{$no}}</td>
                                <td>{{$ST->nomorsurat}}</td>
                                <td>{{$ST->unit_kerja}}</td>
                                <td><?php echo date("d F Y", strtotime($ST->created_at)); ?></td>
								<td><?php echo date("d F Y", strtotime($ST->tanggal_mulai)); ?></td>
							    <td><?php echo date("d F Y", strtotime($ST->tanggal_selesai)); ?></td>
                                <td class="text-right">
									                 
                                    <a target="_blank" href="{{ asset('/../storage/app/public/surat-tugas/'. $ST->attachment_surattugas )}}" class="btn btn-simple btn-info btn-icon" title="Unduh Surat"><i class="fa fa-download"></i></a>
                                </td>
                            </tr>
                            <?php $no++; ?>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search Resource",
        },

    });
    var table = $('#datatables').DataTable();
});

</script>
@endsection
