@extends('layouts.apps')
@section('nav_title')
View Laporan Hasil Pemeriksaan
@endsection
@section('content')
<div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
								<div class="toolbar">

	                            </div>
	                            <p align="center"> View Surat Tugas</p>
                              <div class="row">
                      					<div class="col-md-3">
                      						<form class="" action="#" method="post">
                      							{{ csrf_field() }}
                                    <a href="{{ route('auditor.surat-tugas.index') }}" class="btn btn-danger btn-sm" name="button"><i class="fa fa-arrow-left"></i></a> Back</button>
                      						</form>
                      					</div>
                      				</div>
                      				<hr>
                      				<div class="row">
                      					<div class="col-md-4">
                                		</div>
                      				</div>
                              <br>
                            </div><!-- end content-->
                            <!-- <iframe src='http://docs.google.com/gview?url={{ asset("laporan/laporanlhp.docx") }}&embedded=true'
                            style="width:100%; height:500px;"
                            frameborder="0"></iframe> -->
							
							
                            <iframe src="{{ asset('/../storage/app/public/surat-tugas/'.$pdf) }}" width="100%" height="1000" alt="pdf"></iframe>
							
							
						  </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>

    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

	<!--  Forms Validations Plugin -->
	<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="{{ asset('assets/js/moment.min.js') }}"></script>
	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="{{ asset('assets/js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>
	<!--  Charts Plugin -->
	<script src="{{ asset('assets/js/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>
    <!-- Sweet Alert 2 plugin -->
	<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
    <!-- Vector Map plugin -->
	<script src="{{ asset('assets/js/jquery-jvectormap.js') }}"></script>

	<!-- Wizard Plugin    -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
	<!--  Bootstrap Table Plugin    -->
	<script src="{{ asset('assets/js/bootstrap-table.js') }}"></script>
	<!--  Plugin for DataTables.net  -->
	<script src="{{ asset('assets/js/jquery.datatables.js') }}"></script>
    <!--  Full Calendar Plugin    -->
    <script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>
	<!--   Sharrre Library    -->
    <script src="{{ asset('assets/js/jquery.sharrre.js') }}"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/js/demo.js') }}"></script>
    <script type="text/javascript">
    $(document).ready(function() {
		$('#datatables').DataTable({
		    "pagingType": "full_numbers",
		    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		    responsive: true,
		    language: {
		    search: "_INPUT_",
		    searchPlaceholder: "Search records",
		    }

		});


		var table = $('#datatables').DataTable();

		// Edit record
		table.on( 'click', '.edit', function () {
		    $tr = $(this).closest('tr');

		    var data = table.row($tr).data();
		    alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
		} );

		// Delete a record
		table.on( 'click', '.remove', function (e) {
		    $tr = $(this).closest('tr');
		    table.row($tr).remove().draw();
		    e.preventDefault();
		} );
	});

    </script>
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif
@endsection
