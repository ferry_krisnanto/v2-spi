@extends('layouts.apps')
@section('nav_title')
Surat Tugas
@endsection
@section('content')
<div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
								<div class="toolbar">

	                            </div>
	                            <p align="center"> Data Audit</p>
                                <div class="fresh-datatables">
                					<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                						<thead>
                							<tr>
                							
                								<th>Kode Dokumen</th>
                								<th>Ketua Auditor</th>
                								<th>Tanggal Diterima</th>
                								<th>Tanggal Mulai Audit</th>
                								<th>Tanggal Selesai Audit</th>
                								<th class="disabled-sorting text-right">Surat Tugas</th>
                							</tr>
                						</thead>
                						<tbody>
                					
											@foreach($auditor as $j)
                							<tr>
                								
                								<td>{{$j->nomorsurat}}</td>
                								<td>{{$j->nama}}</td>
                								<td>{{$j->created_at}}</td>
                								<td>{{$j->tanggal_mulai}}</td>
                								<td>{{$j->tanggal_selesai}}</td>
                								<td class="text-right">
                									<a href="{{url('../storage/app/public/surat-tugas/'.$j->attachment_surattugas)}}" class="btn btn-simple btn-warning btn-icon edit"><i class="fa fa-download"></i></a>
                								</td>
                							</tr>
										   @endforeach
                						</tbody>
                					</table>
        				        </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>

    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

	<!--  Forms Validations Plugin -->
	<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="{{ asset('assets/js/moment.min.js') }}"></script>
	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="{{ asset('assets/js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>
	<!--  Charts Plugin -->
	<script src="{{ asset('assets/js/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>
    <!-- Sweet Alert 2 plugin -->
	<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
    <!-- Vector Map plugin -->
	<script src="{{ asset('assets/js/jquery-jvectormap.js') }}"></script>

	<!-- Wizard Plugin    -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
	<!--  Bootstrap Table Plugin    -->
	<script src="{{ asset('assets/js/bootstrap-table.js') }}"></script>
	<!--  Plugin for DataTables.net  -->
	<script src="{{ asset('assets/js/jquery.datatables.js') }}"></script>
    <!--  Full Calendar Plugin    -->
    <script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>
	<!--   Sharrre Library    -->
    <script src="{{ asset('assets/js/jquery.sharrre.js') }}"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/js/demo.js') }}"></script>


    <script type="text/javascript">
    $(document).ready(function() {
		$('#datatables').DataTable({
		    "pagingType": "full_numbers",
		    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		    responsive: true,
		    language: {
		    search: "_INPUT_",
		    searchPlaceholder: "Search records",
		    }

		});


		var table = $('#datatables').DataTable();

		// Edit record
		table.on( 'click', '.edit', function () {
		    $tr = $(this).closest('tr');

		    var data = table.row($tr).data();
		    alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
		} );

		// Delete a record
		table.on( 'click', '.remove', function (e) {
		    $tr = $(this).closest('tr');
		    table.row($tr).remove().draw();
		    e.preventDefault();
		} );

		//Like record
		table.on( 'click', '.like', function () {
		    alert('You clicked on Like button');
		});
	});

    </script>
@endsection

@section('script')

@endsection
