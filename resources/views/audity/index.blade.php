@extends('layouts.apps')
@section('nav_title')
Beranda {{Auth::user()->nama}}
@endsection
@section('content')
<img src="{{ asset('assets/img/banner/auditee.png') }}" style="height:300px;width:100%"class="img-responsive ">
<br><br>
<div class="content">
         
        </div>

    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

	<!--  Forms Validations Plugin -->
	<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="{{ asset('assets/js/moment.min.js') }}"></script>
	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="{{ asset('assets/js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>
	<!--  Charts Plugin -->
	<script src="{{ asset('assets/js/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>
    <!-- Sweet Alert 2 plugin -->
	<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
    <!-- Vector Map plugin -->
	<script src="{{ asset('assets/js/jquery-jvectormap.js') }}"></script>

	<!-- Wizard Plugin    -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
	<!--  Bootstrap Table Plugin    -->
	<script src="{{ asset('assets/js/bootstrap-table.js') }}"></script>
	<!--  Plugin for DataTables.net  -->
	<script src="{{ asset('assets/js/jquery.datatables.js') }}"></script>
    <!--  Full Calendar Plugin    -->
    <script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>
	<!--   Sharrre Library    -->
    <script src="{{ asset('assets/js/jquery.sharrre.js') }}"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/js/demo.js') }}"></script>


    <script type="text/javascript">
    $(document).ready(function() {
		$('#datatables').DataTable({
		    "pagingType": "full_numbers",
		    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		    responsive: true,
		    language: {
		    search: "_INPUT_",
		    searchPlaceholder: "Search records",
		    }

		});


		var table = $('#datatables').DataTable();

		// Edit record
		table.on( 'click', '.edit', function () {
		    $tr = $(this).closest('tr');

		    var data = table.row($tr).data();
		    alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
		} );

		// Delete a record
		table.on( 'click', '.remove', function (e) {
		    $tr = $(this).closest('tr');
		    table.row($tr).remove().draw();
		    e.preventDefault();
		} );

		//Like record
		table.on( 'click', '.like', function () {
		    alert('You clicked on Like button');
		});
	});

    </script>
@endsection

@section('script')
@include('partials.scripts')
<script type="text/javascript">
	$calendar = $('#kalenderku');

	today = new Date();
	y = today.getFullYear();
	m = today.getMonth();
	d = today.getDate();

	$calendar.fullCalendar({
		header: {
			left: 'title',
			center: 'month,agendaWeek,agendaDay',
			right: 'prev,next today'
		},
		defaultDate: today,
		selectable: true,
		selectHelper: true,
		titleFormat: {
			month: 'MMMM YYYY', // September 2015
			week: "MMMM D YYYY", // September 2015
			day: 'D MMM, YYYY'  // Tuesday, Sep 8, 2015
		},

		editable: false,
		eventLimit: true, // allow "more" link when too many events


		// color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
		events: [
			{
				title: 'Audit MIPA',
				start: new Date(2017, 10, 1),
				end: new Date(2017, 10, 1)
			},
			{
				title: 'Audit Google',
				start: new Date(y, m, 21),
				end: new Date(y, m, 23),
				url: 'http://www.capung.tech/',
				className: 'event-orange'
			}
		]
	});
</script>

@endsection
