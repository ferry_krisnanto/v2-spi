@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Data LHP
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">
     			<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Dokumen</th>
                                <th>Tipe Audit</th>
								                <th>Tanggal Mulai Audit</th>
                								<th>Tanggal Selesai Audit</th>
                								<th>Status</th>
                                <th class="disabled-sorting text-right">Tanggapi</th>
                            </tr>
                        </thead>
                        <tbody>
                          <?php $no=1; ?>
                          @foreach($LaporanAudit as $data)
							              <tr>
                                <td>{{$no}}</td>
                                <td>{{$data->kode_dokumen}}</td>
                                <td>{{$data->nama_tipe}}</td>
                								<td>{{$data->tanggal_mulai}}</td>
                								<td>{{$data->tanggal_selesai}}</td>
                								<td>{{$data->status}}</td>
                                <td class="text-right">
                                <a class="edit btn btn-simple btn-warning btn-icon" data-info="{{$data->id}}" href="#"><i class="fa fa-edit"></i></a>
                                   </td>
                            </tr>
                            <?php $no++; ?>
                          @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <form class="form-horizontal" role="form">
        {{csrf_field()}}
          <div class="form-group">
            <input type="hidden" id="token" value="{{ csrf_token() }}">
            <label class="control-label col-sm-2" for="id">NO</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="fid" disabled>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-2" for="fname">Tanggapan</label>
            <div class="col-sm-10">
              <textarea type="text" value="" id="fname" name="tanggapanLHP" class="form-control" required></textarea>
            </div>
          </div>
        </form>
        <div class="deleteContent">
          Apakah Anda Yakin Ingin menghapus Data, dengan nama
          <span class="dname"></span> ?
          <span class="hidden did"></span>
        </div>
        <div class="modal-footer">
          <input type="hidden" id="token" value="{{ csrf_token() }}">

          <button type="button" class="btn actionBtn" data-dismiss="modal">
            <span id="footer_action_button" class='glyphicon'> </span>
          </button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">
            <span class='glyphicon glyphicon-remove'></span> Batal
          </button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
        },

    });
    var table = $('#datatables').DataTable();
});
</script>
<script>
 $(document).on('click', '.edit', function () {
      $('#footer_action_button').text(" Tanggapi");
      $('#footer_action_button').addClass('glyphicon-check');
      $('#footer_action_button').removeClass('glyphicon-trash');
      $('.actionBtn').addClass('btn-success');
      $('.actionBtn').removeClass('btn-danger');
      $('.actionBtn').removeClass('delete');
      $('.actionBtn').addClass('edit');
      $('.modal-title').text('Beri Tanggapan LHP');
      $('.deleteContent').hide();
      $('.form-horizontal').show();
      var stuff = $(this).data('info');
      fillmodalData(stuff)
      $('#myModal').modal('show');
    });

    function fillmodalData(details) {
      $('#fid').val(details);
    }

    $('.modal-footer').on('click', '.edit', function () {
      $.ajax({
        type: 'post',
        url: '/web_spi/public/audity/lhp/tanggap',
        data: {
          '_token': $('input[name=_token]').val(),
          'id': $("#fid").val(),
          'tanggapan': $('#fname').val(),
        },
        success: function (data) {
        location.reload();
        }
      });
    });
    </script>
@endsection
