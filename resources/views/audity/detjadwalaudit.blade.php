@extends('layouts.apps')
@section('nav_title')
Detail Jadwal Audit {{Auth::user()->nama}}
@endsection
@section('content')

<div class="content">
<div class="card">
			<div class="header text-center">
		        <h5 class="title">Jadwal Audit</h5>
		    </div>
            <div class="content">


                <table BORDER=0 CELLSPACING=0 CELLPADDING=20>
                    <tr>
                        <td>Nomor Surat Tugas</td>
                        <td> : </td>
                        <td>{!!$nomor_surat!!}</td>
                    </tr>
                    <tr>
                        <td>Nama Auditor</td>
                        <td> : </td>
                        <td>{!!$ketuaAuditor!!}</td>
                    </tr>
                    <tr>
                        <td>Unit Kerja Auditor</td>
                        <td> : </td>
                        <td>
                         @foreach($unitKerja as $u)
                            {{$u->nama}},
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Tipe Audit</td>
                        <td> : </td>
                        <td>{!!$tipe!!}</td>
                    </tr>
                    <tr>
                        <td>Aspek Audit</td>
                        <td> : </td>
                        <td>
                         @foreach($aspek_audit as $u)
                            {{$u->nama_aspek}},
                        @endforeach
                        </td>
                    </tr>
                    <tr>
                        <td>Tangal Mulai Audit</td>
                        <td> : </td>
                        <td>{!!$tanggal_mulai!!}</td>
                    </tr>
                    <tr>
                        <td>Tangal Selesai  Audit</td>
                        <td> : </td>
                        <td>{!!$tanggal_selesai!!}</td>
                    </tr>
                    <tr>
                        <td>Dokumen Audit</td>
                        <td> : </td>
                        <td>{!!$dokumen!!}</td>
                    </tr>
                    <tr>
                        <td>Keterangan</td>
                        <td> : </td>
                        <td>{!!$keterangan!!}</td>
                    </tr>
                </table>
                <br><br>
                <a href="{{ route('audity') }}" class="btn btn-fill btn-danger btn-flat">Kembali</a>

            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

@endsection
