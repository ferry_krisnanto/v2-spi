@extends('layouts.apps')
@section('nav_title')
Jadwal Audit {{Auth::user()->nama}}
@endsection
@section('content')

<div class="content">
<div class="card">
			<div class="header text-center">
		        <h5 class="title">Jadwal Audit</h5>
		    </div>
            <div class="content">
				<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{route('detjadwalaudit')}}">
					{{ csrf_field() }}
					<div class="form-group">
                        <label class="col-md-3 control-label">Pilih Nomor Surat<star>*</star></label>
                        <div class="col-md-6">
							<select name="jadwal_audit" class="selectpicker">
										@foreach($auditor as $a)
											<option value="{{$a->id}}">{{$a->nomorsurat}}</option>
										@endforeach
							</select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3"></label>

                        <div class="col-md-6">
							<button type="submit" class="btn btn-fill btn-info btn-flat">Pilih</button>
							<a href="{{ route('audity') }}" class="btn btn-fill btn-danger btn-flat">BATAL</a>
                        </div>
                    </div>
                </form>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

@endsection
