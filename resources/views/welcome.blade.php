<!doctype html>
<html lang="en">
@include('partials.htmlheader')
<style media="screen">
.mains-panel {
  background: #F5F5F5;
  position: relative;
  float: left;
  width: 100%;
  min-height: 100%;
}
.mains-panel > .content {
  padding: 30px 15px;
  min-height: calc(100vh - 136px);
}
.mains-panel > .content-no-padding {
  padding: 0;
}
.mains-panel > .footer {
  border-top: 1px solid #e7e7e7;
}
.mains-panel .navbar {
  margin-bottom: 0;
}
</style>
<body>
  <div class="wrapper">
    <div class="mains-panel">
      @include('partials.mainheader_welcome')
      <div class="content">
        <div class="container-fluid">
			<div class="row text-center">
			  <div class="col-md-12" >
				  <div class="card">
				  	<div class="content">
						<div id="myCarousel" class="carousel slide" data-ride="carousel">
	  					  <!-- Indicators -->
	  					  <ol class="carousel-indicators">
	  						<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	  						<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
	  					  </ol>

	  					  <!-- Wrapper for slides -->
	  					  <div class="carousel-inner">
	  						<div class="item active">
	  						  <img src="{{ asset('assets/img/banner/landing_1.png') }}" alt="Selamat Datang 1">
	  						</div>

	  						<div class="item">
	  						  <img src="{{ asset('assets/img/banner/landing_2.png') }}" alt="Selamat Datang 2">
	  						</div>

							<div class="item">
	  						  <img src="{{ asset('assets/img/banner/landing_3.png') }}" alt="Selamat Datang 3">
	  						</div>
	  					  </div>

	  					  <!-- Left and right controls -->
	  					  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
	  						<span class="glyphicon glyphicon-chevron-left"></span>
	  						<span class="sr-only">Previous</span>
	  					  </a>
	  					  <a class="right carousel-control" href="#myCarousel" data-slide="next">
	  						<span class="glyphicon glyphicon-chevron-right"></span>
	  						<span class="sr-only">Next</span>
	  					  </a>
	  					</div>
				  	</div>
				  </div>
			  </div>
			</div>
			<div class="row">
				<div class="col-md-12" >
				  <div class="card">
				      <div class="content">
						  <ul class="nav nav-tabs">
		  					<li class="active"><a data-toggle="tab" href="#beranda">Beranda</a></li>
		  					<li><a data-toggle="tab" href="#profil">Profil SPI</a></li>
		  					<li><a data-toggle="tab" href="#login">Login</a></li>
		  					<li><a data-toggle="tab" href="#help">Kontak SPI Unila</a></li>
		  				</ul>

		  				<div class="tab-content">
		  					<div id="beranda" class="tab-pane fade in active">
		  					  <h4 style="text-align: center;">Jadwal Audit SPI Universitas Lampung</h4>
							  <div class="row">
								  <div class="col-md-12">
									  <div class="card card-calendar">
			                              <div class="content">
			                                  <div id="kalenderku"></div>
			                              </div>
			                          </div>
								  </div>
							  </div>
							  <hr>
							  <div class="row">
			  					<div class="col-md-11">
			  						<div class="col-md-2">
			  							<form action="{{ route('searchTahun') }}" method="post" class="form-inline">
			  								{{ csrf_field() }}
			  								<div class="form-group row">
			  									<select class="form-control" name="tahun">
			  										@for($a = 2017; $a <= 2030; $a++)
			  											@if($tahun == $a)
			  												<option value="{{ $a }}" selected>{{ $a }}</option>
			  											@else
			  												<option value="{{ $a }}">{{ $a }}</option>
			  											@endif
			  										@endfor
			  									</select>
			  									<button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> </button>
			  								</div>
			  							</form>
			  						</div>
			  					</div>
			  					<div class="col-md-1">
			  						<a target="blank" href="{{ route('cetakJadwal', $tahun) }}"> <i class="fa fa-3x fa-download"></i></a>
			  					</div>
			  				</div>
			  				<hr>
							  <div class="row">
								  <div class="col-md-12">
									  <div class="card">
			                              <div class="content">
											  <div class="fresh-datatables">
												  <table class="table table-striped table-bordered" style="width:100%">
													  <tr align="center">
														  <th width="1%" rowspan="2">No</th>
														  <th rowspan="2">Unit Kerja</th>
														  <th colspan="12" style="text-align: center;">Bulan</th>
														  <th rowspan="2">Keterangan</th>
													  </tr>
													  <tr width="50px">
														  @for($x = 1; $x<=12; $x++)
															  <th>{{ $x }}</th>
														  @endfor
													  </tr>
													  <?php $i=1; ?>
													  @foreach($jadwal as $data)
													  <tr>
														  <td style="text-align: center;">{{ $i++ }}</td>
														  <td>{{ $data->auditee->unit_kerja }}</td>
														  <!-- START DATE -->
														  @for($y = 1; $y <= 12; $y++)
															  @if(( (int)date('m', strtotime($data->tanggal_mulai)) <= $y) && ( (int)date('m', strtotime($data->tanggal_selesai)) >= $y))
																  <td bgcolor="#3498db" width="50px"></td>
															  @else
																  <td width="50px"></td>
															  @endif
														  @endfor
														  <!-- END DATE -->
														  <td>{{ $data->keterangan }}</td>
													  </tr>
													  @endforeach
												  </table>
											  </div>
			                              </div>
			                          </div>
								  </div>
							  </div>
		  					</div>
		  					<div id="profil" class="tab-pane fade">
		  					  <h3>Profil SPI</h3>
		  					  <p>Berdasarkan Peraturan Pemerintah Nomor 60 Tahun 2008 tentang Sistem Pengendalian Intern Pemerintah dan Peraturan Menteri Pendidikan Nasional Republik Indonesia Nomor 47 Tahun 2011 tentang Satuan Pengawasan Intern di Lingkungan Kementerian Pendidikan Nasional, maka Universitas Lampung sebagai salah satu Unit Kerja Kementerian Pendidikan Nasional membentuk Satuan Pengendalian Internal Universitas Lampung (SPI UNILA). </p> <br>

							  <p>Tujuan dibentuknya Satuan Pengendalian Internal Unila sesuai dengan pengertian pada Permendiknas Nomor 47 Tahun 2011 Pasal 1 ayat (1) adalah untuk melaksanakan proses kegiatan audit, reviu, evaluasi, pemantauan, dan kegiatan pengawasan lain terhadap penyelenggaraan tugas dan fungsi organisasi yang bertujuan untuk mengendalikan kegiatan, mengamankan harta dan aset, terselenggaranya laporan keuangan yang baik, meningkatkan efektivitas dan efisiensi, dan mendeteksi secara dini terjadinya penyimpangan dan ketidakpatuhan terhadap ketentuan peraturan perundang-undangan, sehingga terbentuk Good University Governance.</p>
		  					</div>
		  					<div id="login" class="tab-pane fade">
		  					  <h3>Login</h3> 
		  					  <a href="{{ route('login') }}">SILAHKAN KLIK LINK INI UNTUK MENUJU HALAMAN LOGIN</a>
		  					</div>
		  					<div id="help" class="tab-pane fade">
		  					 <h2>Kontak SPI UNILA</h2>
                			<p>Jl. Prof. Dr. Soematri Brojonegoro No.1, Gedung Rektorat Universitas Lampung Lantai IV Ruang sekretariat SPI, Bandar Lampung.</p>
                			<div class="phone-no">
                	         <p><strong>Phone:</strong>082186693042</p>
                   	         <p><strong>E-mail:</strong><a href="mailto:spi@kpa.unila.ac.id">spi@kpa.unila.ac.id</a></p>
                             <p><strong>Website:</strong><a href="http://www.spi.unila.ac.id" target="_blank">http://www.spi.unila.ac.id</a></p>

		  					</div>
		  				</div>
				      </div>
				  </div>
				</div>
			</div>
        </div>
      </div>
      @include('partials.footer')
    </div>
  </div>
</body>
@include('partials.scripts')
<script type="text/javascript">
	$calendar = $('#kalenderku');

	today = new Date();
	y = today.getFullYear();
	m = today.getMonth();
	d = today.getDate();
	var jadwal 	= {!! $jadwalJson !!};
	$calendar.fullCalendar({
		header: {
			left: 'title',
			center: 'month,agendaWeek,agendaDay',
			right: 'prev,next today'
		},
		defaultDate: today,
		selectable: true,
		selectHelper: true,
		titleFormat: {
			month: 'MMMM YYYY', // September 2015
			week: "MMMM D YYYY", // September 2015
			day: 'D MMM, YYYY'  // Tuesday, Sep 8, 2015
		},

		editable: false,
		eventLimit: true, // allow "more" link when too many events


		// color classes: [ event-blue | event-azure | event-green | event-orange | event-red ]
		events: jadwal

	});
</script>
</html>
