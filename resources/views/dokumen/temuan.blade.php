<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/css/bootstrap.min.css" integrity="sha384-Zug+QiDoJOrZ5t4lssLdxGhVrurbmBWopoEl+M6BdEfwnCJZtKxi1KgxUyJq13dy" crossorigin="anonymous"> -->

    <title>{{ $laporan->kode_dokumen }}</title>
    <style media="screen">
        .table {
		  width: 100%;
		  max-width: 100%;
		  margin-bottom: 1rem;
		  background-color: transparent;
		}

.table th,
.table td {
  padding: 0.20rem;
  vertical-align: middle;
  border-top: 1px solid #000000;
}

.table thead th {
  vertical-align: middle;
  border-bottom: 2px solid #000000;
}

.table tbody + tbody {
  border-top: solid #000000;
}

.table .table {
  background-color: #fff;
}

.table-sm th,
.table-sm td {
  padding: 0.3rem;
}

.table-bordered {
  border: 1px solid #000000;
}

.table-bordered th,
.table-bordered td {
  border: 1px solid #000000;
}

.table-bordered thead th,
.table-bordered thead td {
  border-bottom-width: 1px;
}

.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(0, 0, 0, 0.05);
}

.table-hover tbody tr:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-primary,
.table-primary > th,
.table-primary > td {
  background-color: #b8daff;
}

.table-hover .table-primary:hover {
  background-color: #9fcdff;
}

.table-hover .table-primary:hover > td,
.table-hover .table-primary:hover > th {
  background-color: #9fcdff;
}

.table-secondary,
.table-secondary > th,
.table-secondary > td {
  background-color: #dddfe2;
}

.table-hover .table-secondary:hover {
  background-color: #cfd2d6;
}

.table-hover .table-secondary:hover > td,
.table-hover .table-secondary:hover > th {
  background-color: #cfd2d6;
}

.table-success,
.table-success > th,
.table-success > td {
  background-color: #c3e6cb;
}

.table-hover .table-success:hover {
  background-color: #b1dfbb;
}

.table-hover .table-success:hover > td,
.table-hover .table-success:hover > th {
  background-color: #b1dfbb;
}

.table-info,
.table-info > th,
.table-info > td {
  background-color: #bee5eb;
}

.table-hover .table-info:hover {
  background-color: #abdde5;
}

.table-hover .table-info:hover > td,
.table-hover .table-info:hover > th {
  background-color: #abdde5;
}

.table-warning,
.table-warning > th,
.table-warning > td {
  background-color: #ffeeba;
}

.table-hover .table-warning:hover {
  background-color: #ffe8a1;
}

.table-hover .table-warning:hover > td,
.table-hover .table-warning:hover > th {
  background-color: #ffe8a1;
}

.table-danger,
.table-danger > th,
.table-danger > td {
  background-color: #f5c6cb;
}

.table-hover .table-danger:hover {
  background-color: #f1b0b7;
}

.table-hover .table-danger:hover > td,
.table-hover .table-danger:hover > th {
  background-color: #f1b0b7;
}

.table-light,
.table-light > th,
.table-light > td {
  background-color: #fdfdfe;
}

.table-hover .table-light:hover {
  background-color: #ececf6;
}

.table-hover .table-light:hover > td,
.table-hover .table-light:hover > th {
  background-color: #ececf6;
}

.table-dark,
.table-dark > th,
.table-dark > td {
  background-color: #c6c8ca;
}

.table-hover .table-dark:hover {
  background-color: #b9bbbe;
}

.table-hover .table-dark:hover > td,
.table-hover .table-dark:hover > th {
  background-color: #b9bbbe;
}

.table-active,
.table-active > th,
.table-active > td {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover {
  background-color: rgba(0, 0, 0, 0.075);
}

.table-hover .table-active:hover > td,
.table-hover .table-active:hover > th {
  background-color: rgba(0, 0, 0, 0.075);
}

.table .thead-dark th {
  color: #fff;
  background-color: #212529;
  border-color: #32383e;
}

.table .thead-light th {
  color: #495057;
  background-color: #e9ecef;
  border-color: #000000;
}

.table-dark {
  color: #fff;
  background-color: #212529;
}

.table-dark th,
.table-dark td,
.table-dark thead th {
  border-color: #32383e;
}

.table-dark.table-bordered {
  border: 0;
}

.table-dark.table-striped tbody tr:nth-of-type(odd) {
  background-color: rgba(255, 255, 255, 0.05);
}

.table-dark.table-hover tbody tr:hover {
  background-color: rgba(255, 255, 255, 0.075);
}

@media (max-width: 575.99px) {
  .table-responsive-sm {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-sm > .table-bordered {
    border: 0;
  }
}

@media (max-width: 767.99px) {
  .table-responsive-md {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-md > .table-bordered {
    border: 0;
  }
}

@media (max-width: 991.99px) {
  .table-responsive-lg {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-lg > .table-bordered {
    border: 0;
  }
}

@media (max-width: 1199.99px) {
  .table-responsive-xl {
    display: block;
    width: 100%;
    overflow-x: auto;
    -webkit-overflow-scrolling: touch;
    -ms-overflow-style: -ms-autohiding-scrollbar;
  }
  .table-responsive-xl > .table-bordered {
    border: 0;
  }
}

.table-responsive {
  display: block;
  width: 100%;
  overflow-x: auto;
  -webkit-overflow-scrolling: touch;
  -ms-overflow-style: -ms-autohiding-scrollbar;
}

.table-responsive > .table-bordered {
  border: 0;
}
.text-center { text-align: center; }
    </style>
</head>
<body>
    <div class="container">
        <h3 class="text-center">Deskripsi Temuan Audit (DTA)</h3>
        <table class="table table-bordered" cellpadding="0" cellspacing="0">
            <tr>
                <td rowspan="4" style="text-align: center; justify-content: center; vertical-align:middle; padding-top: 10px;padding-bottom: 8px !important;"> <img src="{{ asset('assets/img/unila.png') }}"  width="100px"> <br> 
                  <h3 style="padding: 0px; margin: 0px" >SPI UNILA</h3> 
                </td>
                <td rowspan="4" style="text-align: center; justify-content: center; vertical-align:middle; padding: 1px !important;"><h2>DESKRIPSI TEMUAN <br> AUDIT (DTA)</h2></td>
                <td style="padding: 1px !important;">Kode Dokumen</td>
                <td style="padding: 1px !important;">{{ $laporan->kode_dokumen }}</td>
            </tr>
            <tr>
                <td style="padding: 1px !important;">Revisi</td>
                <td style="padding: 1px !important;">-</td>
            </tr>
            <tr>
                <td style="padding: 1px !important;">Tanggal Terbit</td>
                <td style="padding: 1px !important;"> {{ date('d F Y', strtotime($laporan->jadwal_penyelesaian)) }} </td>
            </tr>
            <tr>
                <td style="padding: 1px !important;">Halaman</td>
                <td style="padding: 1px !important;">2 Halaman</td>
            </tr>
        </table>
        <table class="table table-bordered" cellpadding="0" cellspacing="0">
            <tr>
                <td class="text-center" style="background:#efefef; font-weight: bold;">Unit Kerja</td>
                <td class="text-center" style="background:#efefef; font-weight: bold;">Tipe Audit</td>
                <td colspan="2" class="text-center" style="background:#efefef; font-weight: bold;">Aspek Audit</td>
            </tr>
            <tr>
                <td class="text-center">{{ $laporan->jadwalAudit->Auditee->unit_kerja }}</td>
                <td class="text-center">{{ $laporan->jadwalAudit->tipeAudit->nama_tipe }}</td>
                <td colspan="2" class="text-center">
					@foreach($laporan->jadwalAudit->jadwalAspek as $key => $data)
						@if($key == 0)
						{{ $data->aspekAudit->nama_aspek }}
						@else
						/{{ $data->aspekAudit->nama_aspek }}
						@endif
					@endforeach
				</td>
            </tr>
            <tr>
                <td style="font-weight: bold;">Distribusi Audit</td>
                <td style="font-weight: bold;">Auditi : </td>
                <td style="font-weight: bold;">Rektor : </td>
                <td style="font-weight: bold;">Arsip : </td>
            </tr>
            <tr>
                <td class="text-center" style="background:#efefef; font-weight: bold;">Masa Audit</td>
                <td class="text-center" style="background:#efefef; font-weight: bold;">Tanggal Audit</td>
                <td colspan="2" class="text-center" style="background:#efefef; font-weight: bold;">Tim Audit</td>
            </tr>
            <tr>
				<?php
					$end = \Carbon\Carbon::parse($laporan->jadwalAudit->tanggal_selesai);
					$start = \Carbon\Carbon::parse($laporan->jadwalAudit->tanggal_mulai);
					$diff = $end->diffInDays($start);
				 ?>
                <td class="text-center">{{ $diff }} hari</td>
                <td class="text-center">{{ date('d F Y', strtotime($laporan->jadwalAudit->tanggal_mulai)) }}</td>
                <td rowspan="3" colspan="2">
					<ul>
						@foreach($laporan->jadwalAudit->auditorAudit as $dataAuditor)
							<li>{{ $dataAuditor->auditor->user->nama }} ({{ $dataAuditor->jabatanAuditor->nama }})</li>
						@endforeach
					</ul>
				</td>
            </tr>
            <tr>
                <td class="text-center" style="background:#efefef; font-weight: bold;">Nomor Urut Temuan</td>
                <td class="text-center" style="background:#efefef; font-weight: bold;">Kode Temuan</td>
            </tr>
            <tr>
                <td class="text-center">{{ $laporan->nomor_urut }}</td>
                <td class="text-center">{{ $laporan->kode_temuan }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Deskripsi Temuan/Kondisi</td>
                <td colspan="3">{{ $laporan->deskripsi_temuan }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Kriteria/Pesyaratan</td>
                <td colspan="3">{{ $laporan->kriteria }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Akibat</td>
                <td colspan="3">{{ $laporan->akibat }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Sebab</td>
                <td colspan="3">{{ $laporan->sebab }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Simpulan/Temuan</td>
                <td colspan="3">{{ $laporan->kesimpulan }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Tanggapan Auditi</td>
                <td colspan="3">{{ $laporan->tanggapanAudit->tanggapan_auditee }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Tanggapan Auditor</td>
                <td colspan="3">{{ $laporan->tanggapanAudit->tanggapan_auditor }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Rekomendasi</td>
                <td colspan="3">{{ $laporan->rekomendasi }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Rencana Perbaikan</td>
                <td colspan="3">{{ $laporan->rencana_perbaikan }}</td>
            </tr>
            <tr>
                <td style="background:#efefef; font-weight: bold;">Tindak Lanjut</td>
                <td colspan="3">{{ $laporan->tindak_lanjut }}</td>
            </tr>
            <tr>
                <th colspan="2" class="text-center" style="background:#efefef; font-weight: bold;">Jadwal Penyelesaian</th>
                <th colspan="2" class="text-center" style="background:#efefef; font-weight: bold;">Penanggung Jawab</th>
            </tr>
            <tr>
                <td colspan="2" class="text-center">{{ date('d F Y', strtotime($laporan->jadwal_penyelesaian)) }}</td>
                <td colspan="2" class="text-center">Koordinator Bidang Keuangan <br><br><br><br> (............................................) <br> NIP ............................. </td>
            </tr>
            <tr>
                <th colspan="2" class="text-center" style="background:#efefef; font-weight: bold;">Pimpinan Unit Kerja</th>
                <th colspan="2" class="text-center" style="background:#efefef; font-weight: bold;"> Ketua Tim Audit</th>
            </tr>
            <tr>
                <td colspan="2" class="text-center"><br><br><br>{{ $laporan->jadwalAudit->Auditee->pimpinan_kerja }} <br>NIP. {{ $laporan->jadwalAudit->Auditee->nip }} </td>
                <td colspan="2" class="text-center"><br><br><br>{{ $nama_ketua }} <br>NIP. {{ $nip_ketua }} </td>
            </tr>
            <tr>
                <th colspan="4" class="text-center" style="background:#efefef; font-weight: bold;">Hasil Audit Disahkan Oleh Ketua SPI:</th>
            </tr>
            <tr>
                <td colspan="4" class="text-center"><br><br><br>Dr. Heni Siswanto, S.H., M.H. <br>NIP. 19650204 199003 1 004</td>
            </tr>
        </table>
    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.3/js/bootstrap.min.js" integrity="sha384-a5N7Y/aK3qNeh15eJKGWxsqtnX/wWdSZSKp+81YjTmS15nvnvxKHuzaWwXHDli+4" crossorigin="anonymous"></script>
</body>
</html>
