@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Jenis Audit
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
		        <h5 class="title">Jenis Audit</h5>
		    </div>
            <div class="content">
				<button data-toggle="modal" data-target="#tambahJenisAudit" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Tipe Audit</th>
								<th>Aspek Audit</th>
								<th>Dokumen Audit</th>
                                <th>Keterangan</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
							<tr>
                                <td>1</td>
                                <td>PDTT</td>
								<td>Aspek Keuangan</td>
								<td>
									<u>
										<li>Dokumen Keuangan</li>
										<li>Dokumen Anu</li>
									</u>
								</td>
                                <td>Ini keterangannya</td>
                                <td class="text-right">
									<button data-toggle="modal" data-target="#editJenisAudit" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-edit"></i></button>
                                    <a href="#" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Tambah Jenis Audit -->
<div class="modal fade" id="tambahJenisAudit" tabindex="-1" role="dialog" aria-labelledby="tambahJenisAuditLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Jenis Audit</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Tipe Audit</label>
			  <star>*</star>
              <input type="text" name="tipe_audit" value="{{ old('tipe_audit') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>Aspek Audit</label>
			  <star>*</star>
              <input type="text" name="aspek_audit" value="{{ old('aspek_audit') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>Dokumen Audit</label>
			  <star>*</star>
			  <div class="input-group">
				  <input type="text" name="dokumen[]" value="{{ old('dokumen[]') }}" class="form-control" required="true">
				  <span class="input-group-addon"><a href="#"><i class="fa fa-plus-circle"></i></a></span>
			  </div>

          </div>
          <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan" value="{{ old('keterangan') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Jenis Audit -->
<div class="modal fade" id="editJenisAudit" tabindex="-1" role="dialog" aria-labelledby="editJenisAuditLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Edit Data Auditee</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Tipe Audit</label>
			  <star>*</star>
              <input type="text" name="tipe_audit" value="{{ old('tipe_audit') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>Aspek Audit</label>
			  <star>*</star>
              <input type="text" name="aspek_audit" value="{{ old('aspek_audit') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>Dokumen Audit</label>
			  <star>*</star>
			  <div class="input-group">
				  <input type="text" name="dokumen" value="{{ old('dokumen') }}" class="form-control" required="true">
				  <span class="input-group-addon"><a href="#"><i class="fa fa-plus-circle"></i></a></span>
			  </div>

          </div>
          <div class="form-group">
              <label>Keterangan</label>
              <input type="text" name="keterangan" value="{{ old('keterangan') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Auditee",
        },

    });
    var table = $('#datatables').DataTable();
});

</script>
@endsection
