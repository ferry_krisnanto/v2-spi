@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Data Auditor
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
				<h5 class="title">Data Auditor</h5>
			</div>
            <div class="content">
				<button data-toggle="modal" data-target="#tambahAuditor" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th>NIP</th>
								                <th>Fakultas</th>
								                <th>Jurusan</th>
								                <th>Golongan</th>
								                <th>Token</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $i=1; ?>
							@foreach($auditor as $data)
							<tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $data->user->nama }}</td>
                                <td>{{ $data->nip }}</td>
								<td>{{ $data->jurusan->fakultas->nama_fakultas }}</td>
								<td>{{ $data->jurusan->nama_jurusan }}</td>
								<td>{{ $data->golongan->nama }}</td>
								<td>{{ $data->token }}</td>
                                <td class="text-right">
									<button value="{{ $data->id }}" data-toggle="modal" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-edit"></i></button>
                                    <a href="{{ route('operator.auditor.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Edit Auditor -->
<div class="modal fade" id="editAuditor" tabindex="-1" role="dialog" aria-labelledby="editAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editAuditorLabel">Form Edit Data Auditor</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.auditor.edit.post') }}" method="POST">
			<input type="hidden" name="idAuditor" id="idAuditor">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama<star>*</star></label>
			  <input type="text" class="form-control" id="nama" name="nama" value="{{ old('nama') }}">
          </div>
		  <div class="form-group">
              <label>Fakultas<star>*</star></label>
			  <select class="form-control fakultasUbah" maxlength="3" onkeypress="return isNumberKey(event)" name="id_fakultas" id="id_fakultas">
				  <option selected disabled>-- FAKULTAS --</option>
				  @foreach($fak as $data)
				  	<option value="{{ $data->id }}">{{ $data->nama_fakultas }}</option>
				  @endforeach
			  </select>
          </div>
		  <div class="form-group">
              <label>Jurusan<star>*</star></label>
			  <select class="form-control id_jurusan" name="id_jurusan" id="jurusanUbah">
				  <option value="">-- JURUSAN --</option>
				  @foreach($jur as $data)
				  	<option value="{{ $data->id }}">{{ $data->nama_jurusan }}</option>
				  @endforeach
			  </select>
          </div>
		  <div class="form-group">
              <label>Golongan<star>*</star></label>
			  <select class="form-control" name="id_golongan" id="id_golongan">
				  <option value="" selected disabled>-- GOLONGAN --</option>
				  @foreach($gol as $data)
				  	<option value="{{ $data->id }}">{{ $data->nama }}</option>
				  @endforeach
			  </select>
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Tambah Auditor -->
<div class="modal fade" id="tambahAuditor" tabindex="-1" role="dialog" aria-labelledby="tambahAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Data Auditor</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.auditor.tambah.post') }}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama<star>*</star></label>
			 <input type="text" class="form-control" name="nama" value="{{ old('nama') }}">
          </div>
		  <div class="form-group">
              <label>NIP<star>*</star></label>
			  <input type="text" class="form-control" name="nip" maxlength="18" onkeypress="return isNumberKey(event)" value="{{ old('nip') }}">
          </div>
		  <div class="form-group">
              <label>Fakultas<star>*</star></label>
			  <select class="form-control fakultasTambah" name="id_fakultas">
				  <option selected disabled>-- FAKULTAS --</option>
				  @foreach($fak as $data)
				  	<option value="{{ $data->id }}">{{ $data->nama_fakultas }}</option>
				  @endforeach
			  </select>
          </div>
		  <div class="form-group">
              <label>Jurusan<star>*</star></label>
			  <select class="form-control" name="id_jurusan" id="jurusanTambah">
				  <option value="">-- JURUSAN --</option>
			  </select>
          </div>
		  <div class="form-group">
              <label>Golongan<star>*</star></label>
			  <select class="form-control" name="id_golongan">
				  <option value="" selected disabled>-- GOLONGAN --</option>
				  @foreach($gol as $data)
				   <option value="{{ $data->id }}">{{ $data->nama }}</option>
				 @endforeach
			  </select>
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Auditor",
        },

    });
    var table = $('#datatables').DataTable();

	$('.fakultasTambah').change(function(){
		var fakultasId = +$(this).val();
	    $.get('{{ URL::to("operator/auditor/jurusan") }}/' + fakultasId, function (data) {
			var options = '';
            for (var x = 0; x < data.length; x++) {
                options += '<option value="' + data[x]['id'] + '">' + data[x]['nama_jurusan'] + '</option>';
            }
            $('#jurusanTambah').html(options);
	    });
	});

	$('.fakultasUbah').change(function(){
		var fakultasId = +$(this).val();
	    $.get('{{ URL::to("operator/auditor/jurusan") }}/' + fakultasId, function (data) {
			var options = '';
            for (var x = 0; x < data.length; x++) {
                options += '<option value="' + data[x]['id'] + '">' + data[x]['nama_jurusan'] + '</option>';
            }
            $('#jurusanUbah').html(options);
	    });
	});

	$('.edit').click(function(){
		var userId = +$(this).val();
	    $.get('{{ URL::to("operator/auditor/search") }}/' + userId, function (data) {
			$('#idAuditor').val(data.id);
			$('#nama').val(data.nama);
			$('#id_fakultas').val(data.id_fakultas);
			$('.id_jurusan').val(data.id_jurusan);
			$('#id_golongan').val(data.id_golongan);
	    });
		event.stopPropagation();
        $('#editAuditor').modal('show');
	});
});

</script>

@endsection
<script>
    function isNumberKey(evt){
        var charCode=(evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false; 
    return true;
    }
</script>
