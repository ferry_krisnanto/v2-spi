@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Fakultas dan Jurusan
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
				<h5 class="title">Fakultas</h5>
			</div>
            <div class="content">
				<button data-toggle="modal" data-target="#tambahFakultas" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $i=1; ?>
							@foreach($fakultas as $data)
							<tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $data->nama_fakultas }}</td>
                                <td class="text-right">
									<button value="{{ $data->id }}" data-toggle="modal" type="button" class="btn btn-simple btn-warning btn-icon editFakultas" name="button"><i class="fa fa-edit"></i></button>
                                    <a href="{{ route('operator.fakultas.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
				<h5 class="title">Jurusan</h5>
			</div>
            <div class="content">
				<button data-toggle="modal" data-target="#tambahJurusan" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>
				<br>
                <div class="fresh-datatables">
					<table id="datatables-jurusan" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
								<th>Fakultas</th>
                                <th>Nama Jurusan</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $i=1; ?>
							@foreach($jurusan as $data)
							<tr>
                                <td>{{ $i++ }}</td>
								<td>{{ $data->fakultas->nama_fakultas }}</td>
                                <td>{{ $data->nama_jurusan }}</td>
                                <td class="text-right">
									<button value="{{ $data->id }}" data-toggle="modal" type="button" class="btn btn-simple btn-warning btn-icon editJurusan" name="button"><i class="fa fa-edit"></i></button>
                                    <a href="{{ route('operator.jurusan.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Tambah Fakultas -->
<div class="modal fade" id="tambahFakultas" tabindex="-1" role="dialog" aria-labelledby="editAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editAuditorLabel">Tambah Fakultas</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.fakultas.tambah.post') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama Fakultas</label>
			  <input type="text" class="form-control" name="nama_fakultas" value="{{ old('nama_fakultas') }}">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Tambah Jurusan -->
<div class="modal fade" id="tambahJurusan" tabindex="-1" role="dialog" aria-labelledby="editAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="editAuditorLabel">Tambah Jurusan</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.jurusan.tambah.post') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama</label>
			  <input type="text" class="form-control" name="nama_jurusan" value="{{ old('nama_jurusan') }}">
          </div>
		  <div class="form-group">
              <label>Fakultas</label>
			  <select class="form-control" name="id_fakultas">
				  <option selected disabled>-- FAKULTAS --</option>
				  @foreach($fakultas as $data)
				  	<option value="{{ $data->id }}">{{ $data->nama_fakultas }}</option>
				  @endforeach
			  </select>
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Fakultas -->
<div class="modal fade" id="editFakultas" tabindex="-1" role="dialog" aria-labelledby="tambahAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Fakultas</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.fakultas.edit.post') }}" method="post">
          {{ csrf_field() }}
		  <input type="hidden" name="id" id="id_fakultas">
          <div class="form-group">
              <label>Nama</label>
			 <input type="text" class="form-control" name="nama_fakultas" value="{{ old('nama_fakultas') }}" id="nama_fakultas">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Jurusan -->
<div class="modal fade" id="editJurusan" tabindex="-1" role="dialog" aria-labelledby="tambahAuditorLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Jurusan</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.jurusan.edit.post') }}" method="post">
          {{ csrf_field() }}
		  <input type="hidden" name="id" id="id_jurusan">
          <div class="form-group">
              <label>Nama</label>
			 <input type="text" class="form-control" name="nama_jurusan" value="{{ old('nama_jurusan') }}" id="nama_jurusan">
          </div>
		  <select class="form-control" name="id_fakultas" id="id_fakultas_jurusan">
			  <option selected disabled>-- FAKULTAS --</option>
			  @foreach($fakultas as $data)
				<option value="{{ $data->id }}">{{ $data->nama_fakultas }}</option>
			  @endforeach
		  </select>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Fakultas",
        },

    });
    var table = $('#datatables').DataTable();

	$('#datatables-jurusan').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Jurusan",
        },

    });
    var table = $('#datatables-jurusan').DataTable();

	$('.editFakultas').click(function(){
		var idNya = +$(this).val();
	    $.get('{{ URL::to("operator/fakultas/search") }}/' + idNya, function (data) {
			$('#id_fakultas').val(data.id);
			$('#nama_fakultas').val(data.nama_fakultas);
	    });
		event.stopPropagation();
        $('#editFakultas').modal('show');
	});

	$('.editJurusan').click(function(){
		var idNya = +$(this).val();
	    $.get('{{ URL::to("operator/fakultas/jurusan/search") }}/' + idNya, function (data) {
			$('#id_jurusan').val(data.id);
			$('#id_fakultas_jurusan').val(data.id_fakultas);
			$('#nama_jurusan').val(data.nama_jurusan);
	    });
		event.stopPropagation();
        $('#editJurusan').modal('show');
	});
});

</script>
@endsection
