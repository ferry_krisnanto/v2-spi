@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Manajemen Data User
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
      <div class="header text-center">
            <h5 class="title">Data User</h5>
        </div>
            <div class="content">
         {{--<button data-toggle="modal" data-target="#tambahUser" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>--}}
        <br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama</th>
								                <th>Jabatan</th>
                                <th>Username</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
						<?php $i=1; ?>
              @foreach($user as $data)
              <tr>
                                <td>{{ $i++ }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ $data->role->nama }}</td>
                                <td>{{ $data->username }}</td>
                                <td class="text-right">
                  {{-- <button data-toggle="modal" data-target="#editUser" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-edit"></i></button> --}}
                  <button data-toggle="modal" value="{{ $data->id }}" type="button" class="btn btn-simple btn-info btn-icon editPassword" name="button"><i class="fa fa-lock"></i></button>
                                    <a href="{{ route('operator.data-user.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
              @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Tambah Data User -->
<div class="modal fade" id="tambahUser" tabindex="-1" role="dialog" aria-labelledby="tambahUserLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Data User</h4>
      </div>
      <div class="modal-body">
        <form action="{{route('operator.data-user.tambah.post')}}" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>NIP</label>
              <input type="text" name="pimpinan" value="{{ old('pimpinan') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Jabatan</label>
        <select class="form-control" name="jabatan">
          <option value="" selected disabled>---JABATAN---</option>
          @foreach($jabatans as $jabatan)
          <option value="{{$jabatan->id}}" >{{$jabatan->nama}}</option>
          @endforeach
              </select>
          </div>
          <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" value="{{ old('username') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Password</label>
              <input type="text" name="password" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Konfirmasi Password</label>
              <input type="text" name="password-confirm" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edite Data User -->
<div class="modal fade" id="editUser" tabindex="-1" role="dialog" aria-labelledby="editUserLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Edit Data User</h4>
      </div>
      <div class="modal-body">
        <form action="#" method="post">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>NIP</label>
              <input type="text" name="pimpinan" value="{{ old('pimpinan') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Jabatan</label>
              <select class="form-control" name="jabatan">
          <option value="" selected disabled>---JABATAN---</option>
              </select>
          </div>
          <div class="form-group">
              <label>Username</label>
              <input type="text" name="username" value="{{ old('username') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Password</label>
              <input type="text" name="password" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Konfirmasi Password</label>
              <input type="text" name="password-confirm" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Ubah Password User -->
<div class="modal fade" id="resetPassword" tabindex="-1" role="dialog" aria-labelledby="resetPasswordLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Reset Password</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.data-user.reset.password') }}" method="post">
          {{ csrf_field() }}
      <input type="hidden" name="idUser" id="idUser">
          <div class="form-group">
              <label>Nama</label>
              <input type="text" name="nama" id="nama" class="form-control" required="true" readonly>
          </div>
          
          <div class="form-group">
              <label>Username</label>
              <input type="text" class="form-control" id="username" name="username" value="{{ old('username') }}">
          </div>
      <div class="form-group">
              <label>Password</label>
              <input type="text" name="password" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Konfirmasi Password</label>
              <input type="text" name="password_confirmation" class="form-control" required="true">
          </div>
      </div>

      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[25, 50, 60, -1], [25, 50, 60, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari User",
        },

    });
    var table = $('#datatables').DataTable();

  $('.editPassword').click(function(){
    var userId = +$(this).val();
      $.get('{{ URL::to("operator/data-user/search") }}/' + userId, function (data) {
      $('#idUser').val(data.id);
      $('#nama').val(data.nama);
      $('#username').val(data.username);
      });
    event.stopPropagation();
        $('#resetPassword').modal('show');
  });
});

</script>
@endsection
