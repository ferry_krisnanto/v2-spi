@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Data Auditi
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
		<div class="card">
			<div class="header text-center">
		        <h5 class="title">Data Auditi</h5>
		    </div>
            <div class="content">
				<button data-toggle="modal" data-target="#tambahAuditee" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Unit Kerja</th>
								                <th>Pimpinan Unit Kerja</th>
								                <th>NIP</th>
								                <th>Masa Kerja</th>
                                <th>Keterangan</th>
								                <th>Token</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $i=1; ?>
							@foreach($auditee as $data)
							<tr>
	                            <td>{{ $i++ }}</td>
	                            <td>{{ $data->unit_kerja }}</td>
								              <td>{{ $data->pimpinan_kerja }}</td>
								              <td>{{ $data->nip }}</td>
								              <td> <label class="label label-sm label-primary">{{ $data->masa_kerja }}</label> </td>
	                            <td>{{ $data->keterangan }}</td>
								              <td>{{ $data->token }} </td>
	                            <td class="text-right">
									<!--<button data-toggle="modal" value="{{ $data->id }}" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-edit"></i></button> -->
									<button value="{{ $data->id }}" data-toggle="modal" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-edit"></i></button>
	                                <a href="{{ route('operator.auditi.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
	                            </td>
							</tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Tambah Auditee -->
<div class="modal fade" id="tambahAuditee" tabindex="-1" role="dialog" aria-labelledby="tambahAuditeeLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Data Auditi</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.auditi.tambah.post') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama Unit Kerja<star>*</star></label>
              <input type="text" name="unit_kerja" value="{{ old('unit_kerja') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>Pimpinan Unit Kerja<star>*</star></label>
              <input type="text" name="pimpinan_kerja" value="{{ old('pimpinan_kerja') }}" class="form-control" required="true">
          </div>
		  <div class="form-group">
              <label>NIP<star>*</star></label>
              <input type="text" name="nip" value="{{ old('nip') }}" class="form-control" maxlength="18" onkeypress="return isNumberKey(event)" required="true">
          </div>
		  <div class="form-group row">
			  <div class="col-md-12">
				  <?php $yearNow = \Carbon\Carbon::now()->format('Y'); ?>
				  <label>Masa Kerja<star>*</star></label>
				  <div class="row">
					  <div class="col-md-6">
						  <select class="form-control" name="tahun_mulai">
							  <option selected disabled>- PILIH TAHUN MULAI -</option>
							  @for($i = 1980; $i <= $yearNow ; $i++)
							  	<option value="{{ $i }}">{{ $i }}</option>
							  @endfor
						  </select>
					  </div>

					  <div class="col-md-6">
						  <select class="form-control" name="tahun_selesai">
							  <option selected disabled>- PILIH TAHUN SELESAI -</option>
							  @for($i = 1980; $i <= $yearNow ; $i++)
							  	<option value="{{ $i }}">{{ $i }}</option>
							  @endfor
							  <option value="MASIH AKTIF">MASIH AKTIF</option>
						  </select>
					  </div>
				  </div>
			  </div>
          </div>
          <div class="form-group">
              <label>Keterangan<star>*</star></label>
              <input type="text" name="keterangan" value="{{ old('keterangan') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Auditee -->
<div class="modal fade" id="editAuditee" tabindex="-1" role="dialog" aria-labelledby="editAuditeeLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Edit Data Auditi</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.auditi.edit.post') }}" method="post">
        {{ csrf_field() }}
		<input type="text" class="form-control" name="idAuditee" id="idAuditee" value="{{ old('idAuditee') }}"> 
		 
          <div class="form-group">
              <label>Nama Unit Kerja<star>*</star></label>
              <input type="text" name="unit_kerja" id="unit" value="{{ old('unit_kerja') }}" class="form-control" required="true">
          </div>
      <div class="form-group">
              <label>Pimpinan Unit Kerja<star>*</star></label>
              <input type="text" name="pimpinan_kerja"  id="pimpinan" value="{{ old('pimpinan_kerja') }}" class="form-control" required="true">
          </div>
      <div class="form-group row">
        <div class="col-md-12">
          <?php $yearNow = \Carbon\Carbon::now()->format('Y'); ?>
          <label>Masa Kerja<star>*</star></label>
          <div class="row">
            <div class="col-md-6">
              <select class="form-control" name="tahun_mulai" id="tahun_mulai">
                <option selected disabled>- PILIH TAHUN MULAI -</option>
                @for($i = 1980; $i <= $yearNow ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                @endfor
              </select>
            </div>

            <div class="col-md-6">
              <select class="form-control" name="tahun_selesai" id="tahun_selesai">
                <option selected disabled>- PILIH TAHUN SELESAI -</option>
                @for($i = 1980; $i <= $yearNow ; $i++)
                  <option value="{{ $i }}">{{ $i }}</option>
                @endfor
                <option value="MASIH AKTIF">MASIH AKTIF</option>
              </select>
            </div>
          </div>
        </div>
          </div>
          <div class="form-group">
              <label>Keterangan<star>*</star></label>
              <input type="text" name="keterangan" id="keterangan" value="{{ old('keterangan') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>


@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Auditii",
        },

    });
    var table = $('#datatables').DataTable();

	$('.edit').click(function(){
		var userId = +$(this).val();
	    $.get('{{ URL::to("operator/auditi/search") }}/' + userId, function (data) {
			$('#idAuditee').val(data.id);
			$('#unit').val(data.unit_kerja);
			$('#pimpinan').val(data.pimpinan_kerja);
			$('#keterangan').val(data.keterangan);
			$('#tahun_mulai option[value='+data.tahun_mulai+']').prop('selected', true);
			$('#tahun_selesai option[value="'+data.tahun_selesai+'"]').prop('selected', true);
	    });

		event.stopPropagation();
        $('#editAuditee').modal('show');
	});
});

</script>
@endsection
<script>
    function isNumberKey(evt){
        var charCode=(evt.which) ? evt.which : event.keyCode
        if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false; 
    return true;
    }
</script>