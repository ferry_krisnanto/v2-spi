@extends('layouts.apps')
@section('nav_title')
Beranda
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
		        <h5 class="title">Jadwal Audit</h5>
		    </div>
            <div class="content">
				<div class="row">
					<div class="col-md-12">
						<a href="{{ route('operator.jadwal-audit.tambah') }}" class="btn btn-primary btn-block"><i class="fa fa-plus"></i> Tambah</a>
					</div>
				</div>

				<br>
				<div class="row">
					<div class="col-md-12">
						<div class="fresh-datatables">
		                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
								<thead>
									<tr>
			                            <th>No</th>
			                            <th>No Surat Tugas</th>
										<th>Unit Kerja</th>
										<th>Tgl Mulai Audit</th>
										<th>Tgl Selesai Audit</th>
										<th>Aksi</th>
									</tr>
								</thead>
								<tbody>
									<?php $i=1; ?>
									@foreach($jadwal as $data)
									<tr>
										<td>{{ $i++ }}</td>
										<td>{{ $data->nomorsurat }}</td>
										<td>{{ $data->auditee->unit_kerja }}</td>
										<td>{{ date('d-m-Y', strtotime($data->tanggal_mulai)) }}</td>
										<td>{{ date('d-m-Y', strtotime($data->tanggal_selesai)) }}</td>
										<td> <a href="{{ route('operator.jadwal-audit.edit', $data->id) }}" class="fa fa-pencil"></a> <a href="{{ route('operator.jadwal-audit.hapus', $data->id) }}" class="fa fa-trash delete"></a> </td>
			                        </tr>
									@endforeach
								</tbody>

		                    </table>
		                </div>
					</div>
				</div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Jadwal",
        },

    });
    var table = $('#datatables').DataTable();
});

</script>
@endsection
