@extends('layouts.apps')
@section('nav_title')
Beranda
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
	<div class="alert alert-danger">
		<ul>
			@foreach ($errors->all() as $error)
			<li>{{ $error }}</li>
			@endforeach
		</ul>
	</div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
			<div class="header text-center">
		        <h5 class="title">Jadwal Audit</h5>
		    </div>
            <div class="content">
				<div class="row">
					<div class="col-md-11">
						<div class="col-md-2">
							<form action="{{ route('operator.jadwal-audit.search') }}" method="post" class="form-inline">
								{{ csrf_field() }}
								<div class="form-group row">
									<select class="form-control" name="tahun">
										@for($a = 2017; $a <= 2030; $a++)
											@if($tahun == $a)
												<option value="{{ $a }}" selected>{{ $a }}</option>
											@else
												<option value="{{ $a }}">{{ $a }}</option>
											@endif
										@endfor
									</select>
									<button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> </button>
								</div>
							</form>
						</div>
					</div>
					<div class="col-md-1">
						<a href="{{ route('operator.jadwal-audit.download', $tahun) }}"> <i class="fa fa-3x fa-download"></i></a>
					</div>
				</div>
				<hr>
				{{-- <div class="row">
					<div class="col-md-4">
						<a href="{{ route('operator.jadwal-audit.tambah') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus"></i> Tambah</a>
						<a href="#"><i class="fa fa-print fa-lg"></i></a>
						<a href="#"><i class="fa fa-pencil fa-lg"></i></a>
					</div>
				</div>
				--}}
				<br>
				<div class="row">
					<div class="col-md-12">
						<div class="fresh-datatables">
		                    <table class="table table-striped table-bordered" style="width:100%">
		                        <tr align="center">
		                            <th width="1%" rowspan="2">No</th>
		                            <th rowspan="2">Unit Kerja</th>
									<th colspan="12" style="text-align: center;">Bulan</th>
									<th rowspan="2">Keterangan</th>
								</tr>
								<tr width="50px">
									@for($x = 1; $x<=12; $x++)
										<th>{{ $x }}</th>
									@endfor
								</tr>
								<?php $i=1; ?>
								@foreach($jadwal as $data)
								<tr>
									<td style="text-align: center;">{{ $i++ }}</td>
									<td>{{ $data->auditee->unit_kerja }}</td>
									<!-- START DATE -->
									@for($y = 1; $y <= 12; $y++)
										@if(( (int)date('m', strtotime($data->tanggal_mulai)) <= $y) && ( (int)date('m', strtotime($data->tanggal_selesai)) >= $y))
											<td bgcolor="#3498db" width="50px"></td>
										@else
											<td width="50px"></td>
										@endif
									@endfor
									<!-- END DATE -->
									<td>{{ $data->keterangan }}</td>
								</tr>
								@endforeach
		                    </table>
		                </div>
					</div>
				</div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
@endsection

@section('script')

@endsection
