@extends('layouts.apps')
@section('nav_title')
Edit Jadwal Audit
@endsection
@section('style')
<style media="screen">
.form-horizontal .control-label{
/* text-align:right; */
text-align:left;
}
</style>
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
		@if (count($errors) > 0)
		<div class="row">
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		</div>
		@endif
        <div class="card">
			<div class="header text-center">
		        <h5 class="title">Form Edit Data Jadwal Audit</h5>
		    </div>
            <div class="content">
				<form class="form-horizontal" enctype="multipart/form-data" method="POST" action="{{ route('operator.jadwal-audit.edit.post') }}">
					{{ csrf_field() }}
					<input type="hidden" name="id" value="{{ $jadwal->id }}">
					<input type="hidden" name="nama_surat" value="{{ $jadwal->attachment_surattugas }}">
                    <div class="form-group">
                        <label class="col-md-3 control-label label"><b>Nomor Surat Tugas</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="text" placeholder="Contoh: 1836/UN26/KP/2017" value="{{ $jadwal->nomorsurat }}" name="nomorsurat" class="form-control">
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label"><b>Nama Auditor</b> </label>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Ketua Audit<star>*</star></label>
                        <div class="col-md-6">
							<select id="ketua_audit" name="ketua_audit" class="selectpicker" data-title="--PILIH AUDITOR--" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
								@foreach($auditor as $data)
									@if($ketua->id_auditor == $data->id)
										<option selected value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@endif
								@endforeach
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Sekretaris<star>*</star></label>
                        <div class="col-md-6">
							<select id="sekretaris_audit" name="sekretaris" class="selectpicker" data-title="--PILIH AUDITOR--" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
								@foreach($auditor as $data)
									@if($sekretaris->id_auditor == $data->id)
										<option selected value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@endif
								@endforeach
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Anggota Tim<star>*</star></label>
                        <div class="col-md-6">
							<select id="anggota_tim" multiple data-title="--PILIH AUDITOR--" name="anggota_tim[]" class="selectpicker" data-style="btn-default btn-fill btn-block" data-menu-style="dropdown-blue">
								@foreach($auditor as $data)
										@if(in_array($data->id, $tim))
											<option selected value="{{ $data->id }}">{{ $data->user->nama }}</option>
										@else
											<option value="{{ $data->id }}">{{ $data->user->nama }}</option>
										@endif
								@endforeach
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Anggota Penunjang</label>
                        <div class="col-md-6">
							<select id="anggota_penunjang" multiple data-title="--PILIH AUDITOR--" name="anggota_penunjang[]" class="selectpicker" data-style="btn-default btn-fill btn-block" data-menu-style="dropdown-blue">
								@foreach($auditor as $data)
									@if(in_array($data->id, $penunjang))
										<option selected value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@endif
								@endforeach
							</select>
                        </div>
                    </div>
					<div class="form-group">
                        <label class="col-md-3 control-label">Staff Audit</label>
                        <div class="col-md-6">
							<select id="staff_audit" multiple data-title="--PILIH AUDITOR--" name="staff_audit[]" class="selectpicker" data-style="btn-default btn-fill btn-block" data-menu-style="dropdown-blue">
								@foreach($auditor as $data)
									@if(in_array($data->id, $staff))
										<option selected value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->user->nama }}</option>
									@endif
								@endforeach
							</select>
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Nama Auditi/Unit Kerja</b><star>*</star></label>
                        <div class="col-md-6">
							<select name="auditee" class="selectpicker" data-title="--PILIH AUDITI--" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
								@foreach($auditee as $data)
									@if($jadwal->id_auditee == $data->id)
										<option selected value="{{ $data->id }}">{{ $data->unit_kerja }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->unit_kerja }}</option>
									@endif
								@endforeach
							</select>
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Tipe Audit</b><star>*</star></label>
                        <div class="col-md-6">
							<select name="id_tipeaudit" id="tipe" class="selectpicker" data-title="--PILIH TIPE AUDIT--" data-style="btn-default btn-block" data-menu-style="dropdown-blue">
								@foreach($tipeAudit as $data)
									@if($jadwal->id_tipeaudit == $data->id)
										<option selected value="{{ $data->id }}">{{ $data->nama_tipe }}</option>
									@else
										<option value="{{ $data->id }}">{{ $data->nama_tipe }}</option>
									@endif
								@endforeach
							</select>
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Aspek Audit</b><star>*</star></label>
							<div class="col-md-6">
								@foreach($aspekAudit as $data)
								@if(in_array($data->id, $aspek))
									<label class="label-aspek">
										<input checked type="checkbox" name="aspek_audit[]" class="aspek" value="{{ $data->id }}">
											{{ $data->nama_aspek }}
									</label>
								@else
									<label class="label-aspek">
										<input type="checkbox" name="aspek_audit[]" class="aspek" value="{{ $data->id }}">
											{{ $data->nama_aspek }}
									</label>
								@endif
								<br>
								@endforeach
							</div>

                    </div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Tanggal Mulai Audit</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="date" name="tanggal_mulai" id="tanggal_mulai" class="form-control" value="{{ $jadwal->tanggal_mulai }}"/>
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Tanggal Selesai Audit</b><star>*</star></label>
                        <div class="col-md-6">
                            <input type="date" name="tanggal_selesai" id="tanggal_selesai" class="form-control" value="{{ $jadwal->tanggal_selesai }}"/>
                        </div>
                    </div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Upload Surat Tugas</b></label>
                        <div class="col-md-6">
							<input type="file" name="surat_tugas" class="form-control">
                        </div>
                    </div>
					<div class="form-group">
						<div class="col-md-6 col-md-offset-3" style="padding-left: 6px !important;">
							<div class="alert alert-warning">
								<span><b> Perhatikan! - </b> Jangan UPLOAD file jika TIDAK ingin mengubah file</span>
							</div>
						</div>
					</div>

					<div class="form-group">
                        <label class="col-md-3 control-label label"><b>Keterangan</b></label>
                        <div class="col-md-6">
							<textarea name="keterangan" class="form-control" rows="8" cols="80">{{ $jadwal->keterangan }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3"></label>

                        <div class="col-md-6">
							<button type="submit" class="btn btn-fill btn-info btn-flat">SIMPAN</button>
							<a href="{{ route('operator.jadwal-audit.semua') }}" class="btn btn-fill btn-danger btn-flat">BATAL</a>
                        </div>
                    </div>
                </form>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->
@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif
<script type="text/javascript">
$().ready(function(){
	@if($jadwal->id_tipeaudit == 1)
		$(".label-aspek").hide();
	@else
		$(".label-aspek").show();
	@endif
	$('#tipe').change(function() {
		var value=$(this).val();
		if (value == 1) {
			$(".label-aspek").hide();
		}
		else{
			$(".label-aspek").show();
		}
	});

	$('#ketua_audit').change(function() {
		var nilai=$(this).val();
		$("#sekretaris_audit option[value='1']").remove();
	});
});
</script>
@endsection
