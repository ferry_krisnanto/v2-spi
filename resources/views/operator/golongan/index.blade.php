@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Golongan
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
		<div class="card">
			<div class="header text-center">
		        <h5 class="title">Golongan</h5>
		    </div>
            <div class="content">
				<button data-toggle="modal" data-target="#tambahGolongan" type="button" class="btn btn-primary btn-block" name="button"><i class="fa fa-plus"></i> Tambah</button>
				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Golongan</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
							<?php $i=1; ?>
							@foreach($golongan as $data)
							<tr>
	                            <td>{{ $i++ }}</td>
	                            <td>{{ $data->nama }}</td>
	                            <td class="text-right">
									<button data-toggle="modal" value="{{ $data->id }}" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-edit"></i></button>
	                                <a href="{{ route('operator.golongan.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon remove delete"><i class="fa fa-trash"></i></a>
	                            </td>
							</tr>
							@endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

<!-- Modal Tambah Aspek Audit -->
<div class="modal fade" id="tambahGolongan" tabindex="-1" role="dialog" aria-labelledby="tambahGolonganLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Tambah Aspek Audit</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.golongan.tambah.post') }}" method="POST">
          {{ csrf_field() }}
          <div class="form-group">
              <label>Nama Aspek Audit</label>
              <input type="text" name="nama" value="{{ old('nama') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Edit Auditee -->
<div class="modal fade" id="editGolongan" tabindex="-1" role="dialog" aria-labelledby="editGolonganLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Form Edit Data Auditi</h4>
      </div>
      <div class="modal-body">
        <form action="{{ route('operator.golongan.edit.post') }}" method="post">
          {{ csrf_field() }}
		  <input type="hidden" name="id" id="id_golongan" value="{{ old('id') }}">
		  <div class="form-group">
              <label>Nama Aspek Audit</label>
              <input type="text" name="nama" id="nama" value="{{ old('nama') }}" class="form-control" required="true">
          </div>
      </div>
      <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Cari Golongan",
        },

    });
    var table = $('#datatables').DataTable();

	$('.edit').click(function(){
		var idAtr = +$(this).val();
	    $.get('{{ URL::to("operator/golongan/search") }}/' + idAtr, function (data) {
			$('#id_golongan').val(data.id);
			$('#nama').val(data.nama);
	    });

		event.stopPropagation();
        $('#editGolongan').modal('show');
	});
});

</script>
@endsection
