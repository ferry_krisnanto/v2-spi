<div class="sidebar" data-color="grey" data-image="{{ asset('assets/img/full-screen-image-3.jpg') }}">
    {{--
        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag
    --}}
    <div class="logo text-center">
        <a href="#" class="logo-image">
            <img src="{{ asset('assets/img/logo.png') }}" class="image-responsive" width="20%">
        </a>
    </div>
<div class="logo logo-mini">
  <a href="#" class="logo-text">
    SIAUDIT
  </a>
</div>
  <div class="sidebar-wrapper">
    <div class="user">
        <div class="photo">
		@if(Auth::user()->id_role == 1)
          <img src="{{ asset('assets/img/users/ketua_spi.png') }}" />
		@elseif(Auth::user()->id_role == 2)
			<img src="{{ asset('assets/img/users/operator.png') }}" />
		@elseif(Auth::user()->id_role == 3)
			<img src="{{ asset('assets/img/users/auditor.png') }}" />
		@elseif(Auth::user()->id_role == 4)
			<img src="{{ asset('assets/img/users/auditee.png') }}" />
		@elseif(Auth::user()->id_role == 5)
			<img src="{{ asset('assets/img/users/koor_bidang.png') }}" />
		@elseif(Auth::user()->id_role == 6)
			<img src="{{ asset('assets/img/users/rektor.png') }}" />
		@endif
        </div>
        <div class="info">
            <a data-toggle="collapse" href="#collapseExample" class="collapsed">
                {{ Auth::user()->nama }}
                <b class="caret"></b>
            </a>
            <div class="collapse" id="collapseExample">
                {{-- <ul class="nav">
                    <li><a href="#">Ubah Password</a></li>
                </ul>
				--}}
            </div>
        </div>
    </div>
	<!-- KETUA SPI -->
    @if(Auth::user()->id_role == 1)
      <ul class="nav">
        <li class="{{ active('ketuaspi') }}">
            <a href="{{ route('ketuaspi') }}">
                <i class="pe-7s-graph"></i>
                <p>Beranda</p>
            </a>
        </li>
      </ul>

  		<ul class="nav">
  		  <li class="{{ active('ketuaspi.data-laporan.index') }}">
  			  <a href="{{ route('ketuaspi.data-laporan.index') }}">
  				  <i class="pe-7s-angle-right-circle"></i>
  				  <p>Data LHP</p>
  			  </a>
  		  </li>
  		</ul>

  		<ul class="nav">
  		  <li class="{{ active('ketuaspi.data-laporan.validated') }}">
  			  <a href="{{ route('ketuaspi.data-laporan.validated') }}">
  				  <i class="pe-7s-angle-right-circle"></i>
  				  <p>LHP Tervalidasi</p>
  			  </a>
  		  </li>
  		</ul>

	@elseif(Auth::user()->id_role == 2)
	<!-- OPERATOR -->
		<ul class="nav">
		  <li class="{{ active('operator') }}">
			  <a href="{{ route('operator') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Beranda</p>
			  </a>
		  </li>

		<li {{ active(['operator.jadwal-audit.pertahun', 'operator.jadwal-audit.semua']) }}>
            <a data-toggle="collapse" href="#jadwalCollapse">
                <i class="pe-7s-angle-right-circle"></i>
                <p>Jadwal Audit
                    <b class="caret"></b>
                </p>
            </a>
            <div class="collapse {{ (in_array(\Request::route()->getName(), ['admin.passing.tambah', 'admin.passing.index', 'admin.passing.univ.tambah'])) ? 'in' : '' }}" id="jadwalCollapse">
                <ul class="nav">
                    <li class="{{ active(['operator.jadwal-audit.pertahun']) }}"><a href="{{ route('operator.jadwal-audit.pertahun') }}">Lihat Pertahun</a></li>
                    <li class="{{ active(['operator.jadwal-audit.semua']) }}"><a href="{{ route('operator.jadwal-audit.semua') }}">Lihat Semua Jadwal</a></li>
                </ul>
            </div>
        </li>

		  <li class="{{ active('operator.auditi.index') }}">
			  <a href="{{ route('operator.auditi.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Data Auditee</p>
			  </a>
		  </li>

		  <li class="{{ active('operator.auditor.index') }}">
			  <a href="{{ route('operator.auditor.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Data Auditor</p>
			  </a>
		  </li>


		  <li class="{{ active('operator.aspek-audit.index') }}">
			  <a href="{{ route('operator.aspek-audit.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Aspek Audit</p>
			  </a>
		  </li>

		  <li class="{{ active('operator.fakultas.index') }}">
		  	<a href="{{ route('operator.fakultas.index') }}">
		  		<i class="pe-7s-angle-right-circle"></i>
		  		<p>Fakultas dan Jurusan</p>
		  	</a>
		  </li>

		  <li class="{{ active('operator.data-user.index') }}">
			  <a href="{{ route('operator.data-user.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Data User</p>
			  </a>
		  </li>


		{{-- <ul class="nav">
		  <li class="{{ active('operator.surat-tugas.index') }}">
			  <a href="{{ route('operator.surat-tugas.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Surat Tugas</p>
			  </a>
		  </li>
		</ul>
		--}}
	@elseif(Auth::user()->id_role == 3)
	<!-- AUDITOR -->
		<ul class="nav">
		  <li class="">
			  <a href="{{ route('auditor') }}">
				  <i class="pe-7s-graph"></i>
				  <p>Beranda</p>
			  </a>
		  </li>
		</ul>

		<ul class="nav">
		  <li class="">
			  <a href="{{ route('auditor.surat-tugas.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Surat Tugas</p>
			  </a>
		  </li>
		</ul>

		<ul class="nav">
		  <li class="">
			  <a href="{{ route('auditor.data-lhp.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Manajemen Data LHP</p>
			  </a>
		  </li>
		</ul>

    <!-- <ul class="nav">
		  <li class="">
			  <a href="#">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Tanggapan LHP</p>
			  </a>
		  </li>
		</ul> -->
	@elseif(Auth::user()->id_role == 4)
	<!-- AUDITI -->
		<ul class="nav">
		  <li class="">
			  <a href="{{ route('audity') }}">
				  <i class="pe-7s-home"></i>
				  <p>Beranda</p>
			  </a>
		  </li>
		<ul class="nav">
		  <li class="">
			  <a href="{{ route('surat_tugas') }}">
				  <i class="pe-7s-note2"></i>
				  <p>Surat Tugas Auditor</p>
			  </a>
		  </li>
			<li class="">
			  <a href="{{ route('jadwal_audit') }}">
				  <i class="pe-7s-note2"></i>
				  <p>Jadwal Audit Saya</p>
			  </a>
		  </li>
		  <li class="">
			  <a href="{{ route('LHP') }}">
				  <i class="pe-7s-note"></i>
				  <p>Laporan Hasil Pemeriksaan</p>
			  </a>
		  </li>
		</ul>
	@elseif(Auth::user()->id_role == 5)
	<!-- KOOR BIDANG -->
    <ul class="nav">
      <li class="{{ active('koorbidang') }}">
          <a href="{{ route('koorbidang') }}">
              <i class="pe-7s-graph"></i>
              <p>Beranda</p>
          </a>
      </li>
    </ul>

    <ul class="nav">
      <li class="{{ active('koorbidang.data-laporan.index') }}">
        <a href="{{ route('koorbidang.data-laporan.index') }}">
          <i class="pe-7s-angle-right-circle"></i>
          <p>Data LHP</p>
        </a>
      </li>
    </ul>

    <ul class="nav">
      <li class="{{ active('koorbidang.data-laporan.validated') }}">
        <a href="{{ route('koorbidang.data-laporan.validated') }}">
          <i class="pe-7s-angle-right-circle"></i>
          <p>LHP Tervalidasi</p>
        </a>
      </li>
    </ul>
	@elseif(Auth::user()->id_role == 6)

	<!-- REKTOR -->

		<ul class="nav">
		  <li class="{{ active('rektor') }}">
			  <a href="{{ route('rektor') }}">
				  <i class="pe-7s-graph"></i>
				  <p>Beranda</p>
			  </a>
		  </li>
		</ul>

		<ul class="nav">
		  <li class="{{ active('rektor.lhp.index') }}">
			  <a href="{{ route('rektor.lhp.index') }}">
				  <i class="pe-7s-angle-right-circle"></i>
				  <p>Data LHP</p>
			  </a>
		  </li>
		</ul>

    @endif
  </div>
</div>
