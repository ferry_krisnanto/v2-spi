<!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->

<script src="{{ asset('assets/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/jquery-ui.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>


<!--  Forms Validations Plugin -->
<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>

<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
<script src="{{ asset('assets/js/moment.min.js') }}"></script>

<!--  Date Time Picker Plugin is included in this js file -->
<script src="{{ asset('assets/js/bootstrap-datetimepicker.js') }}"></script>

<!--  Select Picker Plugin -->
<script src="{{ asset('assets/js/bootstrap-selectpicker.js') }}"></script>

<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
<script src="{{ asset('assets/js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>

<!--  Charts Plugin -->
<script src="{{ asset('assets/js/chartist.min.js') }}"></script>

<!--  Full Calendar Plugin    -->
<script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>

<!--  Notifications Plugin    -->
<script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>

<!-- Sweet Alert 2 plugin -->
<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>

<!-- Vector Map plugin -->
{{-- <script src="{{ asset('assets/js/jquery-jvectormap.js') }}"></script> --}}

<!--  Google Maps Plugin    -->
{{-- <script src="https://maps.googleapis.com/maps/api/js"></script> --}}

<!-- Wizard Plugin    -->
<script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>

<!--  Bootstrap Table Plugin    -->
<script src="{{ asset('assets/js/bootstrap-table.js') }}"></script>

<!--  Plugin for DataTables.net  -->
<script src="{{ asset('assets/js/jquery.datatables.js') }}"></script>
<script src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js">

</script>


<!--  Full Calendar Plugin    -->
<script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>

<!-- Light Bootstrap Dashboard Core javascript and methods -->
<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>

<!--   Sharrre Library    -->
<script src="{{ asset('assets/js/jquery.sharrre.js') }}"></script>

<script src="{{ asset('assets/js/demo.js') }}"></script>

<script src="{{ asset('assets/js/jquery.mask.js') }}"></script>

{{-- <script src="{{ asset('assets/js/countdown/jquery.countdown.min.js') }}"></script> --}}

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">
$().ready(function(){
	// $('#tanggal_mulai').datetimepicker({
	//    format: 'DD/MM/YYYY',
	//    icons: {
	// 	   time: "fa fa-clock-o",
	// 	   date: "fa fa-calendar",
	// 	   up: "fa fa-chevron-up",
	// 	   down: "fa fa-chevron-down",
	// 	   previous: 'fa fa-chevron-left',
	// 	   next: 'fa fa-chevron-right',
	// 	   today: 'fa fa-screenshot',
	// 	   clear: 'fa fa-trash',
	// 	   close: 'fa fa-remove'
	//    }
	// });
	// $('#tanggal_selesai').datetimepicker({
	//    format: 'DD/MM/YYYY',
	//    icons: {
	// 	   time: "fa fa-clock-o",
	// 	   date: "fa fa-calendar",
	// 	   up: "fa fa-chevron-up",
	// 	   down: "fa fa-chevron-down",
	// 	   previous: 'fa fa-chevron-left',
	// 	   next: 'fa fa-chevron-right',
	// 	   today: 'fa fa-screenshot',
	// 	   clear: 'fa fa-trash',
	// 	   close: 'fa fa-remove'
	//    }
	// });
});

$(document).on("click", ".delete", function(e) {
    var link = $(this).attr("href"); // "get" the intended link in a var
    e.preventDefault();
    swal({  title: "Kamu Yakin?",
        text: "Data akan dihapus selamanya!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn btn-info btn-fill",
        confirmButtonText: "Ya, hapus saja!",
        cancelButtonClass: "btn btn-danger btn-fill",
        cancelButtonText: "Tidak, gajadi!",
        closeOnConfirm: false,
		allowOutsideClick: false,
    },function(){
        document.location.href = link;
    });
});
</script>
@yield('script')
