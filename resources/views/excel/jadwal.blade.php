<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title></title>
	</head>
	<body>
		<table border="1">
			<tr align="center">
				<th>NO</th>
				<th>KEGIATAN</th>
				<th>BULAN</th>
				<th>KETERANGAN</th>
			</tr>
			<tr width="50px">
				<th></th>
				<th></th>
				@for($x = 1; $x<=12; $x++)
					<th>{{ $x }}</th>
				@endfor
			</tr>
			<?php $i=1; ?>
			@foreach($jadwal as $data)
			<tr>
				<td>{{ $i++ }}</td>
				<td>{{ $data->auditee->unit_kerja }}</td>
				<!-- START DATE -->
				@for($y = 1; $y <= 12; $y++)
					@if(( (int)date('m', strtotime($data->tanggal_mulai)) <= $y) && ( (int)date('m', strtotime($data->tanggal_selesai)) >= $y))
						<td>V</td>
					@else
						<td></td>
					@endif
				@endfor
				<!-- END DATE -->
				<td>{{ $data->keterangan }}</td>
			</tr>
			@endforeach
		</table>
	</body>
</html>
