@extends('layouts.apps')
@section('style')
@endsection

@section('nav_title')
Data Laporan Hasil Pemeriksaan
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="content">

				<br>
                <div class="fresh-datatables">
                    <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode Dokument</th>
                                <th>Tipe Audit</th>
							                 	<th>Tanggal Mulai Audit</th>
                				        <th>Tanggal Selesai Audit</th>
                				        <th>Tanggapan Rektor</th>
                                <th class="disabled-sorting text-right">Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $i=1; ?>
                        @foreach($lap as $data)
                            <tr>
                                  <td>{{ $i++ }}</td>
                                  <td>{{ $data->kode_dokumen }}</td>
                                  <td>{{ $data->jadwalAudit->tipeAudit->nama_tipe }}</td>
                                  <td>{{ $data->jadwalAudit->tanggal_mulai }}</td>
                                  <td>{{ $data->jadwalAudit->tanggal_selesai }}</td>
                                  <td>{{ $data->tanggapanAudit->tanggapan_rektor }}</td>

                                <td class="text-right">
                                	<!-- <button type="button" class="btn btn-simple btn-warning btn-icon tambah" name="button"><i class="fa fa-plus-square"></i></button>  -->

									                 <button data-toggle="modal" value="{{ $data->id }}" data-target="#editTanggapanLHP" type="button" class="btn btn-simple btn-warning btn-icon edit" name="button"><i class="fa fa-pencil"></i></button>              

                                    <a href="{{ route('rektor.lhp.hapus', $data->id) }}" class="btn btn-simple btn-danger btn-icon delete"><i class="fa fa-trash"></i></a>
                                </td>

                            </tr>
                              @endforeach
                        </tbody>
                    </table>
                </div>
            </div><!-- end content-->
        </div><!--  end card  -->
    </div> <!-- end col-md-12 -->
</div> <!-- end row -->

 <!-- Modal Edit Rektor -->
                <div class="modal fade" id="editTanggapanLHP" tabindex="-1" role="dialog" aria-labelledby="editTanggapanLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Form Edit Tanggapan LHP</h4>
                      </div>
                      <div class="modal-body">
                        <form action="{{route('rektor.lhp.edit.post')}}" method="post">
                          {{ csrf_field() }}
                          <input type="hidden" name="idnya" id="idnya" value="{{ old('id') }}">
                          <div class="form-group">
                              <label>Kode Dokumen</label>
                              <input type="text" name="kodedokumen" value="{{ old('kode_dokumen') }}" id="kodedokumennya" class="form-control" required="true" readonly="">
                          </div>
                          <div class="form-group">
                              <label>Tanggapan Rektor</label>
                              <input name="tanggapan_rektor" id="rektornya" value="{{ old('tanggapan_rektor') }}" class="form-control" required="true"></input>
                          </div>
                      </div>
                      <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
<!-- Modal Edit Rektor -->

<!-- Modal Lihat Rektor -->
                <div class="modal fade" id="lihatTanggapanLHP" tabindex="-1" role="dialog" aria-labelledby="lihatTanggapanLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" align="center">Lihat Tanggapan LHP</h4>
                      </div>
                      <div class="modal-body">
                        <form action="#" method="post">
                          {{ csrf_field() }}
                          <div class="form-group">
                              <label>Kode Dokumen</label>
                              <input type="text" name="kodedokumen" value="{{ old('kode_dokumen') }}" class="form-control" required="true" readonly="">
                          </div><!-- 
                          <div class="form-group">
                              <label>Jadwal Audit</label>
                              <input type="text" name="jadwalAudit" value="#" class="form-control" required="true" readonly="">
                          </div>
                          <div class="form-group">
                              <label>Tanggal Mulai</label>
                              <input type="text" name="kodedokumen" value="#" class="form-control" required="true" readonly="">
                          </div>
                          <div class="form-group">
                              <label>Tanggal Selesai</label>
                              <input type="text" name="kodedokumen" value="#" class="form-control" required="true" readonly="">
                          </div> -->
                          <div class="form-group">
                              <label>Tanggapan Rektor</label>
                              <input name="tanggapanrektor" value="{{ old('tanggapan_rektor') }}" class="form-control" required="true"></input>
                          </div>
                      </div>
                      <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Tutup</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>
<!-- Modal Lihat Rektor -->



@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif


<script type="text/javascript">
$(document).ready(function() {
    $('#datatables').DataTable({
        "pagingType": "full_numbers",
        "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
        responsive: true,
        language: {
            search: "_INPUT_",
            searchPlaceholder: "Search records",
        },

    });
    var table = $('#datatables').DataTable();

  $('.edit').click(function(){
    var userId = +$(this).val();
      $.get('{{ URL::to("rektor/laporan-hasil-pemeriksaan/search") }}/' + userId, function (data) {
      $('#idnya').val(data.id);
      $('#kodedokumennya').val(data.kode_dokumen);
      $('#rektornya').val(data.tanggapan_rektor);
      });

    event.stopPropagation();
        $('#editTanggapanLHP').modal('show');
  });
});

</script>


@endsection
