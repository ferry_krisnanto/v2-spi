@extends('layouts.apps')
@section('style')
@endsection
@section('nav_title')
Data Laporan Hasil Pemeriksaan Tervalidasi
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
<div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
								<div class="toolbar">

	                            </div>
	                            <p align="center"> Data Laporan Hasil Pemeriksaan Tervalidasi</p>
                                <div class="fresh-datatables">
                					<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                						<thead>
                							<tr>
                								<th>No.</th>
                								<th>Kode Dokumen</th>
                								<th>Tipe Audit</th>
                								<th>Tanggal Mulai Audit</th>
                								<th>Tanggal Selesai Audit</th>
                                                <th>Tanggapan Rektor</th>
                                                <th>Status</th>
                								<th class="disabled-sorting text-right">Aksi</th>
                							</tr>
                						</thead>
                						<tbody>
                							<?php $i=1; ?>
                							@foreach($lap as $data)
                							<tr>
                                  <td>{{ $i++ }}</td>
                                  <td>{{ $data->kode_dokumen }}</td>
                  				  <td>{{ $data->jadwalAudit->tipeAudit->nama_tipe }}</td>
                  				  <td>{{ $data->jadwalAudit->tanggal_mulai }}</td>
                  				  <td>{{ $data->jadwalAudit->tanggal_selesai }}</td>
                  				 <td>
								 @if(!empty($data->tanggapanAudit->tanggapan_rektor))
								 {{ $data->tanggapanAudit->tanggapan_rektor }}
								 @else
								 Tidak ada tanggapan
								 @endif</td>
                  				 <td>{{ $data->status }}</td>
                				 <td class="text-right">
                				<a href="{{ route('koorbidang.data-laporan.viewdocument', $data->id) }}" class="btn btn-simple btn-info btn-icon like"><i class="fa fa-eye"></i></a>
                				</td>
                			 </tr>
                            @endforeach

                						</tbody>
                					</table>
        				        </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

            </div>
        </div>

    <!--   Core JS Files and PerfectScrollbar library inside jquery.ui   -->

	<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
	<script src="{{ asset('assets/js/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>

	<!--  Forms Validations Plugin -->
	<script src="{{ asset('assets/js/jquery.validate.min.js') }}"></script>
	<!--  Plugin for Date Time Picker and Full Calendar Plugin-->
	<script src="{{ asset('assets/js/moment.min.js') }}"></script>
	<!--  Checkbox, Radio, Switch and Tags Input Plugins -->
	<script src="{{ asset('assets/js/bootstrap-checkbox-radio-switch-tags.js') }}"></script>
	<!--  Charts Plugin -->
	<script src="{{ asset('assets/js/chartist.min.js') }}"></script>
    <!--  Notifications Plugin    -->
    <script src="{{ asset('assets/js/bootstrap-notify.js') }}"></script>
    <!-- Sweet Alert 2 plugin -->
	<script src="{{ asset('assets/js/sweetalert2.js') }}"></script>
    <!-- Vector Map plugin -->
	<script src="{{ asset('assets/js/jquery-jvectormap.js') }}"></script>

	<!-- Wizard Plugin    -->
    <script src="{{ asset('assets/js/jquery.bootstrap.wizard.min.js') }}"></script>
	<!--  Bootstrap Table Plugin    -->
	<script src="{{ asset('assets/js/bootstrap-table.js') }}"></script>
	<!--  Plugin for DataTables.net  -->
	<script src="{{ asset('assets/js/jquery.datatables.js') }}"></script>
    <!--  Full Calendar Plugin    -->
    <script src="{{ asset('assets/js/fullcalendar.min.js') }}"></script>
    <!-- Light Bootstrap Dashboard Core javascript and methods -->
	<script src="{{ asset('assets/js/light-bootstrap-dashboard.js') }}"></script>
	<!--   Sharrre Library    -->
    <script src="{{ asset('assets/js/jquery.sharrre.js') }}"></script>

	<!-- Light Bootstrap Dashboard DEMO methods, don't include it in your project! -->
	<script src="{{ asset('assets/js/demo.js') }}"></script>


    <script type="text/javascript">
    $(document).ready(function() {
		$('#datatables').DataTable({
		    "pagingType": "full_numbers",
		    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		    responsive: true,
		    language: {
		    search: "_INPUT_",
		    searchPlaceholder: "Search records",
		    }

		});


		var table = $('#datatables').DataTable();

		// Edit record
		table.on( 'click', '.edit', function () {
		    $tr = $(this).closest('tr');

		    var data = table.row($tr).data();
		    alert( 'You press on Row: ' + data[0] + ' ' + data[1] + ' ' + data[2] + '\'s row.' );
		} );

		// Delete a record
		table.on( 'click', '.remove', function (e) {
		    $tr = $(this).closest('tr');
		    table.row($tr).remove().draw();
		    e.preventDefault();
		} );
	});

    </script>
@endsection

@section('script')

@endsection
