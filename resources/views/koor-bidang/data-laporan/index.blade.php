@extends('layouts.apps')
@section('style')
@endsection
@section('nav_title')
Laporan Hasil Pemeriksaan
@endsection
@section('content')
@if (count($errors) > 0)
<div class="row">
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
</div>
@endif
<div class="content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="content">
								<div class="toolbar">

	                            </div>
	                            <p align="center"> Data Laporan Hasil Pemeriksaan</p>
                                <div class="fresh-datatables">
                					<table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                						<thead>
                							<tr>
                								<th>No.</th>
                								<th>Kode Dokumen</th>
                								<th>Tipe Audit</th>
                								<th>Tanggal Mulai Audit</th>
                								<th>Tanggal Selesai Audit</th>
                                <th>Tanggapan Rektor</th>
                                <th>Status</th>
                								<th class="disabled-sorting text-right">Aksi</th>
                							</tr>
                						</thead>
                						<tbody>
                							<?php $i=1; ?>
                							@foreach($lap as $data)
                							<tr>
                                  <td>{{ $i++ }}</td>
                                  <td>{{ $data->kode_dokumen }}</td>
                  								<td>{{ $data->jadwalAudit->tipeAudit->nama_tipe }}</td>
                  								<td>{{ $data->jadwalAudit->tanggal_mulai }}</td>
                  								<td>{{ $data->jadwalAudit->tanggal_selesai }}</td>
                  								<td>
													@if(!empty($data->tanggapanAudit->tanggapan_rektor))
                            {{ $data->tanggapanAudit->tanggapan_rektor }}
                          @else
                            Tidak ada tanggapan
                          @endif
                          </td>
                  								<td>{{ $data->status }}</td>
                  								<td class="text-right">
                  									<a href="{{ route('koorbidang.data-laporan.viewdocument', $data->id) }}" class="btn btn-simple btn-primary btn-icon edit" name="button"><i class="fa fa-check"></i></a>
													<button value="{{ $data->id }}" data-toggle="modal" type="button" name="button" class="btn btn-simple btn-warning btn-icon editLhp"><i class="fa fa-pencil"></i></button>
                  								</td>
                                </tr>
    							            @endforeach

                						</tbody>
                					</table>
        				        </div>
                            </div><!-- end content-->
                        </div><!--  end card  -->
                    </div> <!-- end col-md-12 -->
                </div> <!-- end row -->

                <!-- Modal Edit Auditee -->
                <div class="modal fade" id="editTanggapanLHP" tabindex="-1" role="dialog" aria-labelledby="editTanggapanLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Form Edit Tanggapan LHP</h4>
                      </div>
                      <div class="modal-body">
                        <form action="{{ route('koorbidang.jadwal-audit.edit.post') }}" method="POST">
							<input type="hidden" name="id_tanggapan" id="id_tanggapan">
                          {{ csrf_field() }}
                          <div class="form-group">
                              <label>Kode Dokumen</label>
                              <input readonly type="text" name="kodedokumen" value="{{ old('kodedokumen') }}" class="form-control" required="true" id="kodedokumen">
                          </div>
                		  <div class="form-group">
                              <label>Tanggapan Auditor</label>
                              <textarea name="tanggapanauditor" value="{{ old('tanggapanauditor') }}" class="form-control" required="true" id="tanggapanauditor"></textarea>
                          </div>
                      </div>
                      <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary btn-fill">Simpan</button>
                        </form>
                      </div>
                    </div>
                  </div>
                </div>

            </div>
        </div>


@endsection

@section('script')
@if ($message=Session::get('success'))
<script> swal("Sukses..","{{ $message }}","success") </script>
@elseif ($message=Session::get('error'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('danger'))
<script> swal("Oops...","{{ $message }}","error") </script>
@elseif ($message=Session::get('warning'))
<script> swal("Oops...","{{ $message }}","warning") </script>
@endif

<script type="text/javascript">
$(document).ready(function() {
	$('#datatables').DataTable({
		"pagingType": "full_numbers",
		"lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
		responsive: true,
		language: {
		search: "_INPUT_",
		searchPlaceholder: "Search records",
		}

	});


	var table = $('#datatables').DataTable();

	// Edit record
	$('.editLhp').click(function(){
		var idNya = +$(this).val();
		$.get('{{ URL::to("koorbidang/data-laporan/search-tanggapan") }}/' + idNya, function (data) {
			$('#id_tanggapan').val(data.id_tanggapan);
			$('#kodedokumen').val(data.kode_dokumen);
			$('#tanggapanauditor').val(data.tanggapan_auditor);
		});
		event.stopPropagation();
		$('#editTanggapanLHP').modal('show');
	});

	// Delete a record
	table.on( 'click', '.remove', function (e) {
		$tr = $(this).closest('tr');
		table.row($tr).remove().draw();
		e.preventDefault();
	} );

	//Like record
	table.on( 'click', '.like', function () {
		alert('You clicked on Like button');
	});
});

</script>
@endsection
