<?php

use Illuminate\Database\Seeder;

class TipeAuditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('tipe_audit')->insert([
            'id'   => '1',
            'nama_tipe' => 'Reguler',
        ]);
        DB::table('tipe_audit')->insert([
            'id'   => '2',
            'nama_tipe' => 'PDTT',
        ]);
    }
}
