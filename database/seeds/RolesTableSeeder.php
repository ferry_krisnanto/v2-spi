<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id'   => '1',
            'nama' => 'Ketua SPI',
        ]);
        DB::table('roles')->insert([
            'id'   => '2',
            'nama' => 'Operator',
        ]);
        DB::table('roles')->insert([
            'id'   => '3',
            'nama' => 'Auditor',
        ]);


        DB::table('roles')->insert([
            'id'   => '4',
            'nama' => 'Audity',
        ]);
        DB::table('roles')->insert([
            'id'   => '5',
            'nama' => 'Koordinator Bidang',
        ]);
        DB::table('roles')->insert([
            'id'   => '6',
            'nama' => 'Rektor',
        ]);
    }
}
