<?php

use Illuminate\Database\Seeder;

class GolonganTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('golongan')->insert([
            'id'   => '1',
            'nama' => 'III/a',
        ]);
        DB::table('golongan')->insert([
            'id'   => '2',
            'nama' => 'III/b',
        ]);
		DB::table('golongan')->insert([
            'id'   => '3',
            'nama' => 'III/c',
        ]);
		DB::table('golongan')->insert([
            'id'   => '4',
            'nama' => 'III/d',
        ]);
		DB::table('golongan')->insert([
            'id'   => '5',
            'nama' => 'IV/a',
        ]);
		DB::table('golongan')->insert([
            'id'   => '6',
            'nama' => 'IV/b',
        ]);
		DB::table('golongan')->insert([
            'id'   => '7',
            'nama' => 'IV/c',
        ]);
		DB::table('golongan')->insert([
            'id'   => '8',
            'nama' => 'IV/d',
        ]);
		DB::table('golongan')->insert([
            'id'   => '9',
            'nama' => 'IV/e',
        ]);
    }
}
