<?php

use Illuminate\Database\Seeder;
use App\Model\User;
use Faker\Factory as Faker;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$faker = Faker::create();

		$data = new User;
        $data->username = 'ketua_spi';
        $data->email    = $faker->email;
        $data->nama     = 'Ketua SPI';
        $data->id_role  = 1;
        $data->password = bcrypt('123456');
        $data->save();

		$data = new User;
        $data->username = 'operator';
        $data->email    = $faker->email;
        $data->nama     = 'Operator';
        $data->id_role  = 2;
        $data->password = bcrypt('123456');
        $data->save();

		$data = new User;
        $data->username = 'auditor';
        $data->email    = $faker->email;
        $data->nama     = 'Auditor';
        $data->id_role  = 3;
        $data->password = bcrypt('123456');
        $data->save();

		$data = new User;
        $data->username = 'auditi';
        $data->email    = $faker->email;
        $data->nama     = 'Auditi';
        $data->id_role  = 4;
        $data->password = bcrypt('123456');
        $data->save();

		$data = new User;
        $data->username = 'koor_bidang';
        $data->email    = $faker->email;
        $data->nama     = 'Koordinator Bidang';
        $data->id_role  = 5;
        $data->password = bcrypt('123456');
        $data->save();

		$data = new User;
        $data->username = 'rektor';
        $data->email    = $faker->email;
        $data->nama     = 'Rektor';
        $data->id_role  = 6;
        $data->password = bcrypt('123456');
        $data->save();
    }
}
