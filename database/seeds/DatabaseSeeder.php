<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
		$this->call(GolonganTableSeeder::class);
		$this->call(JabatanAuditorTableSeeder::class);
		$this->call(AspekAuditTableSeeder::class);
		$this->call(TipeAuditTableSeeder::class);
		$this->call(FakultasTableSeeder::class);
    }
}
