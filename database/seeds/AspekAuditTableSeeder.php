<?php

use Illuminate\Database\Seeder;

class AspekAuditTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('aspek_audit')->insert([
            'id'   		=> '1',
            'nama_aspek' => 'Keuangan/Akuntansi',
        ]);
        DB::table('aspek_audit')->insert([
            'id'   		=> '2',
            'nama_aspek' => 'SDM',
        ]);
		DB::table('aspek_audit')->insert([
            'id'   		=> '3',
            'nama_aspek' => 'Ketatalaksanaan Organisasi',
        ]);
		DB::table('aspek_audit')->insert([
            'id'   		=> '4',
            'nama_aspek' => 'BMN/Aset',
        ]);
		DB::table('aspek_audit')->insert([
            'id'   		=> '5',
            'nama_aspek' => 'Hukum',
        ]);
    }
}
