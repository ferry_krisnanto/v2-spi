<?php

use Illuminate\Database\Seeder;

class FakultasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('fakultas')->insert([
            'id'   			=> '1',
            'nama_fakultas' => 'MIPA',
        ]);
		DB::table('fakultas')->insert([
            'id'   			=> '2',
            'nama_fakultas' => 'Teknik',
        ]);
		DB::table('fakultas')->insert([
            'id'   			=> '3',
            'nama_fakultas' => 'KIP',
        ]);
		DB::table('fakultas')->insert([
            'id'   			=> '4',
            'nama_fakultas' => 'Ekonomi',
        ]);
		DB::table('fakultas')->insert([
            'id'   			=> '5',
            'nama_fakultas' => 'Hukum',
        ]);
		DB::table('fakultas')->insert([
            'id'   			=> '6',
            'nama_fakultas' => 'Pertanian',
        ]);
    }
}
