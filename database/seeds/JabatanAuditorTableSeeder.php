<?php

use Illuminate\Database\Seeder;

class JabatanAuditorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('jabatan_auditor')->insert([
            'id'   	=> '1',
            'nama' 	=> 'Ketua Audit',
        ]);
		DB::table('jabatan_auditor')->insert([
            'id'   	=> '2',
            'nama' 	=> 'Sekretaris',
        ]);
		DB::table('jabatan_auditor')->insert([
            'id'   	=> '3',
            'nama' 	=> 'Anggota Tim',
        ]);
		DB::table('jabatan_auditor')->insert([
            'id'   	=> '4',
            'nama' 	=> 'Anggota Penunjang',
        ]);
		DB::table('jabatan_auditor')->insert([
            'id'   	=> '5',
            'nama' 	=> 'Staff Audit',
        ]);
    }
}
