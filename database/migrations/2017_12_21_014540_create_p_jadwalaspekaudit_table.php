<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePJadwalaspekauditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_jadwalaspekaudit', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_jadwalAudit')->length(10);
			$table->unsignedInteger('id_aspekaudit')->length(10);
            $table->timestamps();

			$table->index('id_jadwalAudit');
			$table->foreign('id_jadwalAudit')
				->references('id')->on('jadwal_audit')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->index('id_aspekaudit');
			$table->foreign('id_aspekaudit')
				->references('id')->on('aspek_audit')
				->onDelete('cascade')
				->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('p_jadwalaspekaudit');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
