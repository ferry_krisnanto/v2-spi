<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLaporanauditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('laporanaudit', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_jadwalAudit')->length(10);
			$table->string('nomor_urut', 100);
			$table->string('kode_dokumen', 100);
			$table->string('kode_temuan', 100);
			$table->string('deskripsi_temuan', 100);
			$table->string('kriteria', 100);
			$table->string('akibat', 100);
			$table->string('sebab', 100);
			$table->text('kesimpulan');
			$table->text('rekomendasi');
			$table->string('rencana_perbaikan', 100);
			$table->string('tindak_lanjut', 100);
			$table->date('jadwal_penyelesaian');
			$table->string('status', 100);
			$table->string('filedokumen', 100);
			$table->timestamps();

			$table->index('id_jadwalAudit');
			$table->foreign('id_jadwalAudit')
				->references('id')->on('jadwal_audit')
				->onDelete('cascade')
				->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('laporanaudit');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
