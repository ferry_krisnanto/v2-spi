<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePAuditorauditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_auditoraudit', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_jadwalAudit')->length(10);
			$table->unsignedInteger('id_auditor')->length(10);
			$table->unsignedInteger('id_jabatan_audit')->length(10);
            $table->timestamps();

			$table->index('id_jadwalAudit');
			$table->foreign('id_jadwalAudit')
				->references('id')->on('jadwal_audit')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->index('id_auditor');
			$table->foreign('id_auditor')
				->references('id')->on('auditor')
				->onDelete('cascade')
				->onUpdate('cascade');

			$table->index('id_jabatan_audit');
			$table->foreign('id_jabatan_audit')
				->references('id')->on('jabatan_auditor')
				->onDelete('cascade')
				->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('p_auditoraudit');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
