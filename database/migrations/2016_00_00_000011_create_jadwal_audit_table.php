<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJadwalAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jadwal_audit', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_auditee')->length(10);
			$table->unsignedInteger('id_tipeaudit')->length(10);
            $table->string('nomorsurat', 100);
			$table->date('tanggal_mulai');
			$table->date('tanggal_selesai');
			$table->string('attachment_surattugas', 100);
			$table->string('keterangan', 100)->nullable();
            $table->timestamps();

			$table->index('id_auditee');
            $table->foreign('id_auditee')
                ->references('id')->on('auditee')
                ->onDelete('cascade')
                ->onUpdate('cascade');

			$table->index('id_tipeaudit');
            $table->foreign('id_tipeaudit')
                ->references('id')->on('tipe_audit')
                ->onDelete('cascade')
                ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('jadwal_audit');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
