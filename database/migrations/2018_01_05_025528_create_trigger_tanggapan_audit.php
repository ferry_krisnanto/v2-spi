<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTriggerTanggapanAudit extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		DB::unprepared("
            CREATE TRIGGER trigger_insert_tanggapan AFTER INSERT ON `laporanaudit` FOR EACH ROW
            BEGIN
                INSERT INTO tanggapanaudit(id_laporanaudit) VALUES(NEW.id);
            END
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared('DROP TRIGGER `trigger_insert_tanggapan`');
    }
}
