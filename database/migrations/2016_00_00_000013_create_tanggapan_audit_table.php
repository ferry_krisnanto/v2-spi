<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTanggapanAuditTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tanggapanaudit', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_laporanaudit')->length(10);
			$table->string('tanggapan_auditee', 100)->default('-');
			$table->string('tanggapan_auditor', 100)->default('-');
			$table->string('tanggapan_rektor', 100)->default('-');
			$table->string('keterangan', 100)->default('-');
            $table->timestamps();

			$table->index('id_laporanaudit');
			$table->foreign('id_laporanaudit')
				->references('id')->on('laporanaudit')
				->onDelete('cascade')
				->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
		Schema::dropIfExists('tanggapanaudit');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
