<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditeeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditee', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_user')->length(10);
            $table->string('unit_kerja');
			$table->string('pimpinan_kerja');
			$table->string('nip');
			$table->string('masa_kerja');
			$table->string('keterangan');
			$table->string('token', 20);
            $table->timestamps();

			$table->index('id_user');
            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('auditee');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
