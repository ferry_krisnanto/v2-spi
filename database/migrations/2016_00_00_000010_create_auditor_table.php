<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auditor', function (Blueprint $table) {
			$table->engine = 'InnoDB';
			$table->increments('id');
			$table->unsignedInteger('id_user')->length(10);
			$table->unsignedInteger('id_jurusan')->length(10);
			$table->unsignedInteger('id_golongan')->length(10);
			$table->string('nip');
			$table->string('token', 20);
            $table->timestamps();

			$table->index('id_user');
            $table->foreign('id_user')
                ->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('cascade');

			$table->index('id_jurusan');
            $table->foreign('id_jurusan')
                ->references('id')->on('jurusan')
                ->onDelete('cascade')
                ->onUpdate('cascade');

			$table->index('id_golongan');
            $table->foreign('id_golongan')
                ->references('id')->on('golongan')
                ->onDelete('cascade')
                ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('auditor');
		DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
